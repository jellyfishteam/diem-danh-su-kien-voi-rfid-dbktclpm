/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dell
 */
public class StudentTest {
    
    private Student instance;
    
    public StudentTest() {
        instance= new Student("b1400717","nguyen quang phong","00000","DI1496A1","ky thuat phan mem","CNTT-TT","40");
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getMSSV method, of class Student.
     */
    @org.junit.Test
    public void testGetMSSV() {
        System.out.println("getMSSV");
        String expResult = "b1400717";
        String result = instance.getMSSV();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getHoTen method, of class Student.
     */
    @org.junit.Test
    public void testGetHoTen() {
        System.out.println("getHoTen");
        String expResult = "nguyen quang phong";
        String result = instance.getHoTen();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getRFID method, of class Student.
     */
    @org.junit.Test
    public void testGetRFID() {
        System.out.println("getRFID");
        String expResult = "00000";
        String result = instance.getRFID();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getLop method, of class Student.
     */
    @org.junit.Test
    public void testGetLop() {
        System.out.println("getLop");
        String expResult = "DI1496A1";
        String result = instance.getLop();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getNganh method, of class Student.
     */
    @org.junit.Test
    public void testGetNganh() {
        System.out.println("getNganh");
        String expResult = "ky thuat phan mem";
        String result = instance.getNganh();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getKhoa method, of class Student.
     */
    @org.junit.Test
    public void testGetKhoa() {
        System.out.println("getKhoa");
        String expResult = "CNTT-TT";
        String result = instance.getKhoa();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getKhoaHoc method, of class Student.
     */
    @org.junit.Test
    public void testGetKhoaHoc() {
        System.out.println("getKhoaHoc");
        String expResult = "40";
        String result = instance.getKhoaHoc();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Student.
     */
    @org.junit.Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Mã SV: " + "b1400717" + " | Họ Tên: " + "nguyen quang phong";
        String result = instance.toString();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }
    
}
