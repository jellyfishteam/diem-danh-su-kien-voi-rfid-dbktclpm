/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dell
 */
public class TeacherTest {
    
    private Teacher instance;
    
    public TeacherTest() {
        instance = new Teacher("123", "astoria pendragon", "321", "dragon@gmail.com", "ky thuat phan mem", "CNTT-TT");
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getMSCB method, of class Teacher.
     */
    @Test
    public void testGetMSCB() {
        System.out.println("getMSCB");
        String expResult = "123";
        String result = instance.getMSCB();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getHoTen method, of class Teacher.
     */
    @Test
    public void testGetHoTen() {
        System.out.println("getHoTen");
        String expResult = "astoria pendragon";
        String result = instance.getHoTen();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getNganh method, of class Teacher.
     */
    @Test
    public void testGetNganh() {
        System.out.println("getNganh");
        String expResult = "ky thuat phan mem";
        String result = instance.getNganh();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getKhoa method, of class Teacher.
     */
    @Test
    public void testGetKhoa() {
        System.out.println("getKhoa");
        String expResult = "CNTT-TT";
        String result = instance.getKhoa();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getRFID method, of class Teacher.
     */
    @Test
    public void testGetRFID() {
        System.out.println("getRFID");
        String expResult = "321";
        String result = instance.getRFID();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getEmail method, of class Teacher.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        String expResult = "dragon@gmail.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Teacher.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Mã CB: " + "123" + " | Họ tên: " + "astoria pendragon";
        String result = instance.toString();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }
    
}
