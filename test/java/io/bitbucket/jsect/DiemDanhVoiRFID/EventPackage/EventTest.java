/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dell
 */
public class EventTest {
    
    private Event instance;
    
    public EventTest() {
        
        try {
            Date date;
            Date timeIn;
            Date timeOut;
            date = new SimpleDateFormat("dd-MM-yyyy").parse("11-12-2017");
            timeIn = new SimpleDateFormat("HH:mm").parse("11:20");
            timeOut = new SimpleDateFormat("HH:mm").parse("12:20");
            instance = new Event("0000", "hieu heo slayer", "kill hieu heo", "quan bon", date, timeIn, timeOut);
        } catch (ParseException ex) {
            Logger.getLogger(EventTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getId method, of class Event.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        String expResult = "0000";
        String result = instance.getId();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getTitle method, of class Event.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        String expResult = "hieu heo slayer";
        String result = instance.getTitle();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getContent method, of class Event.
     */
    @Test
    public void testGetContent() {
        System.out.println("getContent");
        String expResult = "kill hieu heo";
        String result = instance.getContent();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDate method, of class Event.
     */
    @Test
    public void testGetDate() {
        System.out.println("getDate");
        
        try {
            Date expResult;
            expResult = new SimpleDateFormat("dd-MM-yyy").parse("11-12-2017");
            Date result = instance.getDate();
            assertEquals(expResult, result);
        } catch (ParseException ex) {
            Logger.getLogger(EventTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getPlace method, of class Event.
     */
    @Test
    public void testGetPlace() {
        System.out.println("getPlace");
        String expResult = "quan bon";
        String result = instance.getPlace();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setTitle method, of class Event.
     */
    @Test
    public void testSetTitle() {
        System.out.println("setTitle");
        String title = "grail war";
        instance.setTitle(title);
        String expResult = "grail war";
        String result = instance.getTitle();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setContent method, of class Event.
     */
    @Test
    public void testSetContent() {
        System.out.println("setContent");
        String content = "7 servant";
        instance.setContent(content);
        String expResult = "7 servant";
        String result = instance.getContent();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDate method, of class Event.
     */
    @Test
    public void testSetDate() {
        System.out.println("setDate");
        
        try {
            Date expResult;
            Date date = new SimpleDateFormat("dd-MM-yyy").parse("12-12-2017");;
            instance.setDate(date);
            expResult = new SimpleDateFormat("dd-MM-yyy").parse("12-12-2017");
            Date result = instance.getDate();
            assertEquals(expResult, result);
        } catch (ParseException ex) {
            Logger.getLogger(EventTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getTimeIn method, of class Event.
     */
    @Test
    public void testGetTimeIn() {
        System.out.println("getTimeIn");
        
        try {
            Date expResult;
            expResult = new SimpleDateFormat("HH:mm").parse("11:20");
            Date result = instance.getTimeIn();
            assertEquals(expResult, result);
        } catch (ParseException ex) {
            Logger.getLogger(EventTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getTimeOut method, of class Event.
     */
    @Test
    public void testGetTimeOut() {
        System.out.println("getTimeOut");
        
        try {
            Date expResult;
            expResult = new SimpleDateFormat("HH:mm").parse("12:20");
            Date result = instance.getTimeOut();
            assertEquals(expResult, result);
        } catch (ParseException ex) {
            Logger.getLogger(EventTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getNumMembers method, of class Event.
     */
    @Test
    public void testGetNumMembers() {
        System.out.println("getNumMembers");
        int expResult = -1;
        int result = instance.getNumMembers();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setNumMembers method, of class Event.
     */
    @Test
    public void testSetNumMembers() {
        System.out.println("setNumMembers");
        int num = 2;
        instance.setNumMembers(num);
        int expResult = 2;
        int result = instance.getNumMembers();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setPlace method, of class Event.
     */
    @Test
    public void testSetPlace() {
        System.out.println("setPlace");
        String place = "fuyuki";
        instance.setPlace(place);
        String expResult = "fuyuki";
        String result = instance.getPlace();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Event.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        try {
            Date datetest;
            datetest = new SimpleDateFormat("dd-MM-yyyy").parse("11-12-2017");
            String expResult = "Event: " + "hieu heo slayer" + " - " + datetest.getTime() + " | " + "kill hieu heo" + " | " + "quan bon";
            String result = instance.toString();
            assertEquals(expResult, result);
        } catch (ParseException ex) {
            Logger.getLogger(EventTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        //fail("The test case is a prototype.");
    }
    
}
