package io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dell
 */
public class DatabaseHelperTest {
    
    private static DatabaseHelper instance;
    
    public DatabaseHelperTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = DatabaseHelper.getInstance();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getInstance method, of class DatabaseHelper.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        DatabaseHelper expResult = DatabaseHelper.getInstance();
        DatabaseHelper result = DatabaseHelper.getInstance();
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of select method, of class DatabaseHelper.
     */
//    @Test
//    public void testSelect() {
//        System.out.println("select");
//        String select = "khoa_tenkhoa";
//        String from = "khoa";
//        String where = "khoa_ma='KH001'";
//        List listIn = new ArrayList(); listIn.add("Khoa Công Nghệ");
//        List<List<Object>> expResult = new ArrayList<>();
//        expResult.add(listIn);
//        List<List<Object>> result = instance.select(select, from, where);
//        assertEquals(expResult, result);
//        //fail("The test case is a prototype.");
//    }

    /**
     * Test of insert method, of class DatabaseHelper.
     */
//    @Test
//    public void testInsert() throws SQLException {
//        System.out.println("insert");
//        String table = "khoa";
//        String columns = "khoa_ma, khoa_tenkhoa";
//        String vals = "'KH011', 'Khoa master'";
//        //boolean expResult = false;
//        boolean resultIn = DatabaseHelper.getInstance().insert(table, columns, vals);
//        //assertEquals(expResult, result);
//        if(resultIn = true){
//            String select = "khoa_tenkhoa";
//            String from = "khoa";
//            String where = "khoa_ma='KH011'";
//            List listIn = new ArrayList(); listIn.add("Khoa master");
//            List<List<Object>> expResult = new ArrayList<>();
//            expResult.add(listIn);
//            List<List<Object>> result = DatabaseHelper.getInstance().select(select, from, where);
//            System.out.println("baka "+result);
//            String tabledel = "khoa";
//            String condition = "khoa_ma='KH011'";
//            boolean resultdel = DatabaseHelper.getInstance().delete(tabledel, condition);
//            System.out.println("boko "+result);
//            assertEquals(expResult, result);
//        }
//        //fail("The test case is a prototype.");
//    }

    /**
     * Test of getCollegeName method, of class DatabaseHelper.
     */
//    @Test
//    public void testGetCollegeName() {
//        System.out.println("getCollegeName");
//        String collegeID = "KH001";
//        String expResult = "Khoa Công Nghệ";
//        String result = instance.getCollegeName(collegeID);
//        assertEquals(expResult, result);
//        //fail("The test case is a prototype.");
//    }

    /**
     * Test of getSubjectName method, of class DatabaseHelper.
     */
//    @Test
//    public void testGetSubjectName() {
//        System.out.println("getSubjectName");
//        String subjectID = "NG001";
//        String expResult = "Khoa Học Máy Tính";
//        String result = instance.getSubjectName(subjectID);
//        assertEquals(expResult, result);
//        //fail("The test case is a prototype.");
//    }

    /**
     * Test of getNewEventID method, of class DatabaseHelper.
     */
//    @Test
//    public void testGetNewEventID() {
//        System.out.println("getNewEventID");
//        String expResult = "SK007";
//        String result = instance.getNewEventID();
//        assertEquals(expResult, result);
//        //fail("The test case is a prototype.");
//    }

    /**
     * Test of update method, of class DatabaseHelper.
     */
    /*@Test
    public void testUpdate() {
        System.out.println("update");
        String table = "";
        String condition = "";
        String[] setValue = null;
        boolean expResult = false;
        boolean result = instance.update(table, condition, setValue);
        assertEquals(expResult, result);
        //System.out.println("pig hieu");
        fail("The test case is a prototype.");
    }*/

    /**
     * Test of delete method, of class DatabaseHelper.
     */
//    @Test
//    public void testDelete() {
//        System.out.println("delete");
//        String table = "khoa";
//        String condition = "khoa_ma='KH010'";
//        boolean resultdel = DatabaseHelper.getInstance().delete(table, condition);
//        if(resultdel = true){
//            String select = "khoa_tenkhoa";
//            String from = "khoa";
//            String where = "khoa_ma='KH010'";
//            //List listIn = new ArrayList(); listIn.add(null);
//            List<List<Object>> expResult = new ArrayList<>();
//            //expResult.add(listIn);
//            List<List<Object>> result = DatabaseHelper.getInstance().select(select, from, where);
//            String tableIn = "khoa";
//            String columns = "khoa_ma, khoa_tenkhoa";
//            String vals = "'KH010', N'Khoa Thủy Sản'";
//            //boolean expResult = false;
//            boolean resultIn = DatabaseHelper.getInstance().insert(tableIn, columns, vals);
//            //assertEquals(expResult, result);
//            assertEquals(expResult, result);
//        }
//        //fail("The test case is a prototype.");
//    }


    /**
     * Test of closeConnect method, of class DatabaseHelper.
     */
    /*@Test
    public void testCloseConnect() {
        System.out.println("closeConnect");
        instance.closeConnect();
        fail("The test case is a prototype.");
    }*/
    
}
