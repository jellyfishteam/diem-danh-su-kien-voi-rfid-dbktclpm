package io.bitbucket.jsect.DiemDanhVoiRFID.TextToSpeechPackage;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author nmhillusion
 */
public class FPT_TextToSpeech implements Callback{
    private final static FPT_TextToSpeech INSTANCE = new FPT_TextToSpeech();
    public static final MediaType JSON
      = MediaType.parse("text/plain; charset=utf-8");
    
    private FPT_TextToSpeech(){}
    public static FPT_TextToSpeech getInstance(){
        return INSTANCE;
    }
    
    public void speak(String text){
        
        OkHttpClient client = new OkHttpClient();
//        //  API FPT
//        //-H "api_key: xxxxxxxxxxxxx" \
//        //-H "speed: 0" \
//        //-H "voice: male" \
//        //-H "prosody: 1" \
//        //-H "Cache-Control: no-cache"
        Request request = new Request.Builder()
                            .header("api-key", "87c4163081414973a0d1db66e87af478")
                            .header("speed", "0")
                            .header("voice", "hatieumai")
                            .header("prosody", "0")
                            .header("Cache-Control", "no-cache")
                            .post(RequestBody.create(JSON, text))
                            .url("http://api.openfpt.vn/text2speech/v4")
                            .build();
        client.newCall(request).enqueue(this);
    }
    
    @Override
    public void onFailure(Request rqst, IOException ioe) {
        Alert.getIntance()
                .setTitle("Lỗi trong đọc tên")
                .setHeaderText(ioe.getMessage())
                .show();
    }

    @Override
    public void onResponse(Response rspns) throws IOException {
        try{
            String json = rspns.body().string();
            System.out.println("success: " + json);
            
            JSONObject jsonObject = (JSONObject) JSONValue.parseWithException(json);
            
            MediaPlayer mediaPlayer = new MediaPlayer(new Media(jsonObject.get("async").toString()));
            mediaPlayer.setOnReady(() -> {
                mediaPlayer.play();
                destroy();
            });
        }catch(Throwable ex){
            System.out.println(":ERROR:");
            ex.printStackTrace();
            destroy();
        }
    }
    
    private void destroy(){
        try {
            this.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(FPT_TextToSpeech.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
