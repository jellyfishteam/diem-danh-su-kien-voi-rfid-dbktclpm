package io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage;

import javafx.beans.property.SimpleStringProperty;

/**
 * Đây là lớp cơ bản thể hiện một Giáo viên - Cán bộ dành cho việc quản lý
 * @author nmhillusion
 */
public class Teacher {
    private final SimpleStringProperty MSCB;
    private final SimpleStringProperty HoTen;
    private final SimpleStringProperty Nganh;
    private final SimpleStringProperty Khoa;
    private final SimpleStringProperty RFID;
    private final SimpleStringProperty Email;

    public Teacher(String MSCB, String HoTen, String RFID, String Email, String Nganh, String Khoa) {
        this.MSCB = new SimpleStringProperty(MSCB);
        this.HoTen = new SimpleStringProperty(HoTen);
        this.Nganh = new SimpleStringProperty(Nganh);
        this.Khoa = new SimpleStringProperty(Khoa);
        this.RFID = new SimpleStringProperty(RFID);
        this.Email = new SimpleStringProperty(Email);
    }

    public String getMSCB() {
        return MSCB.get();
    }

    public String getHoTen() {
        return HoTen.get();
    }

    public String getNganh() {
        return Nganh.get();
    }

    public String getKhoa() {
        return Khoa.get();
    }

    public String getRFID() {
        return RFID.get();
    }

    public String getEmail() {
        return Email.get();
    }

    @Override
    public String toString() {
        return "Mã CB: " + MSCB.get() + " | Họ tên: " + HoTen.get();
    }
}