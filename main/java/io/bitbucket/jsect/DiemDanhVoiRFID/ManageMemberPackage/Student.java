package io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage;

import javafx.beans.property.SimpleStringProperty;

/**
 * Đây là lớp cơ bản thể hiện một Sinh Viên dành cho việc quản lý
 *
 * @author nmhillusion
 */
public final class Student {

    private final SimpleStringProperty MSSV,
            HoTen,
            RFID,
            Lop,
            Nganh,
            Khoa,
            KhoaHoc,
            attendanceIn,
            attendanceOut;

    public Student(String MSSV, String HoTen, String RFID, String Lop, String Nganh, String Khoa, String KhoaHoc) {
        this(MSSV, HoTen, RFID, Lop, Nganh, Khoa, KhoaHoc, "", "");
    }

    public Student(String MSSV, String HoTen, String RFID, String Lop, String Nganh, String Khoa, String KhoaHoc, String attendIn, String attendOut) {
        this.MSSV = new SimpleStringProperty(MSSV);
        this.HoTen = new SimpleStringProperty(HoTen);
        this.RFID = new SimpleStringProperty(RFID);
        this.Lop = new SimpleStringProperty(Lop);
        this.Nganh = new SimpleStringProperty(Nganh);
        this.Khoa = new SimpleStringProperty(Khoa);
        this.KhoaHoc = new SimpleStringProperty(KhoaHoc);
        this.attendanceIn = new SimpleStringProperty(attendIn.length() > 0 ? "x" : "");
        this.attendanceOut = new SimpleStringProperty(attendOut.length() > 0 ? "x" : "");
    }

    public String getMSSV() {
        return MSSV.get();
    }

    public String getHoTen() {
        return HoTen.get();
    }

    public String getRFID() {
        return RFID.get();
    }

    public String getLop() {
        return Lop.get();
    }

    public String getNganh() {
        return Nganh.get();
    }

    public String getKhoa() {
        return Khoa.get();
    }

    public String getKhoaHoc() {
        return KhoaHoc.get();
    }

    public boolean isAttendanceIn() {
        return attendanceIn.get().compareTo("x") == 0;
    }

    public boolean isAttendanceOut() {
        return attendanceOut.get().compareTo("x") == 0;
    }

    public String getAttendanceIn() {
        return attendanceIn.get();
    }

    public String getAttendanceOut() {
        return attendanceOut.get();
    }

    @Override
    public String toString() {
        return "Mã SV: " + MSSV.get() + " | Họ Tên: " + HoTen.get();
    }
}