package io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXSnackbar;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Confirm;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.OnConfirmClickListener;
import io.bitbucket.jsect.DiemDanhVoiRFID.ControlPackage.ManageFragment;
import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage.ExportExcelFile;
import io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage.IOCanBo;
import io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage.IOSinhVien;
import io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage.ImportView;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.DiemDanhVoiRFID;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.FormAction;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.OnFormActionListener;
import io.bitbucket.jsect.DiemDanhVoiRFID.RFIDPackage.JsectRFIDListener;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author nmhillusion
 */
public class ManageMemberView extends ManageFragment {

    public ManageMemberView(Event event) {
        super(event);
    }

    @Override
    public void setContent() {
        TabPane root = new TabPane();

        GetMemberable contentRootStudent = new ContentRootStudent(eventInProgress.getId()),
                contentRootTeacher = new ContentRootTeacher(eventInProgress.getId());

        Tab tabStudent = new Tab("Sinh Viên", (VBox) contentRootStudent),
                tabTeacher = new Tab("Cán bộ", (VBox) contentRootTeacher);

        root.getTabs().addAll(tabStudent, tabTeacher);

        tabStudent.setClosable(false);
        tabTeacher.setClosable(false);
        tabStudent.getStyleClass().add("tab-member");
        tabTeacher.getStyleClass().add("tab-member");

        contentRootStudent.getMember(eventInProgress.getId());
        contentRootTeacher.getMember(eventInProgress.getId());

        getChildren().add(root);
    }

    interface GetMemberable {

        void getMember(String eventId);

        void searchMember(String eventId, String query);
    }

    private class ContentRootStudent extends VBox implements GetMemberable {

        private final TableView<Student> content = new TableView<>();
        private final ObservableList<Student> data = FXCollections.observableArrayList();
        private final MenuItem btnEdit = new MenuItem("Edit"), btnDelete = new MenuItem("Delete");
        private final ContextMenu contextMenu = new ContextMenu(btnEdit, btnDelete);

        public ContentRootStudent(String eventId) {
            super(5);
            setPrefHeight(DiemDanhVoiRFID.getInstance().getHeightScene());

            TextField searchBox = new TextField();
            searchBox.getStyleClass().add("search");
            searchBox.setPromptText("nhập từ tìm kiếm...");
            searchBox.setOnKeyReleased((event) -> {
                searchMember(eventId, searchBox.getText());
            });

            Button btnAdd = new Button("Thêm sinh viên tham dự"),
                    btnImport = new Button("Nhập từ file Excel");
            CheckBox checkBoxAddRFID = new JFXCheckBox("cập nhật RFID");

            this.setOnKeyReleased(new JsectRFIDListener() {
                @Override
                public void onReceiveRFID(String rfid) {
                    if (checkBoxAddRFID.isSelected()) {
                        Student student = content.getSelectionModel().getSelectedItem();

                        if (DatabaseHelper.getInstance().isRFIDExist(rfid)) {
                            Alert.getIntance()
                                    .setTitle("Đăng ký thất bại")
                                    .setHeaderText("Mã RFID này đã tồn tại trong CSDL")
                                    .show();
                            return;
                        }

                        if (student instanceof Student) {
                            boolean success = false;
                            try {
                                success = DatabaseHelper.getInstance().update(
                                        "quan_li_sinh_vien",
                                        "qlsv_masv='" + student.getMSSV() + "'",
                                        "qlsv_marfid='" + rfid + "'");
                            } catch (SQLException ex) {
                                Logger.getLogger(ManageMemberView.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            if (success) {
                                toast("Đã cập nhật RFID cho " + student, 3000);
                                getMember(eventInProgress.getId());
                            }
                        }
                    }
                }
            }.setExceptHashcodeNodes(searchBox.hashCode()));

            btnAdd.setOnAction((event) -> {
                new StudentForm(new OnFormActionListener() {
                    @Override
                    public void cancel() {

                    }

                    @Override
                    public void finish(Object data) {
                        toast(data != null ? "Đã thêm mới sinh viên tham dự " + data : "Có lỗi trong quá trình thêm sinh viên vào CSDL", 3000);
                        try {
                            DatabaseHelper.getInstance()
                                    .insert("ds_diem_danh_sinh_vien", "qlsk_mask, qlsv_masv", "'" + eventInProgress.getId() + "', '" + data.toString().trim() + "'");
                        } catch (SQLException ex) {
                            Logger.getLogger(ManageMemberView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        getMember(eventId);
                    }
                }, eventInProgress, null).display();
            });

            btnImport.setOnAction((event) -> {
                new ImportView(new OnFormActionListener() {
                    @Override
                    public void cancel() {
                    }

                    @Override
                    public void finish(Object data) {
                    }
                }, eventInProgress, (List<List<String>> data1) -> {
                    boolean success = true;
                    ListView<String> errorListView = new JFXListView<>();
                    errorListView.setPrefWidth(500);
                    errorListView.setMinHeight(200);

                    for (List<String> innerlist : data1) {
                        if ("????".equals(innerlist.get(4))) {
                            continue;
                        }
                        List<List<Object>> rawMaKhoa = DatabaseHelper.getInstance().select("KHOA_MA", "KHOA", "KHOA_TENKHOA=N'" + innerlist.get(4) + "'");
                        String maKhoa = "";
                        if (rawMaKhoa != null && rawMaKhoa.size() > 0 && rawMaKhoa.get(0) != null) {
                            maKhoa = rawMaKhoa.get(0).get(0).toString();
                        } else {
                            errorListView.getItems().add("Null Khoa của sinh viên " + innerlist.get(1));
                            continue;
                        }
                        if ("????".equals(innerlist.get(3))) {
                            continue;
                        }
                        List<List<Object>> rawMaNganh = DatabaseHelper.getInstance().select("NGANH_MA", "NGANH", "NGANH_TENNGANH=N'" + innerlist.get(3) + "'");
                        String maNganh = "";
                        if (rawMaNganh != null && rawMaNganh.size() > 0 && rawMaNganh.get(0) != null) {
                            maNganh = rawMaNganh.get(0).get(0).toString();
                        } else {
                            errorListView.getItems().add("Null Ngành của sinh viên " + innerlist.get(1));
                            continue;
                        }

                        if ("????".equals(innerlist.get(0))) {
                            continue;
                        }
                        if ("????".equals(innerlist.get(2))) {
                            continue;
                        }
                        if ("????".equals(innerlist.get(1))) {
                            continue;
                        }
                        if ("????".equals(innerlist.get(5))) {
                            continue;
                        }

                        try {
                            success = success && DatabaseHelper.getInstance()
                                    .insert("quan_li_sinh_vien",
                                            "qlsv_masv, khoa_ma, nganh_ma, qlsv_hoten, qlsv_lop, qlsv_khoahoc",
                                            "'" + innerlist.get(1) + "', '"
                                            + maKhoa + "', '"
                                            + maNganh + "', N'"
                                            + innerlist.get(0) + "', '"
                                            + innerlist.get(2) + "', '"
                                            + innerlist.get(5) + "'");
                        } catch (SQLException ex) {
                            errorListView.getItems().add("Lỗi trong thêm sinh viên " + innerlist.get(1) + ": " + ex.getLocalizedMessage());
                        }

                        try {
                            DatabaseHelper.getInstance()
                                    .insert("ds_diem_danh_sinh_vien", "qlsk_mask, qlsv_masv", "'" + eventInProgress.getId() + "', '" + innerlist.get(1).trim() + "'");
                        } catch (SQLException ex) {
                            errorListView.getItems().add("Lỗi trong thêm sinh viên " + innerlist.get(1) + ": " + ex.getLocalizedMessage());
                        }
                    }
                    if (errorListView.getItems().size() > 0) {
                        toast("Có lỗi xảy ra trong qứa trình nhập sinh viên vào CSDL");
                        new FormAction(new OnFormActionListener() {
                            @Override
                            public void cancel() {
                            }

                            @Override
                            public void finish(Object data) {

                            }
                        }, eventInProgress) {
                            @Override
                            protected void setContent() {
                                StackPane stackPane = new StackPane(errorListView);
                                root = stackPane;
                                setTitle("Bảng Danh Sách Lỗi");
                            }
                        }.display();
                    } else {
                        toast("Đã nhập danh sách sinh viên vào CSDL thành công!");
                    }
                    getMember(eventId);
                }, "Sinh Viên", new IOSinhVien("")).display();
            });

            ScrollPane scrollContent = new ScrollPane(content);
            HBox headBox = new HBox(10, btnAdd, btnImport, checkBoxAddRFID);
            headBox.setAlignment(Pos.CENTER);

            Button btnExport = new JFXButton("Xuất danh sách đăng kí");
            btnExport.setOnAction((event) -> {
                List<List<String>> dataEx = new ArrayList<>();
                dataEx.add(new ArrayList<>(Arrays.asList("Danh sách sinh viên tham gia sự kiện: " + eventInProgress.getTitle())));
                dataEx.add(new ArrayList<>(Arrays.asList("STT", "MSSV", "Họ tên", "Lớp", "Ngành", "Khoa", "Khóa học")));

                int stt = 0;
                for (Student student : data) {
                    dataEx.add(new ArrayList<>(Arrays.asList(++stt + "", student.getMSSV(), student.getHoTen(), student.getLop(), student.getNganh(), student.getKhoa(), student.getKhoaHoc())));
                }

                try {
                    File file = ExportExcelFile.getInstance()
                            .writeExcel(dataEx);

                    if (file != null) {
                        JFXSnackbar toast = new JFXSnackbar(this);
                        toast.show("Đã xuất file thành công!", "Mở file", 4000, (_event) -> {
                            try {
                                Desktop.getDesktop().open(file);
                            } catch (IOException ex) {
                                Logger.getLogger(ManageMemberView.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        });
                    }
                } catch (IOException ex) {
                    Alert.getIntance()
                            .setTitle("Lỗi xuất file")
                            .setHeaderText("Có lỗi trong xuất file: " + ex.getLocalizedMessage())
                            .show();
                }
            });

            getChildren().addAll(searchBox, headBox, scrollContent, btnExport);

            TableColumn MSSVCol = new TableColumn("MSSV");
            MSSVCol.setCellValueFactory(new PropertyValueFactory("MSSV"));

            TableColumn HoTenCol = new TableColumn("Họ tên");
            HoTenCol.setCellValueFactory(new PropertyValueFactory("HoTen"));

            TableColumn RFIDCol = new TableColumn("RFID");
            RFIDCol.setCellValueFactory(new PropertyValueFactory("RFID"));

            TableColumn LopCol = new TableColumn("Mã Lớp");
            LopCol.setCellValueFactory(new PropertyValueFactory("Lop"));

            TableColumn KhoaHocCol = new TableColumn("Khóa");
            KhoaHocCol.setCellValueFactory(new PropertyValueFactory("KhoaHoc"));

            TableColumn NganhCol = new TableColumn("Ngành");
            NganhCol.setCellValueFactory(new PropertyValueFactory("Nganh"));

            TableColumn KhoaCol = new TableColumn("Khoa");
            KhoaCol.setCellValueFactory(new PropertyValueFactory("Khoa"));

            content.getColumns().addAll(MSSVCol, HoTenCol, RFIDCol, LopCol, KhoaHocCol, NganhCol, KhoaCol);
            content.setOnMouseClicked((event) -> {
                if (event.getButton() == MouseButton.SECONDARY) {
                    setContextMenu(content.getSelectionModel().getSelectedItem());
                }
            });

            content.setOnKeyReleased((event) -> {
                if (event.getCode() == KeyCode.DELETE) {
                    Student student = content.getSelectionModel().getSelectedItem();
                    if (student != null) {
                        deleteStudent(student);
                    }
                }
            });

            contextMenu.setAutoHide(true);
            content.setContextMenu(contextMenu);
        }

        private ContextMenu setContextMenu(Student student) {
            btnEdit.setOnAction((event) -> {
                System.out.println("You want to edit: " + student);
                new StudentForm(new OnFormActionListener() {
                    @Override
                    public void cancel() {

                    }

                    @Override
                    public void finish(Object data) {
                        toast(data != null ? "Đã chỉnh sửa sinh viên tham dự " + data : "Có lỗi trong quá trình lưu sinh viên vào CSDL", 3000);
                        getMember(eventInProgress.getId());
                    }
                }, eventInProgress, student).display();
            });

            btnDelete.setOnAction((event) -> {
                deleteStudent(student);
            });

            return contextMenu;
        }

        private void deleteStudent(Student student) {
            Confirm.getIntance()
                    .setTitle("Xác nhận xóa sinh viên")
                    .setHeaderText("Bạn chắc chắn muốn xóa " + student + " khỏi sự kiện " + eventInProgress.getTitle() + "?")
                    .setListener(new OnConfirmClickListener() {
                        @Override
                        public void onPositive() {
                            boolean success = false;
                            try {
                                success = DatabaseHelper.getInstance()
                                        .delete("ds_diem_danh_sinh_vien", "qlsv_masv='" + student.getMSSV() + "' and qlsk_mask='" + eventInProgress.getId() + "'");
                            } catch (SQLException ex) {
                                toast("Lỗi xóa sinh viên: " + student + " - " + ex.getLocalizedMessage());
                            }
                            if (success) {
                                toast("Đã xóa sinh viên " + student);
                                getMember(eventInProgress.getId());
                            }
                        }

                        @Override
                        public void onNegative() {
                        }
                    }).show();
        }

        @Override
        public void getMember(String eventId) {
            // init display
            data.clear();

            //  get data form database
            DatabaseHelper.getInstance()
                    .select("d.qlsv_masv, qlsv_hoten, qlsv_marfid, qlsv_lop, nganh_ma, khoa_ma, qlsv_khoahoc",
                            "ds_diem_danh_sinh_vien as d, quan_li_sinh_vien",
                            "quan_li_sinh_vien.qlsv_masv = d.qlsv_masv and qlsk_mask='" + eventId + "'")
                    .forEach((sv) -> {
                        data.add(new Student(
                                sv.get(0).toString(), // masv
                                sv.get(1).toString(), // ho ten
                                (sv.get(2) != null ? sv.get(2).toString() : ""), // rfid
                                sv.get(3).toString(), // lop
                                DatabaseHelper.getInstance()
                                        .getSubjectName(sv.get(4).toString()), // nganh
                                DatabaseHelper.getInstance()
                                        .getCollegeName(sv.get(5).toString()), // khoa
                                sv.get(6).toString() //  khoa hoc
                        ));
                    });

            content.setItems(data);

            //update size of table
            content.refresh();
            content.setPrefSize(800, DiemDanhVoiRFID.getInstance().getHeightScene() - 200);
            content.autosize();
        }

        @Override
        public void searchMember(String eventId, String query) {
            // init display
            data.clear();

            //  get data form database
            DatabaseHelper.getInstance()
                    .select("d.qlsv_masv, qlsv_hoten, qlsv_marfid, qlsv_lop, ng.nganh_tennganh, kh.khoa_tenkhoa, qlsv_khoahoc",
                            "ds_diem_danh_sinh_vien as d, quan_li_sinh_vien as qlsv, nganh as ng, khoa as kh",
                            "qlsv.qlsv_masv = d.qlsv_masv and qlsk_mask='" + eventId + "'"
                            + " and qlsv.nganh_ma = ng.nganh_ma and qlsv.khoa_ma = kh.khoa_ma "
                            + " and ("
                            + "d.qlsv_masv like '%" + query + "%' or "
                            + "qlsv_hoten like N'%" + query + "%' or "
                            + "qlsv_marfid like '%" + query + "%' or "
                            + "qlsv_lop like '%" + query + "%' or "
                            + "ng.nganh_tennganh like N'%" + query + "%' or "
                            + "kh.khoa_tenkhoa like N'%" + query + "%'"
                            + ")")
                    .forEach((sv) -> {
                        data.add(new Student(
                                sv.get(0).toString(), // masv
                                sv.get(1).toString(), // ho ten
                                (sv.get(2) != null ? sv.get(2).toString() : ""), // rfid
                                sv.get(3).toString(), // lop
                                sv.get(4).toString(), // nganh
                                sv.get(5).toString(), // khoa
                                sv.get(6).toString()
                        ));
                    });

            content.setItems(data);

            //update size of table
            content.refresh();
            content.setPrefSize(800, DiemDanhVoiRFID.getInstance().getHeightScene() - 200);
            content.autosize();
        }
    }

    private class ContentRootTeacher extends VBox implements GetMemberable {

        private final TableView<Teacher> content = new TableView<>();
        private final ObservableList<Teacher> data = FXCollections.observableArrayList();
        private final MenuItem btnEdit = new MenuItem("Edit"), btnDelete = new MenuItem("Delete");
        private final ContextMenu contextMenu = new ContextMenu(btnEdit, btnDelete);

        public ContentRootTeacher(String eventId) {
            super(5);
            TextField searchBox = new TextField();
            searchBox.getStyleClass().add("search");
            searchBox.setPromptText("nhập từ tìm kiếm...");
            searchBox.setOnKeyReleased((event) -> {
                searchMember(eventId, searchBox.getText());
            });

            Button btnAdd = new Button("Thêm cán bộ tham dự"),
                    btnImport = new Button("Nhập từ file Excel");
            CheckBox checkBoxAddRFID = new JFXCheckBox("đồng thời cập nhật RFID");

            this.setOnKeyReleased(new JsectRFIDListener() {
                @Override
                public void onReceiveRFID(String rfid) {
                    if (checkBoxAddRFID.isSelected()) {
                        Teacher teacher = content.getSelectionModel().getSelectedItem();
                        if (teacher instanceof Teacher) {
                            boolean success = false;
                            try {
                                success = DatabaseHelper.getInstance().update(
                                        "quan_li_can_bo",
                                        "qlcb_macb='" + teacher.getMSCB() + "'",
                                        "qlcb_marfid='" + rfid + "'");
                            } catch (SQLException ex) {
                                Logger.getLogger(ManageMemberView.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            if (success) {
                                toast("Đã cập nhật RFID cho " + teacher, 3000);
                                getMember(eventInProgress.getId());
                            }
                        }
                    }
                }
            }.setExceptHashcodeNodes(searchBox.hashCode()));

            btnAdd.setOnAction((event) -> {
                new TeacherForm(new OnFormActionListener() {
                    @Override
                    public void cancel() {

                    }

                    @Override
                    public void finish(Object data) {
                        toast(data != null ? "Đã thêm mới cán bộ tham dự " + data : "Có lỗi trong quá trình lưu cán bộ vào CSDL", 3000);
                        if (data != null && data.toString().length() > 0) {
                            try {
                                DatabaseHelper.getInstance()
                                        .insert("ds_diem_danh_can_bo", "qlsk_mask, qlcb_macb", "'" + eventInProgress.getId() + "', '" + data.toString().trim() + "'");
                            } catch (SQLException ex) {
                                Logger.getLogger(ManageMemberView.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            getMember(eventId);
                        }
                    }
                }, eventInProgress, null).display();
            });

            btnImport.setOnAction((event) -> {
                new ImportView(new OnFormActionListener() {
                    @Override
                    public void cancel() {
                    }

                    @Override
                    public void finish(Object data) {
                    }
                }, eventInProgress, (List<List<String>> data1) -> {
                    boolean success = true;
                    ListView<String> errorListView = new JFXListView<>();
                    for (List<String> innerlist : data1) {
                        List<List<Object>> rawMaKhoa = DatabaseHelper.getInstance().select("KHOA_MA", "KHOA", "KHOA_TENKHOA=N'" + innerlist.get(4) + "'");
                        String maKhoa = "";
                        if (rawMaKhoa != null && rawMaKhoa.size() > 0 && rawMaKhoa.get(0) != null) {
                            maKhoa = rawMaKhoa.get(0).get(0).toString();
                        } else {
                            errorListView.getItems().add("Null Khoa của cán bộ: " + innerlist.get(1));
                            continue;
                        }
                        List<List<Object>> rawMaNganh = DatabaseHelper.getInstance().select("NGANH_MA", "NGANH", "NGANH_TENNGANH=N'" + innerlist.get(3) + "'");
                        String maNganh = "";
                        if (rawMaNganh != null && rawMaNganh.size() > 0 && rawMaNganh.get(0) != null) {
                            maNganh = rawMaNganh.get(0).get(0).toString();
                        } else {
                            errorListView.getItems().add("Null Khoa của cán bộ: " + innerlist.get(1));
                            continue;
                        }

                        try {
                            success = success && DatabaseHelper.getInstance()
                                    .insert("quan_li_can_bo",
                                            "qlcb_macb, khoa_ma, nganh_ma, qlcb_hoten, qlcb_email",
                                            "'" + innerlist.get(1) + "', '"
                                            + maKhoa + "', '"
                                            + maNganh + "', N'"
                                            + innerlist.get(0) + "', '"
                                            + innerlist.get(2) + "'");
                            DatabaseHelper.getInstance()
                                    .insert("ds_diem_danh_can_bo", "qlsk_mask, qlcb_macb", "'" + eventInProgress.getId() + "', '" + innerlist.get(1).trim() + "'");
                        } catch (SQLException ex) {
                            errorListView.getItems().add("Lỗi thêm vào CSDL: " + ex.getLocalizedMessage());
                        }

                    }
                    if (errorListView.getItems().size() > 0) {
                        toast("Có lỗi xảy ra trong qứa trình nhập cán bộ vào CSDL");
                        new FormAction(new OnFormActionListener() {
                            @Override
                            public void cancel() {

                            }

                            @Override
                            public void finish(Object data) {

                            }
                        }, eventInProgress) {
                            @Override
                            protected void setContent() {
                                StackPane stackPane = new StackPane(errorListView);
                                root = stackPane;
                            }
                        }.display();
                    } else {
                        toast("Đã nhập danh sách cán bộ vào CSDL thành công!");
                    }
                    getMember(eventId);
                }, "Cán Bộ", new IOCanBo("")).display();
            });

            ScrollPane scrollContent = new ScrollPane(content);
            HBox headBox = new HBox(10, btnAdd, btnImport, checkBoxAddRFID);
            headBox.setAlignment(Pos.CENTER);

            getChildren().addAll(searchBox, headBox, scrollContent);

            TableColumn MSSVCol = new TableColumn("MSCB");
            MSSVCol.setCellValueFactory(new PropertyValueFactory("MSCB"));

            TableColumn HoTenCol = new TableColumn("Họ tên");
            HoTenCol.setCellValueFactory(new PropertyValueFactory("HoTen"));

            TableColumn RFIDCol = new TableColumn("RFID");
            RFIDCol.setCellValueFactory(new PropertyValueFactory("RFID"));

            TableColumn EmailCol = new TableColumn("Email");
            EmailCol.setCellValueFactory(new PropertyValueFactory("Email"));

            TableColumn NganhCol = new TableColumn("Ngành");
            NganhCol.setCellValueFactory(new PropertyValueFactory("Nganh"));

            TableColumn KhoaCol = new TableColumn("Khoa");
            KhoaCol.setCellValueFactory(new PropertyValueFactory("Khoa"));

            content.getColumns().addAll(MSSVCol, HoTenCol, RFIDCol, EmailCol, NganhCol, KhoaCol);
            content.setOnMouseClicked((event) -> {
                if (event.getButton() == MouseButton.SECONDARY) {
                    setContextMenu(content.getSelectionModel().getSelectedItem());
                }
            });

            contextMenu.setAutoHide(true);
            content.setContextMenu(contextMenu);
        }

        private ContextMenu setContextMenu(Teacher teacher) {
            btnEdit.setOnAction((event) -> {
                System.out.println("You want to edit: " + teacher);
                new TeacherForm(new OnFormActionListener() {
                    @Override
                    public void cancel() {

                    }

                    @Override
                    public void finish(Object data) {
                        toast(data != null ? "Đã chỉnh sửa cán bộ tham dự " + data : "Có lỗi trong quá trình lưu cán bộ vào CSDL", 3000);
                        getMember(eventInProgress.getId());
                    }
                }, eventInProgress, teacher).display();
            });

            btnDelete.setOnAction((event) -> {
                Confirm.getIntance()
                        .setTitle("Xác nhận xóa sinh viên")
                        .setHeaderText("Bạn chắc chắn muốn xóa " + teacher + " khỏi sự kiện " + eventInProgress.getTitle() + "?")
                        .setListener(new OnConfirmClickListener() {
                            @Override
                            public void onPositive() {
                                boolean success = false;
                                try {
                                    success = DatabaseHelper.getInstance()
                                            .delete("ds_diem_danh_can_bo", "qlcb_macb='" + teacher.getMSCB() + "' and qlsk_mask='" + eventInProgress.getId() + "'");
                                } catch (SQLException ex) {
                                    toast("Có lỗi trong xóa cán bộ: " + ex.getLocalizedMessage());
                                }
                                if (success) {
                                    toast("Đã xóa sinh viên " + teacher);
                                    getMember(eventInProgress.getId());
                                }
                            }

                            @Override
                            public void onNegative() {
                            }
                        }).show();

            });

            return contextMenu;
        }

        @Override
        public void getMember(String eventId) {
            // init display
            data.clear();

            //  get data form database
            DatabaseHelper.getInstance()
                    .select("d.qlcb_macb, qlcb_hoten, qlcb_marfid, qlcb_email, nganh_ma, khoa_ma", "ds_diem_danh_can_bo as d, quan_li_can_bo", "quan_li_can_bo.qlcb_macb = d.qlcb_macb and qlsk_mask='" + eventId + "'")
                    .forEach((cb) -> {
                        data.add(new Teacher(
                                cb.get(0).toString(), // macb
                                cb.get(1).toString(), // ho ten
                                (cb.get(2) != null ? cb.get(2).toString() : ""), // rfid
                                cb.get(3).toString(), // email
                                DatabaseHelper.getInstance()
                                        .getSubjectName(cb.get(4).toString()), // nganh
                                DatabaseHelper.getInstance()
                                        .getCollegeName(cb.get(5).toString()) // khoa
                        ));
                    });

            content.setItems(data);
            //update size after loaded data
            content.refresh();
            content.setPrefSize(800, DiemDanhVoiRFID.getInstance().getHeightScene() - 200);
            content.autosize();
        }

        @Override
        public void searchMember(String eventId, String query) {
            // init display
            data.clear();

            //  get data form database
            DatabaseHelper.getInstance()
                    .select("d.qlcb_macb, qlcb_hoten, qlcb_marfid, qlcb_email, ng.nganh_tennganh, kh.khoa_tenkhoa",
                            "ds_diem_danh_can_bo as d, quan_li_can_bo as qlcb, nganh as ng, khoa as kh",
                            "qlcb.qlcb_macb = d.qlcb_macb and qlsk_mask='" + eventId + "'"
                            + " and qlcb.nganh_ma = ng.nganh_ma and qlcb.khoa_ma = kh.khoa_ma "
                            + " and ("
                            + "d.qlcb_macb like '%" + query + "%' or "
                            + "qlcb_hoten like N'%" + query + "%' or "
                            + "qlcb_marfid like '%" + query + "%' or "
                            + "qlcb_email like '%" + query + "%' or "
                            + "ng.nganh_tennganh like N'%" + query + "%' or "
                            + "kh.khoa_tenkhoa like N'%" + query + "%'"
                            + ")")
                    .forEach((cb) -> {
                        data.add(new Teacher(
                                cb.get(0).toString(), // masv
                                cb.get(1).toString(), // ho ten
                                (cb.get(2) != null ? cb.get(2).toString() : ""), // rfid
                                cb.get(3).toString(), // email
                                cb.get(4).toString(), // nganh
                                cb.get(5).toString() // khoa
                        ));
                    });

            content.setItems(data);

            //update size of table
            content.refresh();
            content.setPrefSize(800, DiemDanhVoiRFID.getInstance().getHeightScene() - 200);
            content.autosize();
        }
    }
}
