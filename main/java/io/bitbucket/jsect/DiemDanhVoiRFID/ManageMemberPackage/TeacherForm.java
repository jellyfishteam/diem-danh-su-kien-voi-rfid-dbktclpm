package io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert;
import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.FormAction;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.OnFormActionListener;
import io.bitbucket.jsect.DiemDanhVoiRFID.RFIDPackage.JsectRFIDListener;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 *
 * @author MysticFantasia
 */
public class TeacherForm extends FormAction {

    private TextField teacherIDField, nameField, emailField, RFIDField;
    private ChoiceBox<String> choiceBoxCollege, choiceBoxDepartment;
    private boolean isEditing = false;

    /**
     * Constructor for Teacher
     *
     * @param lis as parent
     * @param event as parent
     * @param teacher - null if create new or teacher if edit
     */
    public TeacherForm(OnFormActionListener lis, Event event, Teacher teacher) {
        super(lis, event);
        setTitle(":: Tạo mới một cán bộ");
        //fillInfoTeacher(teacher);
        if (teacher instanceof Teacher) {
            fillInfoTeacher(teacher);
        }
    }

    private void fillInfoTeacher(Teacher teacher) {

        isEditing = true;

        if (teacher != null) {
            setTitle("Sửa thông tin cán bộ: " + teacher.getMSCB());

            teacherIDField.setText(teacher.getMSCB());
            //  KHÔNG CHO PHÉP SỬA MSCB
            teacherIDField.setDisable(true);

            nameField.setText(teacher.getHoTen());
            emailField.setText(teacher.getEmail());
            RFIDField.setText(teacher.getRFID());

            choiceBoxCollege.getSelectionModel().select(teacher.getKhoa());
            choiceBoxDepartment.getSelectionModel().select(teacher.getNganh());
        }
    }

    @Override
    protected void setContent() {
        root = new GridPane();
        GridPane grid = (GridPane) root;
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(10);
        grid.setHgap(10);
        grid.setAlignment(Pos.TOP_CENTER);

        Text teacherID = new Text("MSCB:");
        teacherIDField = new TextField();

        Text name = new Text("Họ tên:");
        nameField = new TextField();

        Text college = new Text("Khoa/Phòng:");
        choiceBoxCollege = new ChoiceBox<>();
        Map<String, String> collegeMap = new HashMap<>();
        DatabaseHelper.getInstance()
                .select("khoa_ma, khoa_tenkhoa", "khoa", "1=1")
                .forEach((pair) -> {
                    //  khoa la ten khoa, gia tri la ten khoa de de truy xuat ma khoa sau nay
                    collegeMap.put(pair.get(1).toString(), pair.get(0).toString());
                });
        choiceBoxCollege.setItems(FXCollections.observableArrayList(collegeMap.keySet()));
        choiceBoxCollege.getSelectionModel().select(0);

        Text deparmentID = new Text("Tổ/Bộ môn:");
        choiceBoxDepartment = new ChoiceBox<>();
        Map<String, String> subjectMap = new HashMap<>();
        DatabaseHelper.getInstance()
                .select("nganh_ma, nganh_tennganh", "nganh", "khoa_ma='" + collegeMap.get(choiceBoxCollege.getSelectionModel().getSelectedItem()) + "'")
                .forEach((pair) -> {
                    subjectMap.put(pair.get(0).toString(), pair.get(1).toString());
                });
        choiceBoxDepartment.setItems(FXCollections.observableArrayList(subjectMap.values()));
        choiceBoxDepartment.getSelectionModel().select(0);
        choiceBoxCollege.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                subjectMap.clear();
                DatabaseHelper.getInstance()
                        .select("nganh_ma, nganh_tennganh", "nganh", "khoa_ma='" + collegeMap.get(newValue) + "'")
                        .forEach((pair) -> {
                            subjectMap.put(pair.get(1).toString(), pair.get(0).toString());
                        });
                choiceBoxDepartment.setItems(FXCollections.observableArrayList(subjectMap.keySet()));
                choiceBoxDepartment.getSelectionModel().select(0);
            }
        });

        Text email = new Text("Địa chỉ Email:");
        emailField = new TextField();

        Text RFID = new Text("Mã RFID:");
        RFIDField = new TextField();
        RFIDField.setOnKeyReleased(new JsectRFIDListener() {
            @Override
            public void onReceiveRFID(String rfid) {
                RFIDField.setText(rfid);
            }
        });
        RFIDField.setEditable(false);

        Button btnOk = new Button("Ok");

        Button btnCancel = new Button("Cancel");

        btnOk.setOnMouseClicked((new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (teacherIDField.getText().trim().length() == 0) {
                    Alert.getIntance()
                            .setTitle("Error")
                            .setHeaderText("mã số giáo viên không thể để trống!")
                            .show();
                    teacherIDField.requestFocus();
                } else if (nameField.getText().trim().length() == 0) {
                    Alert.getIntance()
                            .setTitle("Error")
                            .setHeaderText("Tên không được để trống!")
                            .show();
                    nameField.requestFocus();
                } else if (emailField.getText().trim().length() == 0) {
                    Alert.getIntance()
                            .setTitle("Error")
                            .setHeaderText("email không được để trống!")
                            .show();
                    emailField.requestFocus();
                } else if (!isEditing && DatabaseHelper.getInstance().isExist(teacherIDField.getText(), "CB")) {
                    Alert.getIntance()
                            .setTitle("Error")
                            .setHeaderText("Can bo da ton tai")
                            .show();
                    teacherIDField.requestFocus();
                } else if (RFIDField.getText().length() > 0 && DatabaseHelper.getInstance().isRFIDExist(RFIDField.getText())) {
                    Alert.getIntance()
                            .setTitle("Error")
                            .setHeaderText("ma rfid da ton tai")
                            .show();
                } else {
                    isDone = true;
                    try {
                        if (isEditing) {
                            System.out.println("HAHAHAHA");
                            listener.finish(DatabaseHelper.getInstance()
                                    .update("quan_li_can_bo", "qlcb_macb='" + teacherIDField.getText().trim() + "'",
                                            "qlcb_hoten=N'" + nameField.getText() + "'",
                                            "qlcb_marfid='" + RFIDField.getText() + "'",
                                            "khoa_ma='" + collegeMap.get(choiceBoxCollege.getSelectionModel().getSelectedItem()) + "'",
                                            "nganh_ma='" + subjectMap.get(choiceBoxDepartment.getSelectionModel().getSelectedItem()) + "'",
                                            "qlcb_email='" + emailField.getText() + "'")
                                    ? teacherIDField.getText()
                                    : null);
                        } else {  // add new
                            listener.finish(DatabaseHelper.getInstance()
                                    .insert("quan_li_can_bo",
                                            "qlcb_macb, khoa_ma, nganh_ma, qlcb_marfid, qlcb_hoten, qlcb_email",
                                            "'" + teacherIDField.getText() + "', '"
                                            + collegeMap.get(choiceBoxCollege.getSelectionModel().getSelectedItem()) + "', '"
                                            + subjectMap.get(choiceBoxDepartment.getSelectionModel().getSelectedItem()) + "', '"
                                            + RFIDField.getText() + "', N'"
                                            + nameField.getText() + "', '"
                                            + emailField.getText() + "'")
                                    ? teacherIDField.getText()
                                    : null);
                        }
                    } catch (SQLException ex) {
                        Alert.getIntance()
                                .setTitle("Lỗi lưu cán bộ")
                                .setHeaderText("Lỗi: " + ex.getLocalizedMessage())
                                .show();
                    }
                    close();
                }
            }

        }));

        btnCancel.setOnMouseClicked((new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                close();
            }

        }));

        grid.addRow(0, teacherID, teacherIDField);
        grid.addRow(1, name, nameField);
        grid.addRow(2, RFID, RFIDField);
        grid.addRow(3, college, choiceBoxCollege);
        grid.addRow(4, deparmentID, choiceBoxDepartment);
        grid.addRow(5, email, emailField);

        HBox actionBar = new HBox(10, btnOk, btnCancel);
        actionBar.getStyleClass().add("action-bar");
        actionBar.setAlignment(Pos.CENTER);
        grid.add(actionBar, 0, 7, 2, 1);
    }
}
