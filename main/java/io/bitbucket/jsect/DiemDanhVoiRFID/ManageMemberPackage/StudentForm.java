package io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Confirm;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.OnConfirmClickListener;
import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.FormAction;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.OnFormActionListener;
import io.bitbucket.jsect.DiemDanhVoiRFID.RFIDPackage.JsectRFIDListener;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author MysticFantasia
 */
public class StudentForm extends FormAction {

    private TextField studentIDField, nameField, classIDField, courseField, RFIDField;
    private ChoiceBox<String> choiceBoxCollege, choiceBoxSubject;
    private boolean isEditing = false;

    /**
     * constructor for student
     *
     * @param lis as parent
     * @param event as parent
     * @param student - null if create new or student if edit
     */
    public StudentForm(OnFormActionListener lis, Event event, Student student) {
        super(lis, event);
        setTitle(":: Tạo mới một sinh viên ::");
        if (student instanceof Student) {
            fillInfoStudent(student);
        }
    }

    private void fillInfoStudent(Student student) {

        isEditing = true;

        if (student != null) {
            setTitle("Sửa thông tin sinh viên: " + student.getMSSV());

            studentIDField.setText(student.getMSSV());
            // KHÔNG CHO PHÉP SỬA MSSV
            studentIDField.setDisable(true);

            nameField.setText(student.getHoTen());
            classIDField.setText(student.getLop());
            courseField.setText(student.getKhoaHoc());
            RFIDField.setText(student.getRFID());

            choiceBoxCollege.getSelectionModel().select(student.getKhoa());
            choiceBoxSubject.getSelectionModel().select(student.getNganh());

            try {
                DatabaseHelper.getInstance()
                        .update("quan_li_sinh_vien", "qlsv_masv='" + student.getMSSV() + "'", "qlsv_marfid=''");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    protected void setContent() {
        root = new GridPane();
        GridPane grid = (GridPane) root;
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(10);
        grid.setHgap(10);
        grid.setAlignment(Pos.TOP_CENTER);

        Text title = new Text("Thêm sinh viên tham gia sự kiện:");
        title.setStyle("-fx-font: normal bold 20px 'serif' ");

        Text studentID = new Text("MSSV:");
        studentIDField = new TextField();
        studentIDField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, final Boolean newValue) {
                if (!newValue) {
                    checkExistStudentId();
                }
            }
        });

        Text name = new Text("Họ tên:");
        nameField = new TextField();

        Text classID = new Text("Mã Lớp:");
        classIDField = new TextField();

        Text college = new Text("Khoa:");
        choiceBoxCollege = new ChoiceBox<>();
        Map<String, String> collegeMap = new HashMap<>();
        DatabaseHelper.getInstance()
                .select("khoa_ma, khoa_tenkhoa", "khoa", "1=1")
                .forEach((pair) -> {
                    //  WARNING: khoa la ten khoa, gia tri la ten khoa de de truy xuat ma khoa sau nay
                    collegeMap.put(pair.get(1).toString(), pair.get(0).toString());
                });
        choiceBoxCollege.setItems(FXCollections.observableArrayList(collegeMap.keySet()));
        choiceBoxCollege.getSelectionModel().select(0);

        Text subject = new Text("Ngành học:");
        choiceBoxSubject = new ChoiceBox<>();
        Map<String, String> subjectMap = new HashMap<>();
        DatabaseHelper.getInstance()
                .select("nganh_ma, nganh_tennganh", "nganh", "khoa_ma='" + collegeMap.get(choiceBoxCollege.getSelectionModel().getSelectedItem()) + "'")
                .forEach((pair) -> {
                    subjectMap.put(pair.get(1).toString(), pair.get(0).toString());
                });
        choiceBoxSubject.setItems(FXCollections.observableArrayList(subjectMap.keySet()));
        choiceBoxSubject.getSelectionModel().select(0);
        choiceBoxCollege.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                subjectMap.clear();
                DatabaseHelper.getInstance()
                        .select("nganh_ma, nganh_tennganh", "nganh", "khoa_ma='" + collegeMap.get(newValue) + "'")
                        .forEach((pair) -> {
                            subjectMap.put(pair.get(1).toString(), pair.get(0).toString());
                        });
                choiceBoxSubject.setItems(FXCollections.observableArrayList(subjectMap.keySet()));
                choiceBoxSubject.getSelectionModel().select(0);
            }
        });

        Text courseText = new Text("Khóa học:");
        courseField = new TextField();
        courseField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                courseField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });

        Text RFID = new Text("Mã RFID:");
        RFIDField = new TextField();
        RFIDField.setOnKeyReleased(new JsectRFIDListener() {
            @Override
            public void onReceiveRFID(String rfid) {
                RFIDField.setText(rfid);
            }
        });
        RFIDField.setEditable(false);

        Button btnOk = new Button("Ok");

        Button btnCancel = new Button("Cancel");

        btnOk.setOnMouseClicked((new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (studentIDField.getText().trim().length() == 0) {
                    Alert.getIntance()
                            .setTitle("Thông tin không hợp lệ")
                            .setHeaderText("MSSV không thể để trống!")
                            .show();
                    studentIDField.requestFocus();
                } else if (nameField.getText().trim().length() == 0) {
                    Alert.getIntance()
                            .setTitle("Thông tin không hợp lệ")
                            .setHeaderText("Tên không được để trống!")
                            .show();
                    nameField.requestFocus();
                } else if (classIDField.getText().trim().length() == 0) {
                    Alert.getIntance()
                            .setTitle("Thông tin không hợp lệ")
                            .setHeaderText("Mã lớp không được để trống!")
                            .show();
                    classIDField.requestFocus();
                } else if (courseField.getText().trim().length() == 0) {
                    Alert.getIntance()
                            .setTitle("Thông tin không hợp lệ")
                            .setHeaderText("Niên khóa không được để trống!")
                            .show();
                    courseField.requestFocus();
                } /*else if(true){
                    List<List<Object>> result = DatabaseHelper.getInstance().select("qlsv_masv,qlsv_marfid", "quan_li_sinh_vien","1=1");
                    for(List<Object> innerList : result){
                        if(innerList.get(0).toString().equals(studentIDField.getText()) && innerList.get(1) != null){
                            Alert.getIntance()
                                    .setTitle("Error")
                                    .setHeaderText("Sinh vien da ton tai")
                                    .show();
                        }
                        else if(innerList.get(1).toString().equals(RFIDField.getText())){
                            Alert.getIntance()
                                    .setTitle("Error")
                                    .setHeaderText("ma rfid da ton tai")
                                    .show();
                        }
                    }
                }*/ else if (!isEditing && DatabaseHelper.getInstance().isExist(studentIDField.getText(), "SV")) {
                    Alert.getIntance()
                            .setTitle("Thông tin không hợp lệ")
                            .setHeaderText("Sình viên đã tồn tại")
                            .show();
                    studentIDField.requestFocus();
                } else if (RFIDField.getText().length() > 0 && DatabaseHelper.getInstance().isRFIDExist(RFIDField.getText())) {
                    Alert.getIntance()
                            .setTitle("Thông tin không hợp lệ")
                            .setHeaderText("Mã RFID đã tồn tại")
                            .show();
                } else {
                    isDone = true;

                    try {
                        if (isEditing) {
                            listener.finish(DatabaseHelper.getInstance()
                                    .update("quan_li_sinh_vien", "qlsv_masv='" + studentIDField.getText().trim() + "'",
                                            "qlsv_hoten=N'" + nameField.getText() + "'",
                                            "qlsv_marfid='" + RFIDField.getText() + "'",
                                            "khoa_ma='" + collegeMap.get(choiceBoxCollege.getSelectionModel().getSelectedItem()) + "'",
                                            "nganh_ma='" + subjectMap.get(choiceBoxSubject.getSelectionModel().getSelectedItem()) + "'",
                                            "qlsv_lop='" + classIDField.getText() + "'",
                                            "qlsv_khoahoc='" + courseField.getText() + "'")
                                    ? studentIDField.getText()
                                    : null);
                        } else {  // add new
                            checkExistStudentId();

                            listener.finish(DatabaseHelper.getInstance()
                                    .insert("quan_li_sinh_vien",
                                            "qlsv_masv, khoa_ma, nganh_ma, qlsv_marfid, qlsv_hoten, qlsv_lop, qlsv_khoahoc",
                                            "'" + studentIDField.getText() + "', '"
                                            + collegeMap.get(choiceBoxCollege.getSelectionModel().getSelectedItem()) + "', '"
                                            + subjectMap.get(choiceBoxSubject.getSelectionModel().getSelectedItem()) + "', '"
                                            + RFIDField.getText() + "', N'"
                                            + nameField.getText() + "', '"
                                            + classIDField.getText() + "', '"
                                            + courseField.getText() + "'")
                                    ? studentIDField.getText()
                                    : null);
                        }
                    } catch (SQLException ex) {
                        Alert.getIntance()
                                .setTitle("Lưu sinh viên không thành công")
                                .setHeaderText("Lỗi: " + ex.getLocalizedMessage())
                                .show();
                    }
                    close();
                }
            }

        }));

        btnCancel.setOnMouseClicked((new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Stage stage = (Stage) btnCancel.getScene().getWindow();
                stage.close();
            }

        }));

        grid.addRow(0, studentID, studentIDField);
        grid.addRow(1, name, nameField);
        grid.addRow(2, RFID, RFIDField);
        grid.addRow(3, college, choiceBoxCollege);
        grid.addRow(4, subject, choiceBoxSubject);
        grid.addRow(5, classID, classIDField);
        grid.addRow(6, courseText, courseField);

        HBox actionBar = new HBox(10, btnOk, btnCancel);
        actionBar.getStyleClass().add("action-bar");
        actionBar.setAlignment(Pos.CENTER);
        grid.add(actionBar, 0, 7, 2, 1);
    }

    private void checkExistStudentId() {
        List<List<Object>> val = DatabaseHelper.getInstance().select("qlsv_masv, qlsv_hoten", "quan_li_sinh_vien", "qlsv_masv='" + studentIDField.getText().trim() + "'");
        if (val != null && val.size() > 0) {
            Confirm.getIntance()
                    .setTitle("Sinh viên đã tồn tại")
                    .setHeaderText("Sinh viên " + val.get(0).get(1) + " có mã số " + studentIDField.getText() + " đã tồn tại trong CSDL. Bạn có muốn thêm sinh viên đó vào sự kiện này không? Nếu không thì bạn phải đổi mã số")
                    .setListener(new OnConfirmClickListener() {
                        @Override
                        public void onPositive() {
                            listener.finish(studentIDField.getText().trim());
                            close();
                        }

                        @Override
                        public void onNegative() {
                            studentIDField.requestFocus();
                        }
                    })
                    .show();
        }
    }

}
