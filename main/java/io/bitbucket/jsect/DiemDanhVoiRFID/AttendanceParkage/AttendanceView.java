package io.bitbucket.jsect.DiemDanhVoiRFID.AttendanceParkage;

import io.bitbucket.jsect.DiemDanhVoiRFID.TextToSpeechPackage.FPT_TextToSpeech;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Confirm;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.OnConfirmClickListener;
import io.bitbucket.jsect.DiemDanhVoiRFID.ControlPackage.ControlScreen;
import io.bitbucket.jsect.DiemDanhVoiRFID.ControlPackage.ManageFragment;
import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage.ManageMemberView;
import io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage.Student;
import io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage.Teacher;
import io.bitbucket.jsect.DiemDanhVoiRFID.RFIDPackage.JsectRFIDListener;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.util.Pair;

/**
 *
 * @author nmhillusion
 */
enum ATTEND_TYPE {
    STUDENT,
    TEACHER,
    BOTH
}

public class AttendanceView extends ManageFragment {

    private boolean isAttendIn = true;
    private TableColumn IsAttendCol;

    public AttendanceView(Event event, boolean isAttendIn) {
        super(event);
        this.isAttendIn = isAttendIn;
        IsAttendCol.setCellValueFactory(new PropertyValueFactory(isAttendIn ? "attendanceIn" : "attendanceOut"));
    }

    @Override
    public void setContent() {
        TabPane root = new TabPane();

        GetMemberable contentRootStudent = new ContentRootStudent(eventInProgress.getId()),
                contentRootTeacher = new ContentRootTeacher(eventInProgress.getId());

        Tab tabStudent = new Tab("Sinh Viên", (Node) contentRootStudent),
                tabTeacher = new Tab("Cán bộ", (Node) contentRootTeacher);

        tabStudent.setClosable(false);
        tabTeacher.setClosable(false);
        tabStudent.getStyleClass().add("tab-member");
        tabTeacher.getStyleClass().add("tab-member");

        root.getTabs().addAll(tabStudent, tabTeacher);

        contentRootStudent.getMember(eventInProgress.getId());
        contentRootTeacher.getMember(eventInProgress.getId());

        System.out.println(">> attendance...");
        getChildren().setAll(root);
    }

    private Pair<String, ATTEND_TYPE> findIdByRFID(String rfid) {
        List<List<Object>> rawId = DatabaseHelper.getInstance()
                .select("qlsv_masv", "quan_li_sinh_vien", "qlsv_marfid='" + rfid + "'");
        ATTEND_TYPE type = ATTEND_TYPE.STUDENT;

        if (rawId == null || rawId.isEmpty() || rawId.get(0) == null) {
            rawId = DatabaseHelper.getInstance()
                    .select("qlcb_macb", "quan_li_can_bo", "qlcb_marfid='" + rfid + "'");
            type = ATTEND_TYPE.TEACHER;
        }
        if (rawId == null || rawId.isEmpty() || rawId.get(0) == null) {
            // TODO: Thêm giáo viên nếu không có?
            Confirm.getIntance()
                    .setTitle("Thêm thành viên mới?")
                    .setHeaderText("Thành viên này chưa tồn tại, bạn có muốn thêm mới không?")
                    .setListener(new OnConfirmClickListener() {
                        @Override
                        public void onPositive() {
                            ControlScreen.getInstance()
                                    .changeContent(new ManageMemberView(eventInProgress));
                        }

                        @Override
                        public void onNegative() {
                        }
                    })
                    .show();
            return null;
        } else {
            return new Pair<>(rawId.get(0).get(0).toString(), type);
        }
    }

    interface GetMemberable {

        void getMember(String eventId);

        void searchMember(String eventId, String query);
    }

    private boolean hasAttended(String ID) {
        List<List<Object>> sv = DatabaseHelper.getInstance()
                .select(isAttendIn?"dsddsv_vao":"dsddsv_ra", "ds_diem_danh_sinh_vien", "qlsk_mask='" + eventInProgress.getId() + "' and qlsv_masv='" + ID + "'");

        if (sv != null && sv.size() > 0 && sv.get(0) != null && sv.get(0).size() > 0 && sv.get(0).get(0) != null) {
            return true;
        }
        List<List<Object>> cb = DatabaseHelper.getInstance()
                .select(isAttendIn?"dsddcb_vao":"dsddcb_ra", "ds_diem_danh_can_bo", "qlsk_mask='" + eventInProgress.getId() + "' and qlcb_macb='" + ID + "'");

        if (cb != null && cb.size() > 0 && cb.get(0) != null && cb.get(0).size() > 0 && cb.get(0).get(0) != null) {
            return true;
        }

        return false;
    }

    private boolean attendForMember(Pair<String, ATTEND_TYPE> data) {
        if (data == null) {
            return false;
        }
        String ID = data.getKey();
        //  Không điểm danh nếu ID không xác định
        if (ID == null) {
            return false;
        }

        if (hasAttended(ID)) {
            Alert.getIntance()
                        .setTitle("Không thể điểm danh!")
                        .setHeaderText("Sinh viên này đã được điểm danh trước đó!")
                        .show();
            return false;
        }

        boolean success = false;
        String fullName = "";
        ATTEND_TYPE type = data.getValue();

        try {
            switch (type) {
                case STUDENT:
                    success = DatabaseHelper.getInstance()
                            .update("ds_diem_danh_sinh_vien",
                                    "qlsv_masv='" + ID + "' and qlsk_mask='" + eventInProgress.getId() + "'",
                                    (isAttendIn ? "dsddsv_vao='" : "dsddsv_ra='") + LocalDateTime.now() + "'");
                    if (success) {
                        fullName = DatabaseHelper.getInstance()
                                .select("qlsv_hoten", "quan_li_sinh_vien", "qlsv_masv='" + ID + "'").get(0).get(0).toString();
                    }
                    break;
                case TEACHER:
                    success = DatabaseHelper.getInstance()
                            .update("ds_diem_danh_can_bo",
                                    "qlcb_macb='" + ID + "' and qlsk_mask='" + eventInProgress.getId() + "'",
                                    (isAttendIn ? "dsddcb_vao='" : "dsddcb_ra='") + LocalDateTime.now() + "'");
                    if (success) {
                        fullName = DatabaseHelper.getInstance()
                                .select("qlcb_hoten", "quan_li_can_bo", "qlcb_macb='" + ID + "'").get(0).get(0).toString();
                    }
                    break;
                default:
                    success = DatabaseHelper.getInstance()
                            .update("ds_diem_danh_sinh_vien",
                                    "qlsv_masv='" + ID + "' and qlsk_mask='" + eventInProgress.getId() + "'",
                                    (isAttendIn ? "dsddsv_vao='" : "dsddsv_ra='") + LocalDateTime.now() + "'");

                    if (success) {
                        fullName = DatabaseHelper.getInstance()
                                .select("qlsv_hoten", "quan_li_sinh_vien", "qlsv_masv='" + ID + "'").get(0).get(0).toString();
                    } else {
                        success = DatabaseHelper.getInstance()
                                .update("ds_diem_danh_can_bo",
                                        "qlcb_macb='" + ID + "' and qlsk_mask='" + eventInProgress.getId() + "'",
                                        (isAttendIn ? "dsddcb_vao='" : "dsddcb_ra='") + LocalDateTime.now() + "'");
                        if (success) {
                            fullName = DatabaseHelper.getInstance()
                                    .select("qlcb_hoten", "quan_li_can_bo", "qlcb_macb='" + ID + "'").get(0).get(0).toString();
                        }
                    }
                    break;
            }
        } catch (SQLException ex) {
            Alert.getIntance()
                    .setTitle("Lỗi cập nhật CSDL điểm danh")
                    .setHeaderText("Lỗi: " + ex.getLocalizedMessage())
                    .show();
        }

        if (success) {
            toast("Đã điểm danh cho " + fullName + " có mã số " + ID, 3000);
            
            if (isAttendIn) {
                FPT_TextToSpeech.getInstance()
                        .speak("Chào mừng " + fullName + " đến tham dự sự kiện " + eventInProgress.getTitle());
            } else {
                FPT_TextToSpeech.getInstance()
                        .speak("Cảm ơn " + fullName + " đã đến tham dự. Hẹn gặp lại bạn và chúc một ngày tốt đẹp");
            }
        } else {
            try {
                switch (type) {
                    case STUDENT:
                        if (DatabaseHelper.getInstance()
                                .insert("ds_diem_danh_sinh_vien", "qlsv_masv, qlsk_mask", "'" + ID + "','" + eventInProgress.getId() + "'")) {
                            attendForMember(data);
                        }
                        break;
                    case TEACHER:
                        if (DatabaseHelper.getInstance()
                                .insert("ds_diem_danh_can_bo", "qlcb_macb, qlsk_mask", "'" + ID + "','" + eventInProgress.getId() + "'")) {
                            attendForMember(data);
                        }
                        break;
                    default:
                        Alert.getIntance()
                                .setTitle("Lỗi điểm danh!")
                                .setHeaderText("Không thể điểm danh cho thành viên.")
                                .show();
                }
            } catch (SQLException ex) {
                Alert.getIntance()
                        .setTitle("Lỗi thêm người tham dự lâm thời")
                        .setHeaderText("Lỗi: " + ex.getLocalizedMessage())
                        .show();
            }

        }
        return success;
    }

    private class ContentRootStudent extends VBox implements GetMemberable {

        private TableView<Student> content;
        private final ObservableList<Student> data = FXCollections.observableArrayList();
        private final MenuItem btnAttend = new MenuItem("Điểm danh");
        private final ContextMenu contextMenu = new ContextMenu(btnAttend);

        public ContentRootStudent(String eventId) {
            super(5);
            TextField searchBox = new TextField();
            searchBox.getStyleClass().add("search");
            searchBox.setPromptText("input anything to search...");
            searchBox.setOnKeyReleased((event) -> {
                searchMember(eventId, searchBox.getText());
            });

            this.setOnKeyReleased(new JsectRFIDListener() {
                @Override
                public void onReceiveRFID(String rfid) {
                    attendForMember(findIdByRFID(rfid));
                    getMember(eventInProgress.getId());
                }
            }.setExceptHashcodeNodes(searchBox.hashCode()));

            content = new TableView<>();

            ScrollPane scrollContent = new ScrollPane(content);

            this.getChildren().addAll(searchBox, scrollContent);

            IsAttendCol = new TableColumn("Đã điểm danh");

            TableColumn MSSVCol = new TableColumn("MSSV");
            MSSVCol.setCellValueFactory(new PropertyValueFactory("MSSV"));

            TableColumn HoTenCol = new TableColumn("Họ tên");
            HoTenCol.setCellValueFactory(new PropertyValueFactory("HoTen"));

            TableColumn RFIDCol = new TableColumn("RFID");
            RFIDCol.setCellValueFactory(new PropertyValueFactory("RFID"));

            TableColumn LopCol = new TableColumn("Mã Lớp");
            LopCol.setCellValueFactory(new PropertyValueFactory("Lop"));

            TableColumn NganhCol = new TableColumn("Ngành");
            NganhCol.setCellValueFactory(new PropertyValueFactory("Nganh"));

            TableColumn KhoaCol = new TableColumn("Khoa");
            KhoaCol.setCellValueFactory(new PropertyValueFactory("Khoa"));

            TableColumn KhoaHocCol = new TableColumn("Khóa học");
            KhoaHocCol.setCellValueFactory(new PropertyValueFactory("KhoaHoc"));

            content.getColumns().addAll(IsAttendCol, MSSVCol, HoTenCol, RFIDCol, LopCol, NganhCol, KhoaCol, KhoaHocCol);

            content.setOnMouseClicked((event) -> {
                Student studentToAttend = content.getSelectionModel().getSelectedItem();
                if (studentToAttend != null && event.getButton() == MouseButton.SECONDARY) {
                    setContextMenu(content.getSelectionModel().getSelectedItem());
                } else if (event.getClickCount() >= 2) {
                    if (studentToAttend != null) {
                        if ((isAttendIn && studentToAttend.isAttendanceIn())
                                || (!isAttendIn && studentToAttend.isAttendanceOut())) {
                            Alert.getIntance()
                                    .setTitle("Không thể điểm danh!")
                                    .setHeaderText("Sinh viên này đã được điểm danh trước đó!")
                                    .show();
                            return;
                        }
                        Confirm.getIntance()
                                .setTitle("Bạn muốn điểm danh?")
                                .setHeaderText("Bạn xác nhận muốn điểm danh cho: " + studentToAttend)
                                .setListener(new OnConfirmClickListener() {
                                    @Override
                                    public void onPositive() {
                                        attendForMember(new Pair<>(studentToAttend.getMSSV(), ATTEND_TYPE.STUDENT));
                                        getMember(eventId);
                                    }

                                    @Override
                                    public void onNegative() {

                                    }
                                })
                                .show();
                    }
                }
            });

            contextMenu.setAutoHide(true);
            content.setContextMenu(contextMenu);
            content.setCenterShape(true);
        }

        private ContextMenu setContextMenu(Student student) {
            btnAttend.setOnAction((event) -> {
                if ((isAttendIn && student.isAttendanceIn())
                        || (!isAttendIn && student.isAttendanceOut())) {
                    Alert.getIntance()
                            .setTitle("Không thể điểm danh!")
                            .setHeaderText("Sinh viên này đã được điểm danh trước đó!")
                            .show();
                    return;
                }
                attendForMember(new Pair<>(student.getMSSV(), ATTEND_TYPE.STUDENT));
                getMember(eventInProgress.getId());
            });

            return contextMenu;
        }

        @Override
        public void getMember(String eventId) {
            // init display
            data.clear();

            //  get data form database
            DatabaseHelper.getInstance()
                    .select("d.qlsv_masv, qlsv_hoten, qlsv_marfid, qlsv_lop, nganh_ma, khoa_ma, qlsv_khoahoc, dsddsv_vao, dsddsv_ra",
                            "ds_diem_danh_sinh_vien as d, quan_li_sinh_vien",
                            "quan_li_sinh_vien.qlsv_masv = d.qlsv_masv and qlsk_mask='" + eventId + "'")
                    .forEach((sv) -> {
                        data.add(new Student(
                                sv.get(0).toString(), // masv
                                sv.get(1).toString(), // ho ten
                                (sv.get(2) != null ? sv.get(2).toString() : ""), // rfid
                                sv.get(3).toString(), // lop
                                DatabaseHelper.getInstance()
                                        .getSubjectName(sv.get(4).toString()), // nganh
                                DatabaseHelper.getInstance()
                                        .getCollegeName(sv.get(5).toString()), // khoa
                                sv.get(6).toString(), // khoa hoc
                                (sv.get(7) != null ? sv.get(7).toString() : ""), // attend in
                                (sv.get(8) != null ? sv.get(8).toString() : "") // attend out
                        ));
                    });

            content.setItems(data);

            //  update size
            content.refresh();
            content.setPrefWidth(800);
            content.autosize();
        }

        @Override
        public void searchMember(String eventId, String query) {
            // init display
            data.clear();

            //  get data form database
            DatabaseHelper.getInstance()
                    .select("d.qlsv_masv, qlsv_hoten, qlsv_marfid, qlsv_lop, ng.nganh_tennganh, kh.khoa_tenkhoa, qlsv_khoahoc, dsddsv_vao, dsddsv_ra",
                            "ds_diem_danh_sinh_vien as d, quan_li_sinh_vien as qlsv, nganh as ng, khoa as kh",
                            "qlsv.qlsv_masv = d.qlsv_masv and qlsk_mask='" + eventId + "'"
                            + " and qlsv.nganh_ma = ng.nganh_ma and qlsv.khoa_ma = kh.khoa_ma "
                            + " and ("
                            + "d.qlsv_masv like '%" + query + "%' or "
                            + "qlsv_hoten like N'%" + query + "%' or "
                            + "qlsv_marfid like '%" + query + "%' or "
                            + "qlsv_lop like '%" + query + "%' or "
                            + "ng.nganh_tennganh like N'%" + query + "%' or "
                            + "kh.khoa_tenkhoa like N'%" + query + "%'"
                            + ")")
                    .forEach((sv) -> {
                        data.add(new Student(
                                sv.get(0).toString(), // masv
                                sv.get(1).toString(), // ho ten
                                (sv.get(2) != null ? sv.get(2).toString() : ""), // rfid
                                sv.get(3).toString(), // lop
                                sv.get(4).toString(), // nganh
                                sv.get(5).toString(), // khoa
                                sv.get(6).toString(), // khoa hoc
                                (sv.get(7) != null ? sv.get(7).toString() : ""), // attend in
                                (sv.get(8) != null ? sv.get(8).toString() : "") // attend out
                        ));
                    });

            content.setItems(data);

            //update size of table
            content.refresh();
            content.setPrefWidth(800);
            content.autosize();
        }
    }

    private class ContentRootTeacher extends VBox implements GetMemberable {

        private TableView<Teacher> content;
        private final ObservableList<Teacher> data = FXCollections.observableArrayList();
        private final MenuItem btnAttend = new MenuItem("Điểm danh");
        private final ContextMenu contextMenu = new ContextMenu(btnAttend);

        public ContentRootTeacher(String eventId) {
            super(5);
            TextField searchBox = new TextField();
            searchBox.getStyleClass().add("search");
            searchBox.setPromptText("input anything to search...");
            searchBox.setOnKeyReleased((event) -> {
                searchMember(eventId, searchBox.getText());
            });

            this.setOnKeyReleased(new JsectRFIDListener() {
                @Override
                public void onReceiveRFID(String rfid) {
                    attendForMember(findIdByRFID(rfid));
                    getMember(eventInProgress.getId());
                }
            }.setExceptHashcodeNodes(searchBox.hashCode()));

            content = new TableView<>();

            ScrollPane scrollContent = new ScrollPane(content);

            getChildren().addAll(searchBox, scrollContent);

            TableColumn MSSVCol = new TableColumn("MSCB");
            MSSVCol.setCellValueFactory(new PropertyValueFactory("MSCB"));

            TableColumn HoTenCol = new TableColumn("Họ tên");
            HoTenCol.setCellValueFactory(new PropertyValueFactory("HoTen"));

            TableColumn RFIDCol = new TableColumn("RFID");
            RFIDCol.setCellValueFactory(new PropertyValueFactory("RFID"));

            TableColumn EmailCol = new TableColumn("Email");
            EmailCol.setCellValueFactory(new PropertyValueFactory("Email"));

            TableColumn NganhCol = new TableColumn("Ngành");
            NganhCol.setCellValueFactory(new PropertyValueFactory("Nganh"));

            TableColumn KhoaCol = new TableColumn("Khoa");
            KhoaCol.setCellValueFactory(new PropertyValueFactory("Khoa"));

            content.getColumns().addAll(MSSVCol, HoTenCol, RFIDCol, EmailCol, NganhCol, KhoaCol);
            content.setOnMouseClicked((event) -> {
                if (event.getButton() == MouseButton.SECONDARY) {
                    setContextMenu(content.getSelectionModel().getSelectedItem());
                } else if (event.getClickCount() >= 2) {
                    Teacher teacher = content.getSelectionModel().getSelectedItem();
                    if (teacher != null) {
                        Confirm.getIntance()
                                .setTitle("Bạn muốn điểm danh?")
                                .setHeaderText("Bạn xác nhận muốn điểm danh cho: " + teacher)
                                .setListener(new OnConfirmClickListener() {
                                    @Override
                                    public void onPositive() {
                                        attendForMember(new Pair<>(teacher.getMSCB(), ATTEND_TYPE.TEACHER));
                                        getMember(eventId);
                                    }

                                    @Override
                                    public void onNegative() {

                                    }
                                })
                                .show();
                    }
                }
            });

            contextMenu.setAutoHide(true);
            content.setContextMenu(contextMenu);
        }

        private ContextMenu setContextMenu(Teacher teacher) {
            btnAttend.setOnAction((event) -> {
                attendForMember(new Pair<>(teacher.getMSCB(), ATTEND_TYPE.TEACHER));
                getMember(eventInProgress.getId());
            });

            return contextMenu;
        }

        @Override
        public void getMember(String eventId) {
            // init display
            data.clear();

            //  get data form database
            DatabaseHelper.getInstance()
                    .select("d.qlcb_macb, qlcb_hoten, qlcb_marfid, qlcb_email, nganh_ma, khoa_ma", "ds_diem_danh_can_bo as d, quan_li_can_bo", "quan_li_can_bo.qlcb_macb = d.qlcb_macb and qlsk_mask='" + eventId + "'")
                    .forEach((cb) -> {
                        data.add(new Teacher(
                                cb.get(0).toString(), // macb
                                cb.get(1).toString(), // ho ten
                                (cb.get(2) != null ? cb.get(2).toString() : ""), // rfid
                                cb.get(3).toString(), // email
                                DatabaseHelper.getInstance()
                                        .getSubjectName(cb.get(4).toString()), // nganh
                                DatabaseHelper.getInstance()
                                        .getCollegeName(cb.get(5).toString()) // khoa
                        ));
                    });

            content.setItems(data);
            //  update size
            content.refresh();
            content.setPrefWidth(800);
            content.autosize();
        }

        @Override
        public void searchMember(String eventId, String query) {
            // init display
            data.clear();

            //  get data form database
            DatabaseHelper.getInstance()
                    .select("d.qlcb_macb, qlcb_hoten, qlcb_marfid, qlcb_email, ng.nganh_tennganh, kh.khoa_tenkhoa",
                            "ds_diem_danh_can_bo as d, quan_li_can_bo as qlcb, nganh as ng, khoa as kh",
                            "qlcb.qlcb_macb = d.qlcb_macb and qlsk_mask='" + eventId + "'"
                            + " and qlcb.nganh_ma = ng.nganh_ma and qlcb.khoa_ma = kh.khoa_ma "
                            + " and ("
                            + "d.qlcb_macb like '%" + query + "%' or "
                            + "qlcb_hoten like N'%" + query + "%' or "
                            + "qlcb_marfid like '%" + query + "%' or "
                            + "qlcb_email like '%" + query + "%' or "
                            + "ng.nganh_tennganh like N'%" + query + "%' or "
                            + "kh.khoa_tenkhoa like N'%" + query + "%'"
                            + ")")
                    .forEach((cb) -> {
                        data.add(new Teacher(
                                cb.get(0).toString(), // masv
                                cb.get(1).toString(), // ho ten
                                (cb.get(2) != null ? cb.get(2).toString() : ""), // rfid
                                cb.get(3).toString(), // email
                                cb.get(4).toString(), // nganh
                                cb.get(5).toString() // khoa
                        ));
                    });

            content.setItems(data);

            //update size of table
            content.refresh();
            content.setPrefWidth(800);
            content.autosize();
        }
    }
}
