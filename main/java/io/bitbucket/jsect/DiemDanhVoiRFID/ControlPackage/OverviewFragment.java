package io.bitbucket.jsect.DiemDanhVoiRFID.ControlPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.DiemDanhVoiRFID;
import java.awt.Toolkit;
import java.util.Date;
import java.util.List;
import javafx.geometry.HPos;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 *
 * @author nmhillusion
 */
public class OverviewFragment extends ManageFragment{

    public OverviewFragment(Event event) {
        super(event);
    }

    @Override
    protected void setContent() {
        GridPane BOX = new GridPane();
        BOX.getStyleClass().add("right-box");
        BOX.setId("overview");
        
        List<List<Object>> selectDB = DatabaseHelper.getInstance()
                .select("qlsk_mask, qlsk_tensk, qlsk_noidung, qlsk_diadiem, qlsk_ngay, qlsk_giobd, qlsk_giokt", 
                        "quan_li_su_kien",
                        "qlsk_mask='"+eventInProgress.getId()+"'"
                );
        
        selectDB.forEach((row) -> {
            eventInProgress = new Event(
                    row.get(0).toString(), // ma
                    row.get(1).toString(), // ten
                    row.get(2).toString(), // noi dung
                    row.get(3).toString(), // dia diem
                    (Date)row.get(4),       // ngay
                    (Date)row.get(5),       // gio bd
                    (Date)row.get(6)        // gio kt
                );
        });
        
        eventInProgress.setNumMembers(Integer.parseInt(DatabaseHelper.getInstance()
                    .select("count(qlsk_mask)", "ds_diem_danh_sinh_vien", "qlsk_mask='" + eventInProgress.getId() + "'")
                    .get(0).get(0).toString()) 
                    +
                    Integer.parseInt(DatabaseHelper.getInstance()
                            .select("count(qlsk_mask)", "ds_diem_danh_can_bo", "qlsk_mask='" + eventInProgress.getId() + "'")
                            .get(0).get(0).toString()));
        
        Text contentEvent = new Text(eventInProgress.getContent());
        contentEvent.setWrappingWidth(Toolkit.getDefaultToolkit().getScreenSize().getWidth()*.8*.5*.7);
        
        BOX.addRow(0, 
                new iText ("Mã sự kiện"), new Text(eventInProgress.getId()));
        BOX.addRow(1, 
                new iText ("Tên sự kiện"), new Text(eventInProgress.getTitle()));
        BOX.addRow(2, 
                new iText ("Ngày tổ chức"), new Text(DiemDanhVoiRFID.getInstance().formatDate(eventInProgress.getDate())));
        BOX.addRow(3, 
                new iText ("Giờ vào"), new Text(eventInProgress.getTimeIn().toString()));
        BOX.addRow(4, 
                new iText ("Giờ ra"), new Text(eventInProgress.getTimeOut().toString()));
        BOX.addRow(5, 
                new iText ("Địa điểm"), new Text(eventInProgress.getPlace()));
        BOX.addRow(6, 
                new iText ("Nội dung"), contentEvent);
        BOX.addRow(7, 
                new iText ("Số người tham gia"), new Text(eventInProgress.getNumMembers() + " người."));
        
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(20);
        column1.setHalignment(HPos.RIGHT);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(80);
        column2.setHalignment(HPos.LEFT);
        BOX.getColumnConstraints().addAll(column1, column2);
        BOX.setVgap(8); BOX.setHgap(8);
        
        BOX.getChildren().forEach((child) -> {
            child.setStyle("-fx-font-size: 18px;");
        });
        
        getChildren().add(BOX);
    }
    
    class iText extends Text{
        public iText(String txt){
            super(txt);
            getStyleClass().add("header");
        }
    }
    
}