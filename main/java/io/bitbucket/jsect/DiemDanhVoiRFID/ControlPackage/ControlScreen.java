package io.bitbucket.jsect.DiemDanhVoiRFID.ControlPackage;

import com.jfoenix.controls.JFXSnackbar;
import io.bitbucket.jsect.DiemDanhVoiRFID.AttendanceParkage.AttendanceView;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Confirm;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.OnConfirmClickListener;
import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.EventForm;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.DiemDanhVoiRFID;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.OnFormActionListener;
import io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage.ManageMemberView;
import io.bitbucket.jsect.DiemDanhVoiRFID.StatisticPackage.StatisticForm;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 *
 * @author nmhillusion
 */
public class ControlScreen extends Pane {

    private final static ControlScreen INSTANCE = new ControlScreen();
    private final static StackPane content = new StackPane();
    private final static BorderPane menu = new BorderPane();
    private final static double HEIGHT_SCENE = DiemDanhVoiRFID.getInstance().getHeightScene();
    private ToggleButton btnOverview, btnManageMember, btnAttendance, btnStatics;

    private ControlScreen() {
    }

    public static ControlScreen getInstance() {
        return INSTANCE;
    }

    public ControlScreen init(Event event) {
        System.gc();

        INSTANCE.getStyleClass().add("control-scene");
        content.getStyleClass().add("right-box");

        INSTANCE.getChildren().setAll(createLeftBox(event), content);
        content.toBack();

        DiemDanhVoiRFID.getInstance().setTitle(": Quản lý sự kiện " + event.getId() + " - " + event.getTitle() + ":");

        return INSTANCE;
    }

    private BorderPane createLeftBox(Event eventInProgress) {
        VBox BOX = new VBox();

        Button btnBack = new Button("Trở về"),
                btnEdit = new Button("Sửa sự kiện"),
                btnDelete = new Button("Xóa sự kiện");

        btnOverview = new ToggleButton("Tổng quan");
        btnManageMember = new ToggleButton("Quản lý người tham dự");
        btnAttendance = new ToggleButton("Điểm danh");
        btnStatics = new ToggleButton("Thống kê");

        ToggleGroup toggleGroup = new ToggleGroup();
        btnOverview.setToggleGroup(toggleGroup);
        btnManageMember.setToggleGroup(toggleGroup);
        btnAttendance.setToggleGroup(toggleGroup);
        btnStatics.setToggleGroup(toggleGroup);
        toggleGroup.getToggles().stream().forEach((toggleButton) -> {
            toggleButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue && toggleGroup.getSelectedToggle() == null) {
                    toggleButton.setSelected(true);
                }
            });
        });

        btnOverview.getStyleClass().add("function");
        btnEdit.getStyleClass().add("function");
        btnDelete.getStyleClass().add("function");
        btnManageMember.getStyleClass().add("function");
        btnAttendance.getStyleClass().add("function");
        btnStatics.getStyleClass().add("function");

        btnBack.setStyle("-fx-background-image: url('/drawable/backIcon.png')");
        btnOverview.setStyle("-fx-background-image: url('/drawable/overviewIcon.png')");
        btnManageMember.setStyle("-fx-background-image: url('/drawable/manageIcon.png')");
        btnAttendance.setStyle("-fx-background-image: url('/drawable/attendanceIcon.png')");
        btnEdit.setStyle("-fx-background-image: url('/drawable/editIcon.png')");
        btnDelete.setStyle("-fx-background-image: url('/drawable/deleteIcon.png')");
        btnStatics.setStyle("-fx-background-image: url('/drawable/statisticsIcon.png')");

        btnBack.setId("btnBack");
        btnBack.setOnAction((event) -> {
            DiemDanhVoiRFID.getInstance().switchScene(DiemDanhVoiRFID.SceneType.MAIN, null);
            INSTANCE.getChildren().clear();
        });

        btnOverview.setOnAction((event) -> {
            changeContent(new OverviewFragment(eventInProgress));
        });

        JFXSnackbar toast = new JFXSnackbar(INSTANCE);
        btnEdit.setOnAction((event) -> {
            new EventForm(new OnFormActionListener() {
                @Override
                public void cancel() {
                }

                @Override
                public void finish(Object data) {
                    toast.show(data.toString(), 5000);
                    DiemDanhVoiRFID.getInstance().setTitle(": Quản lý sự kiện " + eventInProgress.getId() + " - " + eventInProgress.getTitle() + ":");
                }
            }, eventInProgress, 0, 0).display();
        });

        btnDelete.setOnAction((event) -> {
            Confirm.getIntance()
                    .setHeaderText("Xác nhập xóa")
                    .setTitle("Bạn thực sự muốn xóa?")
                    .setContent("Bạn chuẩn bị xóa sự kiện: " + eventInProgress.getId() + " - " + eventInProgress.getTitle())
                    .setListener(new OnConfirmClickListener() {
                        @Override
                        public void onPositive() {
                            try {
                                DatabaseHelper.getInstance()
                                        .delete("ds_diem_danh_sinh_vien", "qlsk_mask='" + eventInProgress.getId() + "'");
                                DatabaseHelper.getInstance()
                                        .delete("ds_diem_danh_can_bo", "qlsk_mask='" + eventInProgress.getId() + "'");
                                DatabaseHelper.getInstance()
                                        .delete("quan_li_su_kien", "qlsk_mask='" + eventInProgress.getId() + "'");
                                DiemDanhVoiRFID.getInstance().switchScene(DiemDanhVoiRFID.SceneType.MAIN, null);
                                DiemDanhVoiRFID.getInstance().toast("Đã xóa sự kiện");
                            } catch (Exception ex) {
                                toast.show("Có lỗi CSDL nên không thể xóa sự kiện này! " + ex.getLocalizedMessage(), 3000);
                            }
                        }

                        @Override
                        public void onNegative() {

                        }
                    })
                    .show();
        });

        btnManageMember.setOnAction((event) -> {
            changeContent(new ManageMemberView(eventInProgress));
        });

        btnStatics.setOnAction((event) -> {
            changeContent(new StatisticForm(eventInProgress));
        });

        BOX.getChildren().add(btnOverview);
        BOX.getChildren().add(btnManageMember);
        BOX.getChildren().add(btnAttendance);
        BOX.getChildren().add(btnEdit);
        BOX.getChildren().add(btnDelete);
        BOX.getChildren().add(btnStatics);

        if (eventInProgress.isPast()) {
            btnEdit.setDisable(true);
            btnDelete.setDisable(true);
            btnManageMember.setDisable(true);
        } else {
            btnStatics.setDisable(true);
        }

        /**
         * check for attendance
         */
        {
            LocalDateTime now = LocalDateTime.now(),
                    dateEvent = LocalDateTime.ofInstant(Instant.ofEpochMilli(eventInProgress.getDate().getTime()), ZoneId.systemDefault());
            if (now.getMonthValue() == dateEvent.getMonthValue()
                    && now.getDayOfMonth() == dateEvent.getDayOfMonth()
                    && now.getYear() == dateEvent.getYear()) {
                LocalDateTime timeIn = LocalDateTime.ofInstant(Instant.ofEpochMilli(eventInProgress.getTimeIn().getTime()), ZoneId.systemDefault()),
                        timeOut = LocalDateTime.ofInstant(Instant.ofEpochMilli(eventInProgress.getTimeOut().getTime()), ZoneId.systemDefault());
                int nowMinutes = now.getHour() * 60 + now.getMinute(),
                        timeInMinutes = timeIn.getHour() * 60 + timeIn.getMinute(),
                        timeOutMinutes = timeOut.getHour() * 60 + timeOut.getMinute();

                if (nowMinutes >= timeInMinutes - 15) {
                    btnStatics.setDisable(false);
                    btnEdit.setDisable(true);
                    btnDelete.setDisable(true);
                }

                if (nowMinutes >= timeOutMinutes + 15) {
                    btnManageMember.setDisable(true);
                }

                if (nowMinutes >= timeInMinutes - 15 && nowMinutes <= timeInMinutes + 15) {
                    btnAttendance.setText("Điểm danh vào");
                    btnAttendance.setOnAction((event) -> {
                        changeContent(new AttendanceView(eventInProgress, true));
                    });
                } else if (nowMinutes >= timeOutMinutes - 15 && nowMinutes <= timeOutMinutes + 15) {
                    btnAttendance.setText("Điểm danh ra");
                    btnAttendance.setOnAction((event) -> {
                        changeContent(new AttendanceView(eventInProgress, false));
                    });
                } else {
                    btnAttendance.setDisable(true);
                }
            } else {
                btnAttendance.setDisable(true);
            }
        }

        BOX.setAlignment(Pos.CENTER);

        StackPane menuContent = new StackPane(BOX);
        menuContent.setCenterShape(true);
        menuContent.setAlignment(Pos.CENTER);

        menu.getStyleClass().add("left-box");

        ImageView ctuLogo = new ImageView(getClass().getResource("/drawable/logo_ctu.gif").toExternalForm());
        ctuLogo.setFitWidth(100);
        ctuLogo.setFitHeight(100);
        ctuLogo.setEffect(new DropShadow(5, 2, 2, Color.web("#333333")));

        FadeTransition animation = new FadeTransition(Duration.INDEFINITE, ctuLogo);
        animation.setAutoReverse(true);
        animation.setFromValue(0.3);
        animation.setToValue(1.0);
        animation.setDuration(Duration.seconds(20));
        animation.setCycleCount(Animation.INDEFINITE);
        animation.play();

        HBox ctuBox = new HBox(ctuLogo);
        ctuBox.setAlignment(Pos.CENTER);

        menu.setTop(new VBox(10, btnBack, ctuBox));
        menu.setCenter(menuContent);
        Label copyright = new Label("@jsect");
        copyright.setId("copyright");
        menu.setBottom(copyright);
        menu.setPrefHeight(HEIGHT_SCENE);

        btnOverview.fire();

        return menu;
    }

    public void changeContent(Pane mContent) {
        content.getChildren().setAll(mContent);
        menu.toFront();
        content.toBack();

        if (mContent instanceof ManageMemberView) {
            btnManageMember.setSelected(true);
        } else if (mContent instanceof StatisticForm) {
            btnStatics.setSelected(true);
        } else if (mContent instanceof AttendanceView) {
            btnAttendance.setSelected(true);
        }
    }
}
