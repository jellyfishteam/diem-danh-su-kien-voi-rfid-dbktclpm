package io.bitbucket.jsect.DiemDanhVoiRFID.ControlPackage;

import com.jfoenix.controls.JFXSnackbar;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.DiemDanhVoiRFID;
import javafx.scene.layout.StackPane;

/**
 *
 * @author nmhillusion
 */
public abstract class ManageFragment extends StackPane{
    protected Event eventInProgress;
    
    public ManageFragment(Event event){
        super();
        this.eventInProgress = event;
        setContent();
        setPrefHeight(DiemDanhVoiRFID.getInstance().getHeightScene());
        getStyleClass().add("manage-fragment");
    }
    
    abstract protected void setContent();
    
    protected void toast(String msg, long time){
        JFXSnackbar toast = new JFXSnackbar(this);
        toast.show(msg, time);
    }
    
    protected void toast(String msg){
        toast(msg, 3000);
    }
}
