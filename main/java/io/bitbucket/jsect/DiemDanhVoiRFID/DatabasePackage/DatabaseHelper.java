package io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.OnFormActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nmhillusion
 */
public class DatabaseHelper {

    private final static DatabaseHelper INSTANCE = new DatabaseHelper();
    private static Connection connectDB = null;
    private static Config config;
    {
        config = getConfig(false);
    }

    private DatabaseHelper() {
    }

    /**
     * Thực hiện lấy về thể hiện Singleton cho DatabaseHelper và thiết lập kết
     * nối từ file config tương ứng
     *
     * @return thể hiện của lớp DatabaseHelper này
     */
    public static DatabaseHelper getInstance() {
        return INSTANCE;
    }

    public boolean initConnection() {
        boolean success = false;
        try {
            if (config == null || config.username == null || config.username.trim().length() < 1) {
                success = false;
            } else {
                SQLServerDataSource ds = new SQLServerDataSource();

                ds.setServerName(config.ServerName);
                ds.setPortNumber(config.port);
                ds.setDatabaseName(config.DatabaseName);
                ds.setUser(config.username);
                ds.setPassword(config.password);
                if (connectDB == null || connectDB.isClosed()) {
                    connectDB = ds.getConnection();
                    System.out.println("connected database");
                }
                success = true;
            }
        } catch (SQLException ex) {
            Alert.getIntance()
                    .setTitle("Error in connection to database")
                    .setHeaderText(ex.getLocalizedMessage())
                    .show();
            success = false;
        }
        return success;
    }
    
    public void settingDatabase() throws IOException{
        connectDB = null;
        config = getConfig(true);
    }

    /**
     * Lớp giả tạo cho việc thiết lập CSDL
     */
    public class Config {
        public String ServerName, DatabaseName, username, password, WorkSationID;
        public int port = 0;
    }
    
    /**
     * Lấy thông tin của file config tương ứng và đưa vào một thể hiện Config
     *
     * @return Đối tượng config đã có các giá trị tương ứng
     */
    private Config getConfig(boolean isEditting) {
        File f = new File(System.getProperty("user.home") + "/DiemDanhRFID.properties");
        if (!f.exists() || isEditting) {
            new ConfigDatabase(new OnFormActionListener() {
                @Override
                public void cancel() {
                }
                
                @Override
                public void finish(Object data) {
                    try {
                        f.createNewFile();
                        
                        Properties properties = new Properties();
                        Map<String, String> config = (Map<String, String>) data;
                        config.forEach((key, value) -> {
                            properties.setProperty(key, value);
                        });
                        
                        FileOutputStream fileOutputStream = new FileOutputStream(f);
                        properties.store(fileOutputStream, "store user's database config");
                        fileOutputStream.close();
                    } catch (IOException ex) {
                        Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }, null, loadFileConfig()).display();
        }

        return loadFileConfig();
    }
    
    private Config loadFileConfig(){
        Config config = new Config();
        try {
            InputStream file;
            file = new FileInputStream(System.getProperty("user.home") + "/DiemDanhRFID.properties");

            Properties properties = new Properties();
            properties.load(file);
            config.WorkSationID = properties.getProperty("workStationId");
            config.ServerName = properties.getProperty("serverName");
            config.port = properties.getProperty("port").length() > 0
                    ? Integer.valueOf(properties.getProperty("port")) : 0;
            config.username = properties.getProperty("username");
            config.password = properties.getProperty("password");
            config.DatabaseName = properties.getProperty("databaseName");
            
            file.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
            config = null;
        } catch (IOException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
            config = null;
        }
        return config;
    }

    /**
     * func thực hiện một lệnh truy vấn CSDL trực tiếp
     *
     * @param query câu lệnh sql muốn truy vấn
     * @return kết quả của sự truy vấn
     */
    private List<List<Object>> queryResult(String query) throws SQLException {
        if (null == connectDB) {
            return null;
        }

        List<List<Object>> data = new ArrayList<>();
        System.out.println("query: " + query);
        Statement s = connectDB.createStatement();
        ResultSet rs = s.executeQuery(query);
        List<String> columns = getColumnNames(rs);

        while (rs.next()) {
            List<Object> row = new ArrayList<>();
            for (String col : columns) {
                row.add(rs.getObject(col));
            }
            data.add(row);
        }
        return data;
    }

    /**
     * func thục hiện các lệnh cập nhật insert, update, delete
     *
     * @param query query sql sẽ thực hiện
     * @return có thực hiện thay đổi nào trên CSDL không
     */
    private boolean queryExec(String query) throws SQLException {
        if (null == connectDB) {
            return false;
        }

        boolean res;
        System.out.println("query: " + query);
        Statement s = connectDB.createStatement();
        res = s.executeUpdate(query) != 0;

        return res;
    }

    /**
     * func thực hiện lệnh select từ CSDL
     *
     * @param select chuỗi của những gì muốn select
     * @param from bảng muốn select
     * @param where điều kiện để select
     * @return mảng 2 chiều đại diện cho một mảng các hàng trong CSDL
     */
    public List<List<Object>> select(String select, String from, String where) {
        List<List<Object>> res = new ArrayList<>();
        try {
            res = queryResult("select " + select + " from " + from + " where " + where);
        } catch (SQLException ex) {
            Alert.getIntance()
                    .setTitle("Lỗi trong truy vấn")
                    .setHeaderText("Có lỗi trong select CSDL: " + ex)
                    .show();
        }

        return res;
    }

    //insert into table (col1, col2,...) values (val1, val2, ...) 
    /**
     * func thực hiện insert vào CSDL
     *
     * @param table tên bảng muốn thêm vào record mới
     * @param columns danh sách các cột sẽ thêm giá trị
     * @param vals danh sách các giá trị tương ứng
     * @return có thêm được hay không
     */
    public boolean insert(String table, String columns, String vals) throws SQLException {
        return queryExec("insert into " + table + "(" + columns + ") values(" + vals + ")");
    }

    public boolean update(String table, String condition, String... setValue) throws SQLException {
        String setQuery = "";
        setQuery = Arrays.asList(setValue).stream().map((t) -> t + ",").reduce(setQuery, String::concat);
        setQuery = setQuery.substring(0, setQuery.length() - 1);
        return queryExec("update " + table + " set " + setQuery + " where " + condition);
    }

    /**
     * func thực hiện xóa trong CSDL
     *
     * @param table tên bảng chứa dữ liệu sẽ xóa
     * @param condition điều kiện xóa
     * @return có xóa thành công không
     */
    public boolean delete(String table, String condition) throws SQLException {
        return queryExec("delete from " + table + " where " + condition);
    }

    /**
     * func lấy về danh sách tên của các cột trong một ResultSet
     *
     * @param rs ResultSet tương ứng để lấy các tên cột của nó
     * @return danh sách các tên cột đã lấy được
     */
    private List<String> getColumnNames(ResultSet rs) {
        List<String> result = new ArrayList<>();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                result.add(rsmd.getColumnName(i));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Lấy tên khoa từ mã khoa
     *
     * @param collegeID mã khoa
     * @return tên khoa
     */
    public String getCollegeName(String collegeID) {
        return select("khoa_tenkhoa", "khoa", "khoa_ma='" + collegeID + "'").get(0).get(0).toString();
    }

    /**
     * lấy tên ngành từ mã ngành
     *
     * @param subjectID mã ngành
     * @return tên ngành
     */
    public String getSubjectName(String subjectID) {
        return select("nganh_tennganh", "nganh", "nganh_ma='" + subjectID + "'").get(0).get(0).toString();
    }

    /**
     * Lấp đầy độ dài chuỗi với các số 0
     *
     * @param input số đầu vào
     * @param size kích thước cần đạt
     * @return chuỗi sau khi lấp đủ số 0
     */
    private String fillZero(int input, int size) {
        String res = "" + input;
        while (res.length() < size) {
            res = "0" + res;
        }
        return res;
    }

    /**
     * lấy tên ID mới của sự kiện dựa trên tất cả các sự kiện đang có
     *
     * @return ID sự kiện mới
     */
    public String getNewEventID() {
        int lastIdx = Integer.parseInt(select("max(qlsk_mask)", "quan_li_su_kien", "1=1").get(0).get(0).toString().substring(2));
        return "SK" + fillZero(lastIdx + 1, 3);
    }

    /**
     * kiem tra sv/cb co trung hay khong?
     *
     * @return gia tri true hoac false
     */
    public boolean isExist(String maSo, String callClass) {
        List<List<Object>> result;
        if (callClass.equals("SV")) {
            result = DatabaseHelper.getInstance().select("qlsv_masv,qlsv_marfid", "quan_li_sinh_vien", "1=1");
        } else {
            result = DatabaseHelper.getInstance().select("qlcb_macb,qlcb_marfid", "quan_li_can_bo", "1=1");
        }
        for (List<Object> innerList : result) {
            if (innerList.get(0).toString().equals(maSo) && innerList.get(1) != null) {
                return true;
            }
        }
        return false;
    }

    /**
     * kiem tra maRFID co trung hay khong?
     *
     * @return gia tri true hoac false
     */
    public boolean isRFIDExist(String maRFID) {
        List<List<Object>> resultsv = DatabaseHelper.getInstance().select("qlsv_marfid", "quan_li_sinh_vien", "1=1");
        for (List<Object> innerList : resultsv) {
            if (innerList.get(0) != null && innerList.get(0).toString().equals(maRFID)) {
                return true;
            }
        }
        List<List<Object>> resultcb = DatabaseHelper.getInstance().select("qlcb_marfid", "quan_li_can_bo", "1=1");
        for (List<Object> innerList : resultcb) {
            if (innerList.get(0) != null && innerList.get(0).toString().equals(maRFID)) {
                return true;
            }
        }
        return false;
    }

    /**
     * đóng kết nối tường minh cho CSDL
     */
    public void closeConnect() {
        try {
            if (connectDB != null && !connectDB.isClosed()) {
                connectDB.close();
            }
        } catch (SQLException ex) {
            System.out.println("Error in close connect database!");
        }
    }
}
