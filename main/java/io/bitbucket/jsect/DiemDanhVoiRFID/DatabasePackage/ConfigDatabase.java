package io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage;

import com.jfoenix.controls.JFXPasswordField;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.FormAction;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.OnFormActionListener;
import java.util.HashMap;
import java.util.Map;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 *
 * @author nmhillusion
 */
public class ConfigDatabase extends FormAction {

    private TextField ipWorkStationId, ipServerName, ipDatabaseName, ipPort, ipUserame;
    private JFXPasswordField ipPassword;

    public ConfigDatabase(OnFormActionListener lis, Event event, DatabaseHelper.Config config) {
        super(lis, event);

        if (config instanceof DatabaseHelper.Config) {
            ipWorkStationId.setText(config.WorkSationID);
            ipServerName.setText(config.ServerName);
            ipDatabaseName.setText(config.DatabaseName);
            ipPort.setText("" + config.port);
            ipUserame.setText(config.username);
            ipPassword.setText(config.password);
        }

        setTitle("Thiết lập kết nối CSDL");
    }

    @Override
    protected void setContent() {
        ipWorkStationId = new TextField();
        ipServerName = new TextField("NMHILLUSION-PC\\SQLEXPRESS");
        ipDatabaseName = new TextField("QLDD");
        ipPort = new TextField("1433");
        ipUserame = new TextField("sa");
        ipPassword = new JFXPasswordField();

        GridPane box = new GridPane();
        box.addRow(0, new Text("Work Station ID:"), ipWorkStationId);
        box.addRow(1, new Text("Server Name:"), ipServerName);
        box.addRow(2, new Text("DatabaseName:"), ipDatabaseName);
        box.addRow(3, new Text("Port:"), ipPort);
        box.addRow(4, new Text("username:"), ipUserame);
        ipPassword.setText("sa2008");
        box.addRow(5, new Text("password:"), ipPassword);

        Button btnOK = new Button("OK"),
                btnCancel = new Button("Cancel");

        btnOK.setOnAction((event) -> {
            Map<String, String> config = new HashMap<>();
            config.put("workStationId", ipWorkStationId.getText());
            config.put("serverName", ipServerName.getText());
            config.put("port", ipPort.getText());
            config.put("databaseName", ipDatabaseName.getText());
            config.put("username", ipUserame.getText());
            config.put("password", ipPassword.getText());

            listener.finish(config);
            close();
        });

        btnCancel.setOnAction((event) -> {
            close();
        });

        HBox actionBar = new HBox(10, btnOK, btnCancel);
        actionBar.getStyleClass().add("action-bar");
        box.add(actionBar, 0, 6, 2, 1);
        box.setPadding(new Insets(10));
        box.setHgap(10);
        box.setVgap(10);

        root = box;
    }
}
