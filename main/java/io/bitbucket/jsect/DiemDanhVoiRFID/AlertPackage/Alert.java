package io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.DiemDanhVoiRFID;
import java.util.Optional;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

/**
 *
 * @author nmhillusion
 */
public class Alert {
    private String title, headerText, content;
    private OnAlertClickListener alertClickListener;
    private final static Alert INSTANCE = new Alert();
    
    private Alert(){}
    public static Alert getIntance(){
        return INSTANCE;
    }

    public Alert setTitle(String title) {
        this.title = title;
        return this;
    }

    public Alert setHeaderText(String headerText) {
        this.headerText = headerText;
        return this;
    }

    public Alert setContent(String content) {
        this.content = content;
        return this;
    }

    public Alert setListener(OnAlertClickListener listener) {
        this.alertClickListener = listener;
        return this;
    }
    
    public void show(){
        javafx.scene.control.Alert alert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.WARNING);
        ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(DiemDanhVoiRFID.getIcon());
        alert.getDialogPane().setPrefWidth(600);

        alert.setHeaderText(headerText);
        alert.setTitle(title);
        alert.setContentText(content);
        Optional<ButtonType> res = alert.showAndWait();
        if(alertClickListener != null && res.isPresent()){
            if(res.get().getButtonData() == ButtonBar.ButtonData.OK_DONE){
                alertClickListener.onPositive();
            }
        }
    }
}