package io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage;

/**
 *
 * @author nmhillusion
 */
public interface OnConfirmClickListener {
    void onPositive();
    void onNegative();
}
