package io.bitbucket.jsect.DiemDanhVoiRFID.Login;

import com.jfoenix.controls.JFXListView;
import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.FormAction;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.OnFormActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;

/**
 *
 * @author ngong
 */
public class NhatKyDangNhap extends FormAction {

    public NhatKyDangNhap(OnFormActionListener lis, Event event) {
        super(lis, event);
    }

    @Override
    protected void setContent() {
        ListView<String> listView = new JFXListView<>();
        List<List<Object>> listLogin = DatabaseHelper.getInstance()
                .select("tknd_taikhoan, nkdn_thoigian", "nhat_ky_dang_nhap", "1=1");

        int length = listLogin != null ? listLogin.size() : 0;
        for (int i = 1; i <= length && i < 101; ++i) {
            listView.getItems().add(i + ". " + listLogin.get(length - i).get(0).toString() + " đã đăng nhập vào " + new SimpleDateFormat("HH:mm:ss dd/MM/yyyy", Locale.getDefault()).format((Date)listLogin.get(length - i).get(1)));
        }

        listView.setPrefSize(400, 500);
        ScrollPane scrollPane = new ScrollPane(listView);
        root = scrollPane;

        setTitle("Nhật ký đăng nhập");
    }
}
