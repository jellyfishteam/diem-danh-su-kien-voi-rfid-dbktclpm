package io.bitbucket.jsect.DiemDanhVoiRFID.Login;

import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert;
import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.FormAction;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.OnFormActionListener;
import java.io.IOException;
import java.util.List;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Paint;

/**
 *
 * @author ngong
 */
public class Login extends FormAction {

    private final static int dai = 400, rong = 300;
    private Label tieude, tduser, tdpass;
    private TextField user;
    private Button login, exit;
    private PasswordField pass;
    private String str = "";

    public Login(OnFormActionListener lis, Event event) {
        super(lis, event);
        setTitle("Đăng nhập hệ thống");
    }

    @Override
    protected void setContent() {
        //new title
        tieude = new Label("Điểm Danh");
        tieude.setId("lbl-login-title");
        tieude.setTextFill(Paint.valueOf("red"));

        //tieu de user
        tduser = new Label("Tên Tài Khoản: ");
        tduser.setTextFill(Paint.valueOf("green"));

        //khung user
        user = new TextField();
        user.setOnKeyReleased((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                execLogin();
            }
        });
        //tieu de pass
        tdpass = new Label("Mật Khẩu: ");
        tdpass.setTextFill(Paint.valueOf("green"));
        //khung user
        pass = new PasswordField();
        pass.setOnKeyReleased((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                execLogin();
            }
        });
        //forget
        //nut
        login = new Button("Đăng Nhập");
        login.setOnAction((event) -> {
            execLogin();
        });
        exit = new Button("Thoát");
        exit.setOnAction((event) -> {
            isDone = true;
            listener.finish(false);
            close();
            System.exit(0);
        });

        Hyperlink settingDB = new Hyperlink("> Thiết lập lại Cơ sở dữ liệu.");
        settingDB.setOnAction((event) -> {
            try {
                boolean successfulConnection = false;
                while (!successfulConnection) {
                    DatabaseHelper.getInstance().settingDatabase();
                    successfulConnection = DatabaseHelper.getInstance().initConnection();
                    if(!successfulConnection){
                        Alert.getIntance()
                                .setTitle("SQL Connection Error")
                                .setHeaderText("Thiết lập kết nối CSDL của bạn không đúng")
                                .show();
                    }
                    System.out.println(">> setting DB: " + successfulConnection);
                }
            } catch (IOException ex) {
                System.exit(0);
            }
        });

        GridPane gridPane = new GridPane();

        gridPane.add(tieude, 0, 0, 2, 1);
        gridPane.addRow(1, tduser, user);
        gridPane.addRow(2, tdpass, pass);
        gridPane.addRow(3, login, exit);
        gridPane.add(settingDB, 0, 4, 2, 1);

        gridPane.getChildren().forEach((child) -> {
            GridPane.setHalignment(child, HPos.CENTER);
        });

        gridPane.setHgap(8);
        gridPane.setVgap(8);
        gridPane.setPadding(new Insets(20));

        root = gridPane;
    }

    private void execLogin() {
        if (user.getText().compareTo(str) == 0 && pass.getText().compareTo(str) == 0) { //xet trong
            Alert.getIntance()
                    .setTitle("Lỗi đăng nhập")
                    .setHeaderText("Tên đăng nhập và mật khẩu không thể trống !")
                    .show();
        } else if (user.getText().compareTo(str) == 0) {
            Alert.getIntance()
                    .setTitle("Lỗi đăng nhập")
                    .setHeaderText("Tên tài khoản không thể trống !")
                    .show();
        } else if (pass.getText().compareTo(str) == 0) {
            Alert.getIntance()
                    .setTitle("Lỗi đăng nhập")
                    .setHeaderText("Mật khẩu không thể trống!")
                    .show();
        } else {
            boolean success = false;
            List login = DatabaseHelper.getInstance()
                    .select("*", "tai_khoan_nguoi_dung", "tknd_taikhoan='" + user.getText().trim() + "' and tknd_matkhau='" + pass.getText().trim() + "'");
            success = (login instanceof List) && (login.size() > 0);

            if (success) {
                isDone = true;
                listener.finish(user.getText().trim());
                close();
            } else {
                Alert.getIntance()
                        .setTitle("Lỗi đăng nhập")
                        .setHeaderText("Tên đăng nhập hoặc mật khẩu sai, vui lòng đăng nhập lại!")
                        .show();
            }
        }
    }
}
