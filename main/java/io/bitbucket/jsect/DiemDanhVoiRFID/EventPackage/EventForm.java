package io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTimePicker;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert;
import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.FormAction;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.OnFormActionListener;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.StringConverter;

/**
 *
 * @author MysticFantasia
 */
public class EventForm extends FormAction {

    private int year, month, day;
    private JFXDatePicker datePicker;
    private TextField mstextfield, nametextfield, placeField;
    private TextArea discriparea;
    private JFXTimePicker timePickerIn, timePickerOut;
    private boolean isEdit;

    /**
     * Hàm khởi tạo sự kiện
     *
     * @param lis bộ lắng nghe các hành động trên form
     * @param event null nếu tạo mới và khác null nếu chỉnh sửa
     * @param month giá trị tháng khởi tạo
     * @param year giá trị năm khởi tạo
     */
    public EventForm(OnFormActionListener lis, Event event, int month, int year) {
        super(lis, event);
        if (event != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(event.getDate());

            this.year = calendar.get(Calendar.YEAR);
            //  cộng 1 cho month vì JFXDatePicker có tháng từ 1 -> 12, khác với java util
            this.month = calendar.get(Calendar.MONTH) + 1;
            this.day = calendar.get(Calendar.DAY_OF_MONTH);
            fillInfo(event);
        } else {
            setTitle(":: Tạo sự kiện mới ::");
            setResizable(false);
            this.year = year;
            this.month = month;
            this.day = 1;
        }
        datePicker.setValue(LocalDate.of(this.year, this.month, this.day));
    }

    private void fillInfo(Event event) {
        isEdit = true;

        setTitle("Sửa sự kiện: " + event.getId() + " | " + event.getTitle());
        mstextfield.setText(event.getId());
        nametextfield.setText(event.getTitle());
        placeField.setText(event.getPlace());
        discriparea.setText(event.getContent());

        timePickerIn.setValue(LocalTime.parse(event.getTimeIn().toString(), DateTimeFormatter.ofPattern("HH:mm:ss")));
        timePickerOut.setValue(LocalTime.parse(event.getTimeOut().toString(), DateTimeFormatter.ofPattern("HH:mm:ss")));
    }

    @Override
    protected void setContent() {
        root = new GridPane();
        GridPane grid = (GridPane) root;
        grid.setMinSize(400, 400);
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(10);
        grid.setHgap(10);
        grid.setAlignment(Pos.TOP_CENTER);

        Text mstext = new Text("Mã sự kiện:");

        mstextfield = new TextField(DatabaseHelper.getInstance().getNewEventID());
        mstextfield.setDisable(true);

        Text nametext = new Text("Tên sự kiện:");
        nametextfield = new TextField();

        Text eventtimetext = new Text("Ngày diễn ra:");

        datePicker = new JFXDatePicker(LocalDate.now());

        datePicker.setEditable(false);
        datePicker.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return DateTimeFormatter.ofPattern("dd/MM/yyyy").format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.parse(string);
            }
        });

        Text timeintext = new Text("Điểm danh vào:");
        timePickerIn = new JFXTimePicker(LocalTime.now());
        timePickerIn.setEditable(false);

        Text timeouttext = new Text("Điểm danh ra:");
        timePickerOut = new JFXTimePicker(LocalTime.now());
        timePickerOut.setEditable(false);

        Text placeText = new Text("Địa điểm diễn ra:");
        placeField = new TextField();

        Text discriptext = new Text("Chi tiết:");
        discriparea = new TextArea();
        discriparea.setMaxWidth(250);
        discriparea.setWrapText(true);

        Button btnOK = new Button("Ok"),
                btnCancel = new Button("Cancel");

        btnOK.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (nametextfield.getText().trim().length() == 0) {
                    Alert.getIntance()
                            .setTitle("Kết quả: Thất bại")
                            .setHeaderText("Tên sự kiện không thể để trống!")
                            .show();
                    nametextfield.requestFocus();
                } else if (datePicker.getValue().isBefore(LocalDate.now())) {
                    Alert.getIntance()
                            .setTitle("Kết quả: Thất bại")
                            .setHeaderText("Không thể đặt sự kiện ở ngày trong quá khú!")
                            .show();
                    datePicker.requestFocus();
                } else if (timePickerIn.getValue().isAfter(timePickerOut.getValue().minusMinutes(30))) {
                    Alert.getIntance()
                            .setTitle("Kết quả: Thất bại")
                            .setHeaderText("Thời gian ra phải lớn hơn thời gian vào ít nhất 30 phút!")
                            .show();
                    timePickerIn.requestFocus();
                } else if (placeField.getText().trim().length() == 0) {
                    Alert.getIntance()
                            .setTitle("Kết quả: Thất bại")
                            .setHeaderText("Địa điểm diễn ra không được để trống!")
                            .show();
                    placeField.requestFocus();
                } else if (discriparea.getText().trim().length() == 0) {
                    Alert.getIntance()
                            .setTitle("Kết quả: Thất bại")
                            .setHeaderText("Phần mô tả chi tiết không thể để trống")
                            .show();
                    discriparea.requestFocus();
                } else {
                    isDone = true;
                    try {
                        if (isEdit) {
                            listener.finish(DatabaseHelper.getInstance()
                                    .update("quan_li_su_kien", "qlsk_mask='" + eventInProgress.getId() + "'",
                                            "qlsk_tensk=N'" + nametextfield.getText() + "'",
                                            "qlsk_noidung=N'" + discriparea.getText() + "'",
                                            "qlsk_diadiem=N'" + placeField.getText() + "'",
                                            "qlsk_ngay='" + datePicker.getValue().toString() + "'",
                                            "qlsk_giobd='" + timePickerIn.getValue().toString() + "'",
                                            "qlsk_giokt='" + timePickerOut.getValue().toString() + "'")
                                    ? "Đã sửa sự kiện " + eventInProgress.getId() + " thành công!"
                                    : "Có lỗi trong lúc lưu vào CSDL");
                        } else {  // add new
                            List<List<Object>> res = DatabaseHelper.getInstance()
                                    .select("qlsk_tensk, qlsk_ngay, qlsk_giobd, qlsk_giokt, qlsk_diadiem", 
                                            "quan_li_su_kien", 
                                            "qlsk_tensk='"+nametextfield.getText().trim()+"' and "
                                                    + "qlsk_ngay='"+datePicker.getValue().toString()+"' and "
                                                    + "qlsk_giobd='"+timePickerIn.getValue().toString()+"' and "
                                                    + "qlsk_giokt='"+timePickerOut.getValue().toString()+"' and "
                                                    + "qlsk_diadiem='"+placeField.getText().trim()+"'");
                            
                            if(res != null && !res.isEmpty()){
                                Alert.getIntance()
                                        .setTitle("Trùng sự kiện")
                                        .setHeaderText("Đã tồn tại sự kiện có cùng tên, thời gian và địa điểm.")
                                        .show();
                                return;
                            }
                            
                            listener.finish(DatabaseHelper.getInstance()
                                    .insert("quan_li_su_kien",
                                            "qlsk_mask, qlsk_tensk, qlsk_noidung, qlsk_diadiem, qlsk_ngay, qlsk_giobd, qlsk_giokt",
                                            "'" + mstextfield.getText() + "', N'"
                                            + nametextfield.getText() + "', N'"
                                            + discriparea.getText() + "', N'"
                                            + placeField.getText() + "', '"
                                            + datePicker.getValue().toString() + "', '"
                                            + timePickerIn.getValue().toString() + "', '"
                                            + timePickerOut.getValue().toString() + "'")
                                    ? "Đã thêm mới sự kiện thành công vào CSDL"
                                    : "Có lỗi trong lúc lưu vào CSDL");
                        }
                    } catch (SQLException ex) {
                        Alert.getIntance()
                                .setTitle("Lỗi lưu sự kiện vào CSDL")
                                .setHeaderText("Lỗi: " + ex.getLocalizedMessage())
                                .show();
                        return;
                    }
                    close();
                }
            }
        });

        btnCancel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                isDone = true;
                listener.cancel();
                close();
            }
        });

        grid.addRow(0, mstext, mstextfield);
        grid.addRow(1, nametext, nametextfield);
        grid.addRow(2, eventtimetext, datePicker);
        grid.addRow(3, timeintext, timePickerIn);
        grid.addRow(4, timeouttext, timePickerOut);
        grid.addRow(5, placeText, placeField);
        grid.addRow(6, discriptext, discriparea);

        HBox actionBar = new HBox(10, btnOK, btnCancel);
        actionBar.getStyleClass().add("action-bar");
        actionBar.setAlignment(Pos.CENTER);
        grid.add(actionBar, 0, 7, 2, 1);
    }
}
