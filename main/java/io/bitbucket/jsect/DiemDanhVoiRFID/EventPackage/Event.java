package io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage;

import java.time.LocalDate;
import java.util.Date;
import javafx.util.converter.LocalDateStringConverter;

/**
 *
 * @author nmhillusion
 */
public class Event {
    private String
            id,
            title,
            content,
            place;
    private Date date, timeIn, timeOut;
    private int numMember = -1;

    public Event(String id, String title, String content, String place, Date date, Date timeIn, Date timeOut) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.place = place;
        this.date = date;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Date getDate() {
        return date;
    }

    public String getPlace() {
        return place;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public Date getTimeIn() {
        return timeIn;
    }

    public Date getTimeOut() {
        return timeOut;
    }
    
    public int getNumMembers(){
        return numMember;
    }
    
    public void setNumMembers(int num){
        if(num >= 0) numMember = num;
    }

    public void setPlace(String place) {
        this.place = place;
    }
    
    public boolean isPast(){
        return LocalDate.parse(getDate().toString()).atStartOfDay().isBefore(LocalDate.now().atStartOfDay());
    }

    @Override
    public String toString() {
        return "Event: " + title + " - " + date.getTime() + " | " + content + " | " + place;
    }
}
