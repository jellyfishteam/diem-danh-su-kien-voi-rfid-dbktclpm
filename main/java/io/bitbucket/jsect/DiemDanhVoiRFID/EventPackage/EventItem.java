package io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.DiemDanhVoiRFID;
import java.util.Date;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

/**
 *
 * @author nmhillusion
 */
public class EventItem extends GridPane {
    private final Event event;

    //  views
    private Label lblTitle, lblTimestamp, lblPlace, lblNumReg;
    
    public EventItem(Event event, OnEventItemListener listener) {
        super();
        
        this.event = event;
        init();
        
        setOnMouseClicked((_event) -> {
            if(_event.getClickCount() > 1){
                listener.dbclick(event);
            }else{
                listener.click(event);
            }
        });
        
        setOnContextMenuRequested((_event) -> {
            listener.contextMenu(event, _event);
        });
    }
    
    private void init(){
        getStyleClass().add("event-item");
        
        setVgap(5);
        setHgap(5);
        add(new Label(event.getId().toUpperCase()), 0, 0, 1, 3);
        
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(10);
        column1.setHalignment(HPos.CENTER);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(45);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(45);
        column3.setHalignment(HPos.RIGHT);
        
        getColumnConstraints().addAll(column1, column2, column3);
        
        setTitle(event.getTitle());
        setTimestamp(event.getDate());
        setPlace(event.getPlace());
        setNumReg(event.getNumMembers());
    }
    
    private EventItem setTitle(String title) {
        lblTitle = new Label(title);
        lblTitle.setId("title");
        this.add(lblTitle, 1, 0, 2, 1);
        lblTitle.setTextFill(Color.BLUEVIOLET);
        return this;
    }
    
    private EventItem setTimestamp(Date timestamp) {
        lblTimestamp = new Label(DiemDanhVoiRFID.getInstance().formatDate(timestamp));
        lblTimestamp.setId("timestamp");
        lblTimestamp.setPadding(new Insets(0, 5, 0, 0));
        this.add(lblTimestamp, 2, 1);
        return this;
    }
    
    private EventItem setPlace(String place) {
        lblPlace = new Label(place);
        lblPlace.setId("place");
        this.add(lblPlace, 1, 1);
        return this;
    }
    
    private EventItem setNumReg(int numReg) {        
        lblNumReg = new Label("Số người tham gia: " + numReg);
        lblNumReg.setId("numReg");
        this.add(lblNumReg, 1, 2);
        return this;
    }
    
    public Event getEvent(){
        return event;
    }
    
    public EventItem setWidthView(double width) {
        setPrefWidth(width);
        return this;
    }
}
