package io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage;

import javafx.scene.input.ContextMenuEvent;

/**
 *
 * @author nmhillusion
 */

public interface OnEventItemListener{
    public void click(Event event);
    public void contextMenu(Event event, ContextMenuEvent eventmenu);
    public void dbclick(Event event);
}