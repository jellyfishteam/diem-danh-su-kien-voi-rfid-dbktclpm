package io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage;

import com.jfoenix.controls.JFXSnackbar;
import io.bitbucket.jsect.DiemDanhVoiRFID.ControlPackage.ControlScreen;
import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.EventItem;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.EventForm;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.OnEventItemListener;
import io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage.IOGoogleSheet;
import io.bitbucket.jsect.DiemDanhVoiRFID.Login.Login;
import io.bitbucket.jsect.DiemDanhVoiRFID.Login.NhatKyDangNhap;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.geometry.HPos;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.scene.control.TextField;

/**
 *
 * @author nmhillusion
 */
public class DiemDanhVoiRFID extends Application implements OnEventItemListener {

    private static DiemDanhVoiRFID INSTANCE;

    private static Dimension dimensionScreen;
    private VBox rightBox, contentLeftBox;

    private Stage stage;
    private Scene scene;
    private GridPane screenMain;

    private static Image icon;
    private static String stylePath;
    private boolean hasLogin = false;
    private String username = "";
    private int monthPresent, yearPresent;  //  tháng và năm đang lấy sự kiện để phục vụ cho việc khôi phục hiện trạng cũ

    public static enum SceneType {
        MAIN,
        CONTROL,
        FEATURE
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static DiemDanhVoiRFID getInstance() {
        return INSTANCE;
    }

    public static Image getIcon() {
        return icon;
    }

    public static String getStylePath() {
        return stylePath;
    }

    public double getHeightScene() {
        return scene.getHeight();
    }

    public double getWidthScene() {
        return scene.getWidth();
    }

    public void setTitle(String title) {
        stage.setTitle(title);
    }
    
    public String formatDate(Date date){
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            INSTANCE = this;
            stage = primaryStage;
            icon = new Image(getClass().getResource("/drawable/icon.png").toExternalForm());
            stylePath = getClass().getResource("/stylesheet/styleMain.css").toExternalForm();
            while (!DatabaseHelper.getInstance().initConnection()) {
                try {
                    DatabaseHelper.getInstance().settingDatabase();
                } catch (IOException ex) {
                    System.out.println("ERROR: " + ex.getLocalizedMessage());
                    System.exit(0);
                }
            }

            primaryStage.setOnCloseRequest((event) -> {
                DatabaseHelper.getInstance().closeConnect();
            });

            dimensionScreen = Toolkit.getDefaultToolkit().getScreenSize();

            VBox leftBox = createLeftBox();
            rightBox = new VBox(3);
            rightBox.getStyleClass().add("right-box");

            defaultWelcome();

            screenMain = new GridPane();
            screenMain.getStyleClass().add("main-scene");
            screenMain.setCenterShape(true);
            leftBox.setPrefSize(dimensionScreen.getWidth() * .4, dimensionScreen.getHeight() * .8);
            rightBox.setPrefSize(dimensionScreen.getWidth() * .4, dimensionScreen.getHeight() * .8);

            screenMain.add(leftBox, 0, 0);
            screenMain.add(rightBox, 1, 0);

            scene = new Scene(screenMain, dimensionScreen.getWidth() * 0.8, dimensionScreen.getHeight() * 0.8);
            scene.getStylesheets().add(stylePath);

            primaryStage.setTitle(":: Diem Danh voi RFID ::");

            primaryStage.setResizable(false);
            primaryStage.setWidth(dimensionScreen.getWidth() * .8);
            primaryStage.setHeight(dimensionScreen.getHeight() * .8);
            primaryStage.getIcons().add(icon);
            primaryStage.setScene(scene);

            //  Login
            while (!hasLogin) {
                new Login(new OnFormActionListener() {
                    @Override
                    public void cancel() {
                        System.exit(0);
                    }

                    @Override
                    public void finish(Object data) {
                        username = data.toString().trim();
                        hasLogin = username.length() > 0;
                    }
                }, null).display();
            }

            if (hasLogin) {
                DatabaseHelper.getInstance()
                        .insert("nhat_ky_dang_nhap", "nkdn_thoigian, tknd_taikhoan", "'" + LocalDateTime.now().toString() + "', '" + username + "'");
                JFXSnackbar toast = new JFXSnackbar(screenMain);
                toast.show("Chào mừng " + username + " đã đăng nhập. Bạn có muốn xem nhật ký không?", "Xem", 10000, (event) -> {
                    toast.unregisterSnackbarContainer(screenMain);
                    new NhatKyDangNhap(new OnFormActionListener() {
                        @Override
                        public void cancel() {
                        }

                        @Override
                        public void finish(Object data) {
                        }
                    }, null).display();
                });
            }

            primaryStage.show();
            demo();
        } catch (Exception exMain) {
            exMain.printStackTrace();
        }
    }

    private void defaultWelcome() {
        GridPane introduction = new GridPane();
        introduction.setVgap(8);
        introduction.setHgap(8);
        introduction.getStyleClass().add("introduction");

        Text tieude = new Text("ĐIỂM DANH RFID");

        tieude.setId("tieude");
        introduction.add(tieude, 0, 0, 2, 1);
        GridPane.setHalignment(tieude, HPos.CENTER);

        Text tenmon = new Text("Đảm bảo chất lượng và Kiểm thử phần mềm(CT243)");
        tenmon.setWrappingWidth(300);
        tenmon.setId("tengv");
        introduction.addRow(1, new Text("Học phần:"), tenmon);
        introduction.addRow(2, new Text(" "));

        Text tengv = new Text("PGS.TS Trần Cao Đệ");
        tengv.setId("tengv");
        introduction.addRow(3, new Text("Giáo viên hướng dẫn:"), tengv);

        introduction.addRow(4, new Text(" "));
        Text tennhom = new Text("01");
        tennhom.setId("tengv");
        introduction.addRow(5, new Text("Nhóm:"), tennhom);
        introduction.addRow(6, new Text(" "));
        introduction.addRow(7, new Text("Thành viên nhóm:"));
        Text space = new Text("");
        introduction.addRow(8, space, new Text("- Kiều Nhựt Trường B1400802\n\n"
                + "- Nguyễn Quang Phong B1400717\n\n"
                + "- Nguyễn Quang Huy Hải B1400690\n\n"
                + "- Dương Văn Lý B1400705\n\n"
                + "- Phan Thanh Liêm B1400701\n\n"
                + "- Lê Thanh Tâm B1204067\n\n"
                + "- Nguyễn Văn Âu B1400745\n\n"
                + "- Nguyễn Minh Hiếu B1400691\n\n"));
        rightBox.setAlignment(Pos.CENTER);
        rightBox.getChildren().setAll(introduction);
        introduction.setAlignment(Pos.CENTER);
    }

    private void demo() {
//        IOGoogleSheet.getInstance()
//                .getSheet("1CkvcAeg7NsfFgFQ4a1mv4m5A5kAVlKtq_OxKwW7XmOc", "sinh_vien", "a2:g112");
    }

    private VBox createLeftBox() {
        //  main hbox
        VBox result = new VBox(3);
        result.getStyleClass().add("left-box");

        //  component of box
        VBox headBox = new VBox(3);
        headBox.setId("headBox");
        headBox.setPrefHeight(40);

        ChoiceBox<String> monthEvent = new ChoiceBox<>(FXCollections.observableArrayList("Tất cả", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"));
        TextField yearEvent = new TextField();
        yearEvent.getStyleClass().add("year-input");
        yearEvent.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                yearEvent.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        TextField searchBox = new TextField();
        searchBox.getStyleClass().add("search");
        searchBox.setPromptText("nhập vài từ của tên sự kiện...");
        searchBox.setPrefWidth(dimensionScreen.getWidth() * .3);

        monthEvent.valueProperty().addListener((observable, oldValue, newValue) -> {
            int month = monthEvent.getValue().compareTo("Tất cả") != 0 ? Integer.valueOf(monthEvent.getValue()) : -1,
                    year = yearEvent.getText().trim().length() > 0 ? Integer.valueOf(yearEvent.getText().trim()) : -1;
            String query = searchBox.getText().trim();
            getEvents(month, year, query);
        });
        yearEvent.setOnKeyReleased((event) -> {
            int month = monthEvent.getValue().compareTo("Tất cả") != 0 ? Integer.valueOf(monthEvent.getValue()) : -1,
                    year = yearEvent.getText().trim().length() > 0 ? Integer.valueOf(yearEvent.getText().trim()) : -1;
            String query = searchBox.getText().trim();
            getEvents(month, year, query);
        });

        searchBox.setOnKeyReleased((event) -> {
            int month = monthEvent.getValue().compareTo("Tất cả") != 0 ? Integer.valueOf(monthEvent.getValue()) : -1,
                    year = yearEvent.getText().trim().length() > 0 ? Integer.valueOf(yearEvent.getText().trim()) : -1;
            String query = searchBox.getText().trim();
            getEvents(month, year, query);
        });

        HBox hboxDate = new HBox(8, new Label("Sự kiện trong : "), monthEvent, yearEvent),
                hboxTitle = new HBox(new Label("Tên sự kiện: "), searchBox);
        hboxDate.getStyleClass().add("headBox-item");
        hboxTitle.getStyleClass().add("headBox-item");
        headBox.getChildren().addAll(hboxDate, hboxTitle);

        //  content to display
        contentLeftBox = new VBox(2);
        int year = LocalDate.now().getYear(),
                month = LocalDate.now().getMonthValue();
        monthEvent.getSelectionModel().select(month);
        yearEvent.setText(year + "");

        getEvents(month, year, "");

        ScrollPane scrollPane = new ScrollPane(contentLeftBox);
        scrollPane.setId("listContent");
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

        result.getChildren().addAll(headBox, contentLeftBox, scrollPane);
        return result;
    }

    private void getEvents(int month, int year, String query) {
        monthPresent = month;
        yearPresent = year;

        contentLeftBox.getChildren().clear();

        Label btnAdd = new Label(" + Thêm sự kiện mới");
        btnAdd.setId("btnAddNew");
        btnAdd.setOnMouseClicked((event) -> {
            int mMonth = month == -1 ? LocalDate.now().getMonthValue() : month,
                    mYear = Math.max(year, LocalDate.now().getYear());
            new EventForm(new OnFormActionListener() {
                @Override
                public void cancel() {
                }

                @Override
                public void finish(Object data) {
                    toast(data.toString());
                    getEvents(monthPresent, yearPresent, "");
                }
            }, null, mMonth, mYear).display();
        });
        btnAdd.setPrefWidth(dimensionScreen.getWidth() * .4);
        btnAdd.setAlignment(Pos.CENTER);

        contentLeftBox.getChildren().add(
                btnAdd
        );

//        Event(id, String title, String content, Calendar date, String place)
        List<List<Object>> selectDB = DatabaseHelper.getInstance()
                .select("qlsk_mask, qlsk_tensk, qlsk_noidung, qlsk_diadiem, qlsk_ngay, qlsk_giobd, qlsk_giokt",
                        "quan_li_su_kien",
                        "(qlsk_tensk like N'%" + query + "%' or qlsk_mask like '%" + query + "%' or qlsk_diadiem like N'%" + query + "%') and "
                        + "datepart(month, qlsk_ngay) like '%" + (month != -1 ? month : "") + "%'"
                        + " and datepart(year, qlsk_ngay) like '%" + (year != -1 ? year : "") + "%'"
                );

        List<Event> arrEvents = new ArrayList<>();
        selectDB.forEach((row) -> {
            arrEvents.add(new Event(
                    row.get(0).toString(), // ma
                    row.get(1).toString(), // ten
                    row.get(2).toString(), // noi dung
                    row.get(3).toString(), // dia diem
                    (Date) row.get(4), // ngay
                    (Date) row.get(5), // gio bd
                    (Date) row.get(6) // gio kt
            )
            );
        });

        arrEvents.stream().map((e) -> {
            e.setNumMembers(Integer.parseInt(DatabaseHelper.getInstance()
                    .select("count(qlsk_mask)", "ds_diem_danh_sinh_vien", "qlsk_mask='" + e.getId() + "'")
                    .get(0).get(0).toString())
                    + Integer.parseInt(DatabaseHelper.getInstance()
                            .select("count(qlsk_mask)", "ds_diem_danh_can_bo", "qlsk_mask='" + e.getId() + "'")
                            .get(0).get(0).toString()));
            return e;
        }).forEachOrdered((e) -> {
            contentLeftBox.getChildren().add(new EventItem(e, this));
        });
    }

    private void loadEvent(Event event) {
        //  title
        Label title = new Label(event.getTitle());
        title.setPrefWidth(dimensionScreen.getWidth() * .4);
        title.getStyleClass().add("title");
        title.setTooltip(new Tooltip(event.getTitle()));

        GridPane define = new GridPane();
        define.getStyleClass().add("define");

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);
        column1.setHalignment(HPos.LEFT);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(50);
        column2.setHalignment(HPos.RIGHT);
        define.getColumnConstraints().addAll(column1, column2);

        define.add(new Label(DiemDanhVoiRFID.getInstance().formatDate(event.getDate())), 0, 0);
        define.add(new Label(event.getPlace()), 1, 0);

        Text txtContent = new Text("\t" + event.getContent());
        txtContent.setId("txtContent");
        ScrollPane content = new ScrollPane(txtContent);
        content.getStyleClass().add("content");
        content.setPrefSize(dimensionScreen.getWidth() * .4, dimensionScreen.getHeight() * .6);
        content.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        txtContent.wrappingWidthProperty().bind(content.prefWidthProperty());

        HBox footer = new HBox();
        footer.getStyleClass().add("footer");
        Button btnAction = new Button("Quản lý sự kiện");
        btnAction.setOnAction((e) -> {
            switchScene(SceneType.CONTROL, event);
        });
        footer.getChildren().add(btnAction);
        footer.setAlignment(Pos.BASELINE_RIGHT);

        rightBox.getChildren().setAll(title, define, content, footer);
    }

    @Override
    public void click(Event event) {
        loadEvent(event);
    }

    @Override
    public void contextMenu(Event event, ContextMenuEvent eventMenu) {
        //  TODO: implements content menu of event items
    }

    @Override
    public void dbclick(Event event) {
        switchScene(SceneType.CONTROL, event);
    }

    public void toast(String text, long dur) {
        JFXSnackbar toast = new JFXSnackbar(screenMain);
        toast.show(text, dur);
    }

    public void toast(String text) {
        toast(text, 10000);
    }

    public void switchScene(SceneType sceneType, Event event) {
        switch (sceneType) {
            case MAIN:
                scene.setRoot(screenMain);
                setTitle(":: Diem Danh voi RFID ::");
                getEvents(monthPresent, yearPresent, "");
                defaultWelcome();
                return;
            case CONTROL:
                scene.setRoot(ControlScreen.getInstance().init(event));
                return;
            default:
        }
    }
}
