package io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage;

/**
 *
 * @author nmhillusion
 */
public interface OnFormActionListener {
    /**
     * Công việc sẽ thực hiện nếu hủy tác vụ form
     */
    void cancel();
    /**
     * Công việc sẽ thực hiện khi đóng form
     * @param data dữ liệu kết quả sau khi đã xử lý form
     */
    void finish(Object data);
}
