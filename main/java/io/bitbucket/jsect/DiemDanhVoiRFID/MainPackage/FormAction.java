package io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Lớp dùng để thực hiện một hành động với một sự kiện nào đó
 * @author nmhillusion
 */
public abstract class FormAction extends Stage{
    protected Parent root;
    /**
     * Có thực hiện xong công việc trên form hay chưa
     */
    protected boolean isDone = false;
    /**
     * Bộ lắng nghe các sự kiện đóng form
     */
    protected OnFormActionListener listener;
    /**
     * sự kiện đang được xét
     */
    protected Event eventInProgress;
    
    public FormAction(OnFormActionListener lis, Event event){
        super();
        listener = lis;
        eventInProgress = event;
        setContent();
    }
    
    /**
     * Cài đặt View cho form này
     */
    abstract protected void setContent();
    
    /**
     * Phương thức để show form này lên, nên dùng phương thức này, không nên dùng show
     */
    public void display(){
        Scene scene = new Scene(root);
        scene.getStylesheets().add(DiemDanhVoiRFID.getStylePath());
        getIcons().add(DiemDanhVoiRFID.getIcon());
        setScene(scene);
        
        setOnCloseRequest((event) -> {
            if(!isDone) listener.cancel();
        });
        
        initModality(Modality.APPLICATION_MODAL);
        showAndWait();
    }
}