package io.bitbucket.jsect.DiemDanhVoiRFID.StatisticPackage;

import com.jfoenix.controls.JFXSnackbar;
import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert;
import io.bitbucket.jsect.DiemDanhVoiRFID.ControlPackage.ManageFragment;
import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage.IOCanBo;
import io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage.IOSinhVien;
import io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage.ManageMemberView;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 *
 * @author RiO
 */
public class StatisticForm extends ManageFragment {

    public StatisticForm(Event event) {
        super(event);
    }

    @Override
    protected void setContent() {
        String kindAttendance = "";
        getStyleClass().add("statisc-pane");
        BorderPane pnSta = new BorderPane();

        // label topic
        Label lbTopic = new Label("Sự kiện : ");
        Label lbEvent = new Label(eventInProgress.getTitle());
        lbTopic.getStyleClass().add("lbstalv1");
//        lbEvent.setMaxWidth(500);
        lbEvent.setWrapText(true);
        lbEvent.getStyleClass().add("lbstalv1");

        pnSta.setTop(new HBox(lbTopic, lbEvent));

        // vbox criteria
        ToggleButton btnPresent = new ToggleButton("Có mặt");
        btnPresent.setMinWidth(100);
        btnPresent.getStyleClass().add("button-statistic-style");

        ToggleButton btnAbsent = new ToggleButton("Vắng");
        btnAbsent.getStyleClass().add("button-statistic-style");
        btnAbsent.setMinWidth(100);

        ToggleButton btnIn = new ToggleButton("Có vào");
        btnIn.setMinWidth(100);
        btnIn.getStyleClass().add("button-statistic-style");

        ToggleButton btnOut = new ToggleButton("Có ra");
        btnOut.setMinWidth(100);
        btnOut.getStyleClass().add("button-statistic-style");

        ToggleButton btnBoth = new ToggleButton("Cả vào ra");
        btnBoth.setMinWidth(100);
        btnBoth.getStyleClass().add("button-statistic-style");

        ToggleGroup btnTogGro = new ToggleGroup();
        btnPresent.setToggleGroup(btnTogGro);
        btnAbsent.setToggleGroup(btnTogGro);
        btnIn.setToggleGroup(btnTogGro);
        btnOut.setToggleGroup(btnTogGro);
        btnBoth.setToggleGroup(btnTogGro);
//        btnTogGro.getProperties();
        btnPresent.setSelected(true);

        VBox vbCriteria = new VBox(5, btnPresent, btnAbsent, btnIn, btnOut, btnBoth);
        vbCriteria.getStyleClass().add("sta-vb-style");

        // vb tuy chon 
        Button btnExp = new Button("Xuất file");
        btnExp.setMinWidth(100);
        btnExp.getStyleClass().add("button-statistic-style");
        VBox vbOption = new VBox(5, btnExp);
        vbOption.getStyleClass().add("sta-vb-style");

        Label lbCri = new Label("Criteria:");
        Label lbOpt = new Label("Option:");
        pnSta.setLeft(new VBox(lbCri, vbCriteria, lbOpt, vbOption));

        // tab
        TabSinhVien tabSinhVien = new TabSinhVien();
        tabSinhVien.setClosable(false);
        TabCanBo tabCanBo = new TabCanBo();
        tabCanBo.setClosable(false);
        TabPane tabList = new TabPane(tabSinhVien, tabCanBo);
//        tabList.getTabs().addAll(tabSinhVien, tabCanBo);
//
        tabList.setPrefWidth(550);
        // tao bang ban dau 
        List<List<Object>> openSV = getPresentSV();
        List<List<Object>> openCB = getPresentCB();
        tabSinhVien.setData(openSV);
        tabCanBo.setData(openCB);
//        vbTabMem.getStyleClass().add("sta-vb-style");
        pnSta.setCenter(new ScrollPane(tabList));

        // btn event
        btnPresent.setOnAction((event) -> {
            btnPresent.setSelected(true);
            List<List<Object>> sv = getPresentSV();
            List<List<Object>> cb = getPresentCB();
            tabSinhVien.setData(sv);
            tabCanBo.setData(cb);
        });
        btnAbsent.setOnAction((event) -> {
            btnAbsent.setSelected(true);
            List<List<Object>> sv = getAbsentSV();
            List<List<Object>> cb = getAbsentCB();
            tabSinhVien.setData(sv);
            tabCanBo.setData(cb);
        });
        btnIn.setOnAction((event) -> {
            btnIn.setSelected(true);
            List<List<Object>> sv = getInSV();
            List<List<Object>> cb = getInCB();
            tabSinhVien.setData(sv);
            tabCanBo.setData(cb);
        });
        btnOut.setOnAction((event) -> {
            btnOut.setSelected(true);
            List<List<Object>> sv = getOutSV();
            List<List<Object>> cb = getOutCB();
            tabSinhVien.setData(sv);
            tabCanBo.setData(cb);
        });
        btnBoth.setOnAction((event) -> {
            btnBoth.setSelected(true);
            List<List<Object>> sv = getBothSV();
            List<List<Object>> cb = getBothCB();
            tabSinhVien.setData(sv);
            tabCanBo.setData(cb);
        });
        btnExp.setOnAction((event) -> {
            //variable
            String filePath = "";
            String fileName = "";
            //load data
            int tabIndex = tabList.getSelectionModel().getSelectedIndex();
            List<List<String>> rs = null;
            if (tabIndex == 0) {
                rs = tabSinhVien.getExportData();
            } else if (tabIndex == 1) {
                rs = tabCanBo.getExportData();
            }
            //test data null
            rs.forEach((row) -> {
                System.out.println(row);
            });
            //get address
            FileChooser fc = new FileChooser();
            fc.setTitle("Chọn nơi lưu file");
            fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("over Excel 2007", "*.xlsx"), new FileChooser.ExtensionFilter("Excel 2003", "*.xls"));
            File file = fc.showSaveDialog(null);
            if (file == null) {
                System.out.println("Hủy export");
            } else {
                String url = file.getAbsolutePath();
                filePath = url.substring(0, url.lastIndexOf(File.separator) + 1);
                fileName = url.substring(url.lastIndexOf(File.separator) + 1, url.length());
                System.out.println("File Path : " + filePath);
                System.out.println("File Name : " + fileName);
            }
            //export
            Boolean exportResult = false;
            if (tabIndex == 0) {
                IOSinhVien sv = new IOSinhVien(filePath);
                exportResult = sv.exportFile(rs, fileName);
            } else if (tabIndex == 1) {
                IOCanBo cb = new IOCanBo(filePath);
                exportResult = cb.exportFile(rs, fileName);
            }
            //export result
            if (exportResult) {
                JFXSnackbar toast = new JFXSnackbar(this);
                toast.show("Đã xuất file thành công!", "Mở file", 4000, (_event) -> {
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException ex) {
                        Logger.getLogger(ManageMemberView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            } else {
                Alert.getIntance()
                        .setTitle("Thông báo")
                        .setHeaderText("Xuất file thất bại !!!")
                        .show();
            }

        });

        getChildren().setAll(pnSta);
    }

    // lay danh sach diem danh sinh vien                     // eventInProgress.getId()  //"SK003"
    private List<List<Object>> getPresentSV() {
        String select = "QL.QLSV_MASV,QLSV_HOTEN, QLSV_LOP,NGANH_MA,KHOA_MA, QLSV_KHOAHOC";
        String from = "QUAN_LI_SINH_VIEN AS QL,DS_DIEM_DANH_SINH_VIEN AS DS";
        String where = "QL.QLSV_MASV=DS.QLSV_MASV AND (DS.DSDDSV_VAO IS NOT NULL or DS.DSDDSV_RA IS NOT NULL) AND DS.QLSK_MASK='" + eventInProgress.getId() + "'";
        return DatabaseHelper.getInstance().select(select, from, where);
    }

    private List<List<Object>> getAbsentSV() {
        String select = "QL.QLSV_MASV,QLSV_HOTEN, QLSV_LOP,NGANH_MA,KHOA_MA, QLSV_KHOAHOC";
        String from = "QUAN_LI_SINH_VIEN AS QL,DS_DIEM_DANH_SINH_VIEN AS DS";
        String where = "QL.QLSV_MASV=DS.QLSV_MASV AND (DS.DSDDSV_VAO IS NULL AND DS.DSDDSV_RA IS NULL) AND DS.QLSK_MASK='" + eventInProgress.getId() + "'";

        return DatabaseHelper.getInstance().select(select, from, where);
    }

    private List<List<Object>> getInSV() {
        String select = "QL.QLSV_MASV,QLSV_HOTEN, QLSV_LOP,NGANH_MA,KHOA_MA, QLSV_KHOAHOC";
        String from = "QUAN_LI_SINH_VIEN AS QL,DS_DIEM_DANH_SINH_VIEN AS DS";
        String where = "QL.QLSV_MASV=DS.QLSV_MASV AND (DS.DSDDSV_VAO IS NOT NULL AND DS.DSDDSV_RA IS NULL) AND DS.QLSK_MASK='" + eventInProgress.getId() + "'";

        return DatabaseHelper.getInstance().select(select, from, where);
    }

    private List<List<Object>> getOutSV() {
        String select = "QL.QLSV_MASV,QLSV_HOTEN, QLSV_LOP,NGANH_MA,KHOA_MA, QLSV_KHOAHOC";
        String from = "QUAN_LI_SINH_VIEN AS QL,DS_DIEM_DANH_SINH_VIEN AS DS";
        String where = "QL.QLSV_MASV=DS.QLSV_MASV AND (DS.DSDDSV_VAO IS NULL AND DS.DSDDSV_RA IS NOT NULL) AND DS.QLSK_MASK='" + eventInProgress.getId() + "'";

        return DatabaseHelper.getInstance().select(select, from, where);
    }

    private List<List<Object>> getBothSV() {
        String select = "QL.QLSV_MASV,QLSV_HOTEN, QLSV_LOP,NGANH_MA,KHOA_MA, QLSV_KHOAHOC";
        String from = "QUAN_LI_SINH_VIEN AS QL,DS_DIEM_DANH_SINH_VIEN AS DS";
        String where = "QL.QLSV_MASV=DS.QLSV_MASV AND (DS.DSDDSV_VAO IS NOT NULL AND DS.DSDDSV_RA IS NOT NULL) AND DS.QLSK_MASK='" + eventInProgress.getId() + "'";

        return DatabaseHelper.getInstance().select(select, from, where);
    }

    //lay danh sach diem danh can bo
    private List<List<Object>> getPresentCB() {
        String select = "QL.QLCB_MACB,QLCB_HOTEN, QLCB_EMAIL,NGANH_MA,KHOA_MA";
        String from = "QUAN_LI_CAN_BO AS QL,DS_DIEM_DANH_CAN_BO AS DS";
        String where = "QL.QLCB_MACB=DS.QLCB_MACB AND (DS.DSDDCB_VAO IS NOT NULL or DS.DSDDCB_RA IS NOT NULL) AND DS.QLSK_MASK='" + eventInProgress.getId() + "'";
        return DatabaseHelper.getInstance().select(select, from, where);
    }

    private List<List<Object>> getAbsentCB() {
        String select = "QL.QLCB_MACB,QLCB_HOTEN, QLCB_EMAIL,NGANH_MA,KHOA_MA";
        String from = "QUAN_LI_CAN_BO AS QL,DS_DIEM_DANH_CAN_BO AS DS";
        String where = "QL.QLCB_MACB=DS.QLCB_MACB AND (DS.DSDDCB_VAO IS NULL AND DS.DSDDCB_RA IS NULL) AND DS.QLSK_MASK='" + eventInProgress.getId() + "'";
        return DatabaseHelper.getInstance().select(select, from, where);
    }

    private List<List<Object>> getInCB() {
        String select = "QL.QLCB_MACB,QLCB_HOTEN, QLCB_EMAIL,NGANH_MA,KHOA_MA";
        String from = "QUAN_LI_CAN_BO AS QL,DS_DIEM_DANH_CAN_BO AS DS";
        String where = "QL.QLCB_MACB=DS.QLCB_MACB AND (DS.DSDDCB_VAO IS NOT NULL AND DS.DSDDCB_RA IS NULL) AND DS.QLSK_MASK='" + eventInProgress.getId() + "'";
        return DatabaseHelper.getInstance().select(select, from, where);
    }

    private List<List<Object>> getOutCB() {
        String select = "QL.QLCB_MACB,QLCB_HOTEN, QLCB_EMAIL,NGANH_MA,KHOA_MA";
        String from = "QUAN_LI_CAN_BO AS QL,DS_DIEM_DANH_CAN_BO AS DS";
        String where = "QL.QLCB_MACB=DS.QLCB_MACB AND (DS.DSDDCB_VAO IS NULL AND DS.DSDDCB_RA IS NOT NULL) AND DS.QLSK_MASK='" + eventInProgress.getId() + "'";
        return DatabaseHelper.getInstance().select(select, from, where);
    }

    private List<List<Object>> getBothCB() {
        String select = "QL.QLCB_MACB,QLCB_HOTEN, QLCB_EMAIL,NGANH_MA,KHOA_MA";
        String from = "QUAN_LI_CAN_BO AS QL,DS_DIEM_DANH_CAN_BO AS DS";
        String where = "QL.QLCB_MACB=DS.QLCB_MACB AND (DS.DSDDCB_VAO IS NOT NULL AND DS.DSDDCB_RA IS NOT NULL) AND DS.QLSK_MASK='" + eventInProgress.getId() + "'";
        return DatabaseHelper.getInstance().select(select, from, where);
    }
}
