package io.bitbucket.jsect.DiemDanhVoiRFID.StatisticPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage.Student;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author RiO
 */
public class TabSinhVien extends Tab {

    private final TableView<Student> content = new TableView<>();
    private final ObservableList<Student> data = FXCollections.observableArrayList();
    private List<List<String>> exportList = null;

    public TabSinhVien() {
        super("Sinh Viên");
        getStyleClass().add("tab-member");

        ScrollPane scrollContent = new ScrollPane(content);

        TableColumn MSSVCol = new TableColumn("MSSV");
        MSSVCol.setCellValueFactory(new PropertyValueFactory("MSSV"));

        TableColumn HoTenCol = new TableColumn("Họ tên");
        HoTenCol.setCellValueFactory(new PropertyValueFactory("HoTen"));

        TableColumn LopCol = new TableColumn("Lớp");
        LopCol.setCellValueFactory(new PropertyValueFactory("Lop"));

        TableColumn KhoaHocCol = new TableColumn("Khóa");
        KhoaHocCol.setCellValueFactory(new PropertyValueFactory("KhoaHoc"));

        TableColumn NganhCol = new TableColumn("Ngành");
        NganhCol.setCellValueFactory(new PropertyValueFactory("Nganh"));

        TableColumn KhoaCol = new TableColumn("Khoa");
        KhoaCol.setCellValueFactory(new PropertyValueFactory("Khoa"));

        content.getColumns().addAll(MSSVCol, HoTenCol, LopCol, KhoaHocCol, NganhCol, KhoaCol);
        setContent(content);
    }

    public void setData(List<List<Object>> list) {

        // init display
        data.clear();
        //  get data form database
        list.forEach((sv) -> {
            
            data.add(new Student(
                    sv.get(0).toString(), // maSV
                    sv.get(1).toString(), // ho ten
                    "",
                    sv.get(2).toString(), // lop
                    DatabaseHelper.getInstance()
                            .getSubjectName(sv.get(3).toString()), // nganh
                    DatabaseHelper.getInstance()
                            .getCollegeName(sv.get(4).toString()), // khoa
                    sv.get(5).toString() // khoa hoc
            ));
        });

        content.setItems(data);
        //update size after loaded data
        content.refresh();
        content.autosize();
    }
    
    public List<List<String>> getExportData()
    {
        List<List<String>> rs= new ArrayList<>();
        data.forEach((student) -> {
            List<String> listItem = new ArrayList<>();
            listItem.add(student.getHoTen());
            listItem.add(student.getMSSV());
            
            listItem.add(student.getLop());
            
            
            listItem.add(student.getNganh());
            listItem.add(student.getKhoa());
            listItem.add(student.getKhoaHoc());
            
            System.out.println("sv:" + student.getKhoa());
            
            
            rs.add(listItem);
        });
        return rs;
    }
}
