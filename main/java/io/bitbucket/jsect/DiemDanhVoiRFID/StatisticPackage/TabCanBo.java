package io.bitbucket.jsect.DiemDanhVoiRFID.StatisticPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.DatabasePackage.DatabaseHelper;
import io.bitbucket.jsect.DiemDanhVoiRFID.ManageMemberPackage.Teacher;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author RiO
 */
public class TabCanBo extends Tab {

    private final TableView<Teacher> content = new TableView<>();
    private final ObservableList<Teacher> data = FXCollections.observableArrayList();

    public TabCanBo() {
        super("Cán Bộ");
        getStyleClass().add("tab-member");

        ScrollPane scrollContent = new ScrollPane(content);

        TableColumn MSSVCol = new TableColumn("MSCB");
        MSSVCol.setCellValueFactory(new PropertyValueFactory("MSCB"));

        TableColumn HoTenCol = new TableColumn("Họ tên");
        HoTenCol.setCellValueFactory(new PropertyValueFactory("HoTen"));

        TableColumn EmailCol = new TableColumn("Email");
        EmailCol.setCellValueFactory(new PropertyValueFactory("Email"));

        TableColumn NganhCol = new TableColumn("Ngành");
        NganhCol.setCellValueFactory(new PropertyValueFactory("Nganh"));

        TableColumn KhoaCol = new TableColumn("Khoa");
        KhoaCol.setCellValueFactory(new PropertyValueFactory("Khoa"));

        content.getColumns().addAll(MSSVCol, HoTenCol, EmailCol, NganhCol, KhoaCol);
        setContent(content);
    }

    public void setData(List<List<Object>> list) {
        // init display
        data.clear();
        //  get data form database
        list.forEach((cb) -> {
            data.add(new Teacher(
                    cb.get(0).toString(), // macb
                    cb.get(1).toString(), // ho ten
                    "",
                    cb.get(2).toString(), // email
                    DatabaseHelper.getInstance()
                            .getSubjectName(cb.get(3).toString()), // nganh
                    DatabaseHelper.getInstance()
                            .getCollegeName(cb.get(4).toString()) // khoa
            ));
        });

        content.setItems(data);
        //update size after loaded data
        content.refresh();
        content.autosize();
    }
    public List<List<String>> getExportData()
    {
        List<List<String>> rs= new ArrayList<>();
        data.forEach((teacher) -> {
            List<String> listItem = new ArrayList<>();
            listItem.add(teacher.getMSCB());
            listItem.add(teacher.getHoTen());
            listItem.add(teacher.getEmail());
            listItem.add(teacher.getNganh());
            listItem.add(teacher.getKhoa());
            rs.add(listItem);
        });
        return rs;
    }
}
