package io.bitbucket.jsect.DiemDanhVoiRFID.RFIDPackage;

import com.sun.javafx.scene.KeyboardShortcutsHandler;
import java.awt.KeyboardFocusManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author nmhillusion
 */
public abstract class JsectRFIDListener implements EventHandler<KeyEvent>{
    private final StringBuilder value = new StringBuilder();
    private static final List<KeyCode> KEY_DIGIT = new ArrayList<>();
    private final List<Integer> exceptNodes = new ArrayList<>();
    
    public JsectRFIDListener(){
        KEY_DIGIT.add(KeyCode.DIGIT0);
        KEY_DIGIT.add(KeyCode.DIGIT1);
        KEY_DIGIT.add(KeyCode.DIGIT2);
        KEY_DIGIT.add(KeyCode.DIGIT3);
        KEY_DIGIT.add(KeyCode.DIGIT4);
        KEY_DIGIT.add(KeyCode.DIGIT5);
        KEY_DIGIT.add(KeyCode.DIGIT6);
        KEY_DIGIT.add(KeyCode.DIGIT7);
        KEY_DIGIT.add(KeyCode.DIGIT8);
        KEY_DIGIT.add(KeyCode.DIGIT9);
    }
    
    public JsectRFIDListener setExceptHashcodeNodes(Integer...hashcodes){
        exceptNodes.addAll(Arrays.asList(hashcodes));
        return this;
    }
    
    @Override
    public void handle(KeyEvent event) {
        if(exceptNodes.contains(event.getTarget().hashCode())){
            value.delete(0, value.length());
        }else if(KEY_DIGIT.contains(event.getCode())){
            value.append(event.getText());
            event.consume();
        }else if(event.getCode() == KeyCode.ENTER && value.length() > 0){
            onReceiveRFID(value.toString());
            value.delete(0, value.length());
            event.consume();
        }
    }
    
    public abstract void onReceiveRFID(String rfid);
}