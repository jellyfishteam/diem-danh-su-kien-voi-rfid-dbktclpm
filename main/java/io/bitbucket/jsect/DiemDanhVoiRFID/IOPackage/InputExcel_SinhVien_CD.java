package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.JOptionPane;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class InputExcel_SinhVien_CD {
	int stt=0;
	public InputExcel_SinhVien_CD(){
		stt=0;
	};
	/*Cau truc:
	 * Cot A: stt;
	 * Cot B: MSSV
	 * Cot C: Ho Ten
	 * Cot D: Lop
	 * Cot E: Nganh
	 * Cot F: Khoa (vi du: CNTT, Su pham,...)
	 * Cot G: Khoa (vi du: K40, k45, K38, K02,...)
	 * Cot H: RFID
	 * Hang 1: tieu de
	 * Hang 2+ : du lieu
	 * */
	public boolean getdatafromIterator(Iterator<Row> rowIter, ListType sv){
		int dat=0;
		String data=null;
		Iterator<Cell> cellIter;
		boolean t=true;
		Cell cell;
		CellType ct;
		SinhVien nsv = new SinhVien();
		if(rowIter.hasNext()){
			Row row = rowIter.next();
			cell=row.getCell(0);
			ct=cell.getCellTypeEnum();
			if(ct!=CellType._NONE){ 
				data = cell.getStringCellValue();
				if(!data.equalsIgnoreCase("STT") && !data.equalsIgnoreCase("#")) t=false;
			}
			if(t){
				cell=row.getCell(1);
				ct=cell.getCellTypeEnum();
				if(ct!=CellType._NONE){ 
					data = cell.getStringCellValue();
					if(!data.equalsIgnoreCase("MSSV")) t=false;
					}else t=false;
				if(t){
					cell=row.getCell(2);
					ct=cell.getCellTypeEnum();
					if(ct!=CellType._NONE){ 
						data = cell.getStringCellValue();
						if(!data.equalsIgnoreCase("Ho Ten") && !data.equalsIgnoreCase("HoTen")) t=false;
						}else t=false;
					if(t){
						cell=row.getCell(3);
						ct=cell.getCellTypeEnum();
						if(ct!=CellType._NONE){ 
							data = cell.getStringCellValue();
							if(!data.equalsIgnoreCase("Lop")) t=false;
						}
						if(t){
							cell=row.getCell(4);
							ct=cell.getCellTypeEnum();
							if(ct!=CellType._NONE){ 
								data = cell.getStringCellValue();
								if(!data.equalsIgnoreCase("Nganh")) t=false;
							}
							if(t){
								cell=row.getCell(5);
								ct=cell.getCellTypeEnum();
								if(ct!=CellType._NONE){ 
									data = cell.getStringCellValue();
									if(!data.equalsIgnoreCase("Khoa")) t=false;
								}
								if(t){
									cell=row.getCell(6);
									ct=cell.getCellTypeEnum();
									if(ct!=CellType._NONE){ 
										data = cell.getStringCellValue();
										if(!data.equalsIgnoreCase("K") && !data.equalsIgnoreCase("Khoa hoc") && !data.equalsIgnoreCase("Khoahoc")) t=false;
									}
									/*if(t){
										cell=row.getCell(7);
										ct=cell.getCellTypeEnum();
										if(ct!=CellType._NONE){ 
											data = cell.getStringCellValue();
											if(!data.equalsIgnoreCase("RFID")) t=false;
										}
									}*/
								}
							}
						}
					}
				}
			}
			if(t) while(rowIter.hasNext()){
				sv.add(new SinhVien());
				nsv= (SinhVien) sv.next();
				row = rowIter.next();
				for(int i=1; i<=6; i++){
					cell = row.getCell(i);
					if(cell!=null){
						ct = cell.getCellTypeEnum();
						switch(ct){
						case _NONE:
							data= "????";
							break;
						case STRING:
							data = cell.getStringCellValue();
							break;
						case NUMERIC:
							data = String.valueOf((int) cell.getNumericCellValue());
							break;
						default:
							data = "Du lieu khong hop le!";
							break;
						}
					}else data= "????";
					switch(i){
						case 1:
							nsv.setCode(data);
							break;
						case 2:
							nsv.setName(data);
							break;
						case 3:
							nsv.setClas(data);
							break;
						case 4:
							nsv.setJob(data);
							break;
						case 5:
							nsv.setDepartment(data);
							break;
						case 6:
							nsv.setYear(data);
							break;
						/*case 7:
							nsv.setCardCode(data);
							break;*/
					}
				}
				stt=stt+1;
			}
		}
		sv.gotoFirstElement();
		return t;
	}
	public void getExcel(String url, ListType sv){
		boolean t=true;
		try {
			FileInputStream input = new FileInputStream(new File(url));
			char[] part = new char[4];
			String c="";
			for(int i=0; i<4;i++){
				c=c+url.charAt(url.length()-(4-i));
			}
			try {
				if(c.equalsIgnoreCase(".xls")){
					HSSFWorkbook work = new HSSFWorkbook(input);
					HSSFSheet sheet;
					sheet = work.getSheetAt(0);
					Iterator<Row> rowIter = sheet.iterator();
					t=getdatafromIterator(rowIter, sv);
					if(!t){
						System.out.println("File khong dung dinh dang!");
//						JOptionPane.showMessageDialog(null, "File không đúng định dạng!", "Error", JOptionPane.WARNING_MESSAGE);
					}
				}else if(c.equalsIgnoreCase("xlsx")){
					XSSFWorkbook work = new XSSFWorkbook(input);
					XSSFSheet sheet = work.getSheetAt(0);
					Iterator<Row> rowIter = sheet.iterator();
					t=getdatafromIterator(rowIter, sv);
					if(!t){
						System.out.println("File khong dung dinh dang!");
//						JOptionPane.showMessageDialog(null, "File không đúng định dạng!", "Error", JOptionPane.WARNING_MESSAGE);
					}
				}else {
					System.out.println("Can't read this file!");
//					JOptionPane.showMessageDialog(null, "Không thể đọc file này!", "Error", JOptionPane.WARNING_MESSAGE);
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("This isn't file Excel!");
//				JOptionPane.showMessageDialog(null, "Không thể đọc file này!", "Error", JOptionPane.WARNING_MESSAGE);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("File not found!");
//			JOptionPane.showMessageDialog(null, "Không tìm thấy file!", "Error", JOptionPane.WARNING_MESSAGE);
		}
	}
	public int getNumberOfIn(){
		return stt;
	}
}
