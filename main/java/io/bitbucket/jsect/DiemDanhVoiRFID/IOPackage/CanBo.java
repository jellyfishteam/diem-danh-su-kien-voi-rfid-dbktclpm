package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

public class CanBo {	//loai hinh 1
	private String Code; 		//MSCB			X[1]
	private String Name; 		//Ho ten		X[2]
	private String Clas;		//Bo mon/to		X[3]
	//private String CardCode;	//RFID			X[4]
	private String Email;		//Email			X[5]
	private String Room;		//Khoa phong	X[6]
	/*private CanBo before;
	private CanBo next;
	private CanBo Last;
	public CanBo(){
		this.Code=null;
		this.Name=null;
		this.Clas=null;
		this.CardCode=null;
		this.Email=null;
		this.Room=null;
		this.before=null;
		this.next=null;
		this.Last= this;
		this.position=-1;
	}
	public CanBo Before(){
		return this.before;
	}
	public CanBo Next(){
		return this.next;
	}
	public int getPosition(){
		return this.position;
	}
	public void add(CanBo cb){
		CanBo ncb = cb;
		ncb.before=Last;
		Last.next=ncb;
		Last.Last=ncb;
		ncb.next = null;
		ncb.position = Last.position+1;;
		ncb.Last=ncb;
		this.Last=ncb;
	}*/
	public CanBo(){
		this.Code="";
		this.Name="";
		this.Clas="";
		//this.CardCode="";
		this.Email="";
		this.Room="";
	}
	public void setCode(String s){
		this.Code = s;
	}
	public void setName(String s){
		this.Name = s;
	}
	public void setEmail(String s){
		this.Email = s;
	}
	/*public void setCardCode(String s){
		this.CardCode = s;
	}*/
	public void setClas(String s){
		this.Clas = s;
	}
	public void setRoom(String s){
		this.Room = s;
	}
	public String getCode(){
		return this.Code;
	}
	public String getName(){
		return this.Name;
	}
	public String getEmail(){
		return this.Email;
	}
	/*public String getCardCode(){
		return this.CardCode;
	}*/
	public String getClas(){
		return this.Clas;
	}
	public String getRoom(){
		return this.Room;
	}
}
