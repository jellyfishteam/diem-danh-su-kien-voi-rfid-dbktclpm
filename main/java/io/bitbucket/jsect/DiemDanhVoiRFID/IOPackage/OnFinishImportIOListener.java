package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

import java.util.List;

/**
 *
 * @author nmhillusion
 */
public interface OnFinishImportIOListener {
    void handle(List<List<String>> data);
}
