package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

public class SinhVien {  //loai hinh 2
	private String Code;		//MSSV									X[1]
	private String Name;		//Ho ten								X[2]
	private String Clas;		//Lop									X[3]
	//private String CardCode;	//RFID									X[4]
	private String Job;			//Nganh									X[5]
	private String Department;	//Khoa (vi du: CNTT, Su pham,...)		X[6]
	private String Year;		//Khoa (vi du: K40, k45, K38, K02,...)	X[7]
	public SinhVien(){
		this.Code="";
		this.Name="";
		this.Clas="";
		//this.CardCode="";
		this.Job="";
		this.Department="";
		this.Year="";
	}
	public void setCode(String s){
		this.Code = s;
	}
	public void setName(String s){
		this.Name = s;
	}
	public void setClas(String s){
		this.Clas = s;
	}
	public void setJob(String s){
		this.Job = s;
	}
	public void setDepartment(String s){
		this.Department=s;
	}
	public void setYear(String s){
		this.Year=s;
	}
	/*public void setCardCode(String s){
		this.CardCode = s;
	}*/
	public String getCode(){
		return this.Code;
	}
	public String getName(){
		return this.Name;
	}
	public String getClas(){
		return this.Clas;
	}
	public String getJob(){
		return this.Job;
	}
	public String getDepartment(){
		return this.Department;
	}
	public String getYear(){
		return this.Year;
	}
	/*public String getCardCode(){
		return this.CardCode;
	}*/

}
