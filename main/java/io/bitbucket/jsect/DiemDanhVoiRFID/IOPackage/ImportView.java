package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.EventPackage.Event;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.FormAction;
import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.OnFormActionListener;
import java.io.File;
import java.util.List;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

/**
 *
 * @author nmhillusion
 */
public class ImportView extends FormAction{
    private static OnFinishImportIOListener listenerFinish;
    private static IOable _io;
    private final static Label lblTitle = new Label();

    public ImportView(OnFormActionListener lis, Event event, OnFinishImportIOListener closeLis, String importFor, IOable io) {
        super(lis, event);
        _io = io;
        listenerFinish = closeLis;
        lblTitle.setText(" - Nhập danh sách cho " + importFor + " -");
        lblTitle.setId("lblTitle");
        setTitle(lblTitle.getText());
    }

    @Override
    protected void setContent() {
        isDone = true;
        
        GridPane table = new GridPane();
        table.setPrefSize(400, 400);
        table.setHgap(6);
        table.setVgap(6);
        table.getStyleClass().add("table-import");
        
        TextField pathFile = new TextField();
        Button btnChoose = new Button("Choose File");
        
        pathFile.setDisable(true);
        pathFile.getStyleClass().add("input-path-file");
                
        btnChoose.setOnAction((event) -> {
            FileChooser fc = new FileChooser();
            fc.setInitialDirectory(new File("/"));
            fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("over MS Excel 2007", "*.xlsx"), new FileChooser.ExtensionFilter("excel 2003", "*.xls"));
            fc.setTitle("Choose file to import");
            File file = fc.showOpenDialog(getOwner());
            if(file != null){
                pathFile.setText(file.getAbsolutePath());
                List<List<String>> res = _io.importFile(file.getAbsolutePath());
                
                table.getChildren().clear();
                for(int i = 0; i < res.size() ; ++i){
                    for(int j = 0; j < res.get(i).size(); ++j){
                        table.add(new Text(res.get(i).get(j)), j, i);
                    }
                }
            }
        });
        
        Button btnImport = new Button("Import");
        btnImport.setOnAction((event) -> {
            if(pathFile.getText().length() > 0){
                List<List<String>> res = _io.importFile(pathFile.getText());
                if(res != null && !res.isEmpty()){
                    listenerFinish.handle(res);
                }else{
                    io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert.getIntance()
                                .setTitle("Kết quả: Error")
                                .setHeaderText("Danh sách rỗng, không có gì để thêm!")
                                .show();
                }
                close();
            }else{
                new Alert(AlertType.WARNING, "Please choose file before", ButtonType.CLOSE).show();
            }
        });
        GridPane.setHalignment(btnImport, HPos.CENTER);
        btnImport.setId("btnImport");
        
        HBox inputFile = new HBox(8, pathFile, btnChoose);
        inputFile.setAlignment(Pos.CENTER);
        VBox tmpRoot = new VBox(6, lblTitle, inputFile, new ScrollPane(table), btnImport);
        tmpRoot.getStyleClass().add("io-view");
        tmpRoot.setAlignment(Pos.CENTER);
        
        root = tmpRoot;
    }
}