package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

import java.util.List;

/**
 *
 * @author nmhillusion
 */
public interface IOable {
    public List<List<String>> importFile(String url);
    public boolean exportFile(List<List<String>> data, String filename);
}