package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import javafx.stage.FileChooser;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author nmhillusion
 */
public class ExportExcelFile {
    private final static ExportExcelFile INSTANCE = new ExportExcelFile();
    
    private ExportExcelFile(){}
    public static ExportExcelFile getInstance(){
        return INSTANCE;
    }
    
    private Workbook getWorkbook(String excelFilePath)
            throws IOException {
        Workbook workbook = null;
     
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook();
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook();
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }
     
        return workbook;
    }
    
    public File writeExcel(List<List<String>> rowDatas) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Xuất file");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("over excel 2007", "*.xlsx"), new FileChooser.ExtensionFilter("excel 2003", "*.xls"));
        fileChooser.setInitialDirectory(new File("."));
        File file = fileChooser.showSaveDialog(null);
        String excelFilePath = "";
        if(file != null){
            excelFilePath = file.getAbsolutePath();
        }else{
            return null;
        }
        
        Workbook workbook = getWorkbook(excelFilePath);
        Sheet sheet = workbook.createSheet();
        int rowCount = 0;
     
        for (List<String> rowData : rowDatas) {
            Row row = sheet.createRow(rowCount++);
            writeBook(rowData, row);
        }
     
        try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
            workbook.write(outputStream);
        }
        return file;
    }
    
    private void writeBook(List<String> rowData, Row row) {        
        for(int i = 0; i < rowData.size(); ++i){
            Cell cell = row.createCell(i);
            cell.setCellValue(rowData.get(i));
        }
    }
}
