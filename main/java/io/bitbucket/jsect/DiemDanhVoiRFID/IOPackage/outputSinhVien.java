package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.AlertPackage.Alert;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class outputSinhVien {
	ListType sv;
	SinhVien outsv;
	private Font ftieude = null;
	private Font ftencot = null;
	private Font fbang = null;
	private CellStyle CT0 = null;
	private CellStyle CT1 = null;
	private CellStyle CTbang = null;
	public outputSinhVien(ListType ne){
		this.sv=ne;
	}
	private void createnewList(ListType row, String TieuDe){
		Cell[] cell = new Cell[7];
		Row inrow = (Row) row.getElement(0);
		int i, j;
		ftieude.setBold(true);
		ftieude.setItalic(true);
		ftieude.setColor(IndexedColors.AQUA.index);
		ftieude.setFontHeight((short) 20);
		ftieude.setFontHeightInPoints((short) 20);
		ftieude.setFontName("Arial");
		CT0.setFont(ftieude);
		CT0.setAlignment(HorizontalAlignment.CENTER);
		CT0.setVerticalAlignment(VerticalAlignment.CENTER);
		ftencot.setBold(true);
		ftencot.setColor(IndexedColors.RED.index);
		ftencot.setFontHeight((short) 12);
		ftencot.setFontHeightInPoints((short) 12);
		ftencot.setFontName("Arial");
		CT1.setAlignment(HorizontalAlignment.CENTER);
		CT1.setVerticalAlignment(VerticalAlignment.CENTER);
		CT1.setFont(ftencot);
		CT1.setBorderBottom(BorderStyle.MEDIUM);
		CT1.setBorderLeft(BorderStyle.MEDIUM);
		CT1.setBorderRight(BorderStyle.MEDIUM);
		CT1.setBorderTop(BorderStyle.MEDIUM);
		CT1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		CT1.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
		fbang.setFontHeight((short) 12);
		fbang.setFontHeightInPoints((short) 12);
		fbang.setFontName("Arial");
		CTbang.setFont(fbang);
		CTbang.setBorderBottom(BorderStyle.THIN);
		CTbang.setBorderLeft(BorderStyle.THIN);
		CTbang.setBorderRight(BorderStyle.THIN);
		CTbang.setBorderTop(BorderStyle.THIN);
		cell[0] = inrow.createCell(0);
		cell[0].setCellValue(TieuDe);
		cell[0].setCellStyle(CT0);
		inrow = (Row) row.getElement(1);
		for(j=0;j<=6;j++){
			cell[j] = inrow.createCell(j);
			cell[j].setCellStyle(CT1);
		}
		cell[0].setCellValue("STT");
		cell[1].setCellValue("Họ tên");
		cell[2].setCellValue("MSSV");
		cell[3].setCellValue("Lớp");
		cell[4].setCellValue("Khoa");
		cell[5].setCellValue("Ngành");
		cell[6].setCellValue("Niên khóa");
		//cell[7].setCellValue("RFID");
		i=2;
		while(sv.hasnext()){
			inrow = (Row) row.getElement(i);
			for(j=0;j<=6;j++){
				cell[j] = inrow.createCell(j);
				cell[j].setCellStyle(CTbang);
			}
			outsv = (SinhVien) sv.next();
			cell[0].setCellValue(i-1);
			cell[1].setCellValue(outsv.getName());
			cell[2].setCellValue(outsv.getCode());
			cell[3].setCellValue(outsv.getClas());
			cell[4].setCellValue(outsv.getDepartment());
			cell[5].setCellValue(outsv.getJob());
			cell[6].setCellValue(outsv.getYear());
			//cell[7].setCellValue(outsv.getCardCode());
			i=i+1;
		}
	}
	public boolean savetofile(String url, String TieuDe){
		int n = sv.getnumElement();
		ListType row = new ListType(null);
		File file = new File(url);
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
                        return false;
		}
		String part="";
		for(int i=0; i<4;i++){
			part=part+url.charAt(url.length()-(4-i));
		}
		if(part.equalsIgnoreCase(".XLS")){		
			HSSFWorkbook work = new HSSFWorkbook();
			HSSFSheet sheet = work.createSheet("Danh Sách Sinh Viên");
			ftencot = work.createFont();
			ftieude = work.createFont();
			fbang = work.createFont();
			CT0 = work.createCellStyle();
			CT1 = work.createCellStyle();
			CTbang = work.createCellStyle();
			for(int i=0; i<=n+1;i++){
				row.add(sheet.createRow(i));
			}
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));
                        String tieude = TieuDe.substring(0, TieuDe.length()-4);
			createnewList(row, tieude);
			if(fout!=null) try {
				work.write(fout);
			} catch (IOException e) {
				e.printStackTrace();
                                return false;
			}
		}else if(part.equalsIgnoreCase("XLSX")){
			XSSFWorkbook work = new XSSFWorkbook();
			XSSFSheet sheet = work.createSheet("Danh Sách Sinh Viên");
			ftencot = work.createFont();
			ftieude = work.createFont();
			fbang = work.createFont();
			CT0 = work.createCellStyle();
			CT1 = work.createCellStyle();
			CTbang = work.createCellStyle();
			for(int i=0; i<=n+1;i++){
				row.add(sheet.createRow(i));
			}
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));
                        String tieude = TieuDe.substring(0, TieuDe.length()-5);
			createnewList(row, tieude);
			if(fout!=null) try {
				work.write(fout);
			} catch (IOException e) {
				e.printStackTrace();
				Alert.getIntance()
                                        .setTitle("Lỗi lưu tập tin")
                                        .setContent("Không thể lưu tập tin. Lý do: " + e.getLocalizedMessage())
                                        .show();
                                return false;
			}
		}
                return true;
	}
}
