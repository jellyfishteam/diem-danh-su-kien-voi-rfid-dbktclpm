package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.JOptionPane;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class InputExcel_CanBo_CD {
	int stt = 0;
	public InputExcel_CanBo_CD(){
		stt=0;
	};
	/*Cau truc:
	 * Cot A: stt;
	 * Cot B: MSCB
	 * Cot C: Ho Ten
	 * Cot D: Email
	 * Cot E: Bo mon/ to
	 * Cot F: Khoa/phong 
	 * Cot G: RFID
	 * Hang 1+n: tieu de
	 * Hang 2+n+ : du lieu
	 * */
	public boolean getdatafromIterator(Iterator<Row> rowIter, ListType cb){
		int dat=0;
		String data="";
		Cell cell;
		CellType ct;
		boolean t=true;
		CanBo ncb = new CanBo();
		if(rowIter.hasNext()){
			Row row = rowIter.next();
			cell=row.getCell(0);
			ct=cell.getCellTypeEnum();
			if(ct!=CellType._NONE){ 
				data = cell.getStringCellValue();
				if(!data.equalsIgnoreCase("STT") && !data.equalsIgnoreCase("#")) t=false;
			}
			if(t){
				cell=row.getCell(1);
				ct=cell.getCellTypeEnum();
				if(ct!=CellType._NONE){ 
					data = cell.getStringCellValue();
					if(!data.equalsIgnoreCase("MSCB")) t=false;
					}else t=false;
				if(t){
					cell=row.getCell(2);
					ct=cell.getCellTypeEnum();
					if(ct!=CellType._NONE){ 
						data = cell.getStringCellValue();
						if(!data.equalsIgnoreCase("Ho Ten") && !data.equalsIgnoreCase("HoTen")) t=false;
						}else t=false;
					if(t){
						cell=row.getCell(3);
						ct=cell.getCellTypeEnum();
						if(ct!=CellType._NONE){ 
							data = cell.getStringCellValue();
							if(!data.equalsIgnoreCase("Email")) t=false;
						}
						if(t){
							cell=row.getCell(4);
							ct=cell.getCellTypeEnum();
							if(ct!=CellType._NONE){ 
								data = cell.getStringCellValue();
								if(!data.equalsIgnoreCase("Bo Mon") && !data.equalsIgnoreCase("BoMon") && !data.equalsIgnoreCase("To")) t=false;
							}
							if(t){
								cell=row.getCell(5);
								ct=cell.getCellTypeEnum();
								if(ct!=CellType._NONE){ 
									data = cell.getStringCellValue();
									if(!data.equalsIgnoreCase("Khoa") && !data.equalsIgnoreCase("Phong")) t=false;
								}
								/*if(t){
									cell=row.getCell(6);
									ct=cell.getCellTypeEnum();
									if(ct!=CellType._NONE){ 
										data = cell.getStringCellValue();
										if(!data.equalsIgnoreCase("RFID")) t=false;
									}
								}*/
							}
						}
					}
				}
			}
			if(t) while(rowIter.hasNext()){
				cb.add(new CanBo());
				ncb= (CanBo) cb.next();
				row = rowIter.next();
				for(int i=1; i<=5; i++){
					cell = row.getCell(i);
					if(cell!=null){
					ct = cell.getCellTypeEnum();
					switch(ct){
						case _NONE:
							data= "????";
							break;
						case STRING:
							data = cell.getStringCellValue();
							break;
						case NUMERIC:
							data = String.valueOf((int) cell.getNumericCellValue());
							break;
						default:
							data = "Du lieu khong hop le!";
							break;
					}
					} else data= "????";
					switch(i){
						case 1:
							ncb.setCode(data);
							break;
						case 2:
							ncb.setName(data);
							break;
						case 3:
							ncb.setEmail(data);
							break;
						case 4:
							ncb.setClas(data);
							break;
						case 5:
							ncb.setRoom(data);
							break;
						/*case 6:
							ncb.setCardCode(data);
							break;*/
					}
				}
				stt=stt+1;
			}
		}
		cb.gotoFirstElement();
		return t;
	}
	public void getExcel(String url, ListType cb){
		boolean t=true;
		try {
			FileInputStream input = new FileInputStream(new File(url));
			String c="";
			for(int i=0; i<4;i++){
				c=c+url.charAt(url.length()-(4-i));
			}
			try {
				if(c.equalsIgnoreCase(".XLS")){
					HSSFWorkbook work = new HSSFWorkbook(input);
					HSSFSheet sheet;
					sheet = work.getSheetAt(0);
					Iterator<Row> rowIter = sheet.iterator();
					t=getdatafromIterator(rowIter, cb);
					if(!t){
						System.out.println("File khong dung dinh dang!");
						JOptionPane.showMessageDialog(null, "File không đúng định dạng!", "Error", JOptionPane.WARNING_MESSAGE);
					}
				}else if(c.equalsIgnoreCase("XLSX")){
					XSSFWorkbook work = new XSSFWorkbook(input);
					XSSFSheet sheet = work.getSheetAt(0);
					Iterator<Row> rowIter = sheet.iterator();
					t=getdatafromIterator(rowIter, cb);
					if(!t){ 
						System.out.println("File khong dung dinh dang!");
						JOptionPane.showMessageDialog(null, "File không đúng định dạng!", "Error", JOptionPane.WARNING_MESSAGE);
					}
				}else{
					System.out.println("Can't read this file!");
					JOptionPane.showMessageDialog(null, "Không thể đọc file này!", "Error", JOptionPane.WARNING_MESSAGE);
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("This isn't file Excel!");
				JOptionPane.showMessageDialog(null, "Không thể đọc file này!", "Error", JOptionPane.WARNING_MESSAGE);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("File not found!");
			JOptionPane.showMessageDialog(null, "Không tìm thấy file!", "Error", JOptionPane.WARNING_MESSAGE);
		}
	}
	public int getNumberOfIn(){
		return stt;
	}
}