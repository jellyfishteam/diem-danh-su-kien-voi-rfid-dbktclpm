/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

/**
 *
 * @author TLiem
 */
public class IOCanBo implements IOable{
    String URL = "";
    public IOCanBo(String Url){
        this.URL = Url;
    }
    @Override
    public List<List<String>> importFile(String url) {
        List<List<String>> Data = new ArrayList<>();
        InputExcel_CanBo_CD input = new InputExcel_CanBo_CD();
        ListType getin = new ListType(new CanBo());
        input.getExcel(url, getin);
        getin.gotoFirstElement();
        int i=0;
        while(getin.hasnext()){
            List<String> newgetin = new ArrayList<>();
            CanBo CB = (CanBo) getin.next();
            Data.add(newgetin);
            Data.get(i).add(CB.getName());
            Data.get(i).add(CB.getCode());
            Data.get(i).add(CB.getEmail());
            Data.get(i).add(CB.getClas());
            Data.get(i).add(CB.getRoom());
            //Data.get(i).add(CB.getCardCode());
            i=i+1;
        }
        return Data;
    }

    @Override
    public boolean exportFile(List<List<String>> data, String filename) {
        ListType CB =new ListType(new CanBo());
        CanBo ncb = new CanBo();
        Iterator<List<String>> G1 = data.iterator();
        List<String> getin =new ArrayList<String>();
        while(G1.hasNext()){
            CB.add(new CanBo());
            ncb = (CanBo) CB.next();
            getin = G1.next();
            ncb.setName(getin.get(0));
            ncb.setCode(getin.get(1));
            ncb.setEmail(getin.get(2));
            ncb.setClas(getin.get(3));
            ncb.setRoom(getin.get(4));
            //ncb.setCardCode(getin.get(5));
        }
        CB.gotoFirstElement();
        outputCanBo OutCanBo = new outputCanBo(CB);
        String S = URL + filename;
        return OutCanBo.savetofile(S, filename);
    }
    
}
