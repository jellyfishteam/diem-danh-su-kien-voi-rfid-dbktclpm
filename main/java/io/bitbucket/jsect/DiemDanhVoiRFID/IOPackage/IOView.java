package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

import io.bitbucket.jsect.DiemDanhVoiRFID.MainPackage.DiemDanhVoiRFID;
import java.io.File;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author nmhillusion
 */
public class IOView extends Stage{
    private final static IOView INSTANCE = new IOView();
    
    private static OnCloseViewIOListener listener;
    private static IOable _io;
    
    private IOView(){
        super();
    }
    public static IOView getInstance(IOable io, OnCloseViewIOListener lis){
        listener = lis;
        _io = io;
        return INSTANCE;
    }
    
    public void display(){
        GridPane root = new GridPane();
        
        Label lblTitle = new Label("Nhập danh sách cho: ");
        ChoiceBox<String> cbTypeImport = new ChoiceBox<>(FXCollections.observableArrayList("Cán bộ", "Sinh viên"));
        cbTypeImport.getSelectionModel().select(0);
        
        TextField pathFile = new TextField();
        Button btnChoose = new Button("Choose File");
        
        pathFile.setDisable(true);
        pathFile.getStyleClass().add("input-path-file");
                
        btnChoose.setOnAction((event) -> {
            FileChooser fc = new FileChooser();
            fc.setInitialDirectory(new File("/"));
            fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("MS Excel 2003", "*.xls"), new FileChooser.ExtensionFilter("over MS Excel 2007", "*.xlsx"), new FileChooser.ExtensionFilter("demo", "*.txt"));
            fc.setTitle("Choose file to import");
            File file = fc.showOpenDialog(getOwner());
            if(file != null){
                new Alert(AlertType.INFORMATION, file.getAbsolutePath(), ButtonType.CLOSE).show();
                pathFile.setText(file.getAbsolutePath());
            }
        });
        
        Button btnImport = new Button("Import");
        btnImport.setOnAction((event) -> {
            if(pathFile.getText().length() > 0){
                List<List<String>> res = _io.importFile(pathFile.getText());                
                if(res != null && !res.isEmpty())listener.handle(res);
                close();
            }else{
                new Alert(AlertType.WARNING, "Please choose file before", ButtonType.CLOSE).show();
            }
        });
        GridPane.setHalignment(btnImport, HPos.CENTER);
        btnImport.setId("btnImport");
        
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.RIGHT);
        ColumnConstraints column2 = new ColumnConstraints();
        column1.setHalignment(HPos.LEFT);
        root.getColumnConstraints().addAll(column1, column2);
        
        root.addRow(0, lblTitle, cbTypeImport);
        root.addRow(1, pathFile, btnChoose);
        root.add(btnImport, 0, 2, 2,1);
        
        root.setVgap(8); root.setHgap(8);
        root.setAlignment(Pos.CENTER);
        root.getStyleClass().add("io-view");
                
        Scene scene = new Scene(root);
        scene.getStylesheets().add(DiemDanhVoiRFID.getStylePath());
        setScene(scene);
        
        setTitle("Nhập danh sách người tham dự");
        initModality(Modality.APPLICATION_MODAL);
        getIcons().add(DiemDanhVoiRFID.getIcon());
        showAndWait();
    }
}