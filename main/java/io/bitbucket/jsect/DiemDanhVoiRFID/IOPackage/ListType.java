package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

public class ListType {
	Object element[][] = new Object[255][255];
	int anum=0;
	int bnum=0;
	int pos=0;
	public ListType(Object type){
		for(int i=0;i<255;i++)
			for(int j=0;j<255;j++)
				element[i][j]=type;
		anum=0;
		bnum=0;
		pos=0;
	}
	public Object getElement(int i){
		if(i<0) return null;
		else{
			int a = i/255;
			int b = i%255;
			return element[a][b];
		}
	}
	public void add(Object ob){
		if(anum<255){
			element[anum][bnum] = ob;
			bnum = bnum+1;
			if(bnum==255){
				bnum = 0;
				anum = anum+1;
			}
		}
		//else không thêm được giá trị
	}
	public Object next(){
		Object rt=null;
		if(pos<((anum*255)+bnum)){
			rt= getElement(pos);
			pos=pos+1;
		}
		return rt;
	}
	public boolean hasnext(){
		if(pos<((anum*255)+bnum)) return true;
		else return false;
	}
	public int getpostion(){
		return pos-1;
	}
	public int getnumElement(){
		return ((anum*255)+bnum);
	}
	public void gotoFirstElement(){
		this.pos=0;
	}
}
