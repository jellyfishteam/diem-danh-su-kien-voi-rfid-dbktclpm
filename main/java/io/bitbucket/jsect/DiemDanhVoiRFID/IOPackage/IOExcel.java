/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

/**
 *
 * @author TLiem
 */
public class IOExcel {
    String url = null;
    public IOExcel(String URL){
        url = URL;
    }
    public ListType importCanBo(){
        InputExcel_CanBo_CD imp = new InputExcel_CanBo_CD();
        ListType CB = new ListType(new CanBo());
        imp.getExcel(url, CB);
        return CB;
    }
    public ListType importSinhVien(){
        InputExcel_SinhVien_CD imp = new InputExcel_SinhVien_CD();
        ListType SV = new ListType(new SinhVien());
        imp.getExcel(url, SV);
        return SV;
    }
    public boolean exportCanBo(ListType CB, String TieuDe){
        outputCanBo OCB = new outputCanBo(CB);
        return OCB.savetofile(url, TieuDe);
    }
    public boolean exportSinhVien(ListType SV, String TieuDe){
        outputSinhVien OSV = new outputSinhVien(SV);
        return OSV.savetofile(url, TieuDe);
    }
}
