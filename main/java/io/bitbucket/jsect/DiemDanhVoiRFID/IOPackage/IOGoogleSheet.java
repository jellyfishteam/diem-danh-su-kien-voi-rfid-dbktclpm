package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author nmhillusion
 */
public class IOGoogleSheet {
    private static final IOGoogleSheet INSTANCE = new IOGoogleSheet();
    
    private IOGoogleSheet(){}
    public static IOGoogleSheet getInstance(){
        return INSTANCE;
    }
    
    public List<List<String>> getSheet(String id, String nameSheet, String range){
        List<List<String>> result = new ArrayList<>();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("username", "nmhillusion")
                .url("https://nmhillusion.herokuapp.com/api/google/spreadsheet/" + id + "/getValues/"+nameSheet+"!"+range)
                .build();
        
        try {
            String json = client.newCall(request).execute().body().string();
            JSONObject jsonObject = (JSONObject) JSONValue.parseWithException(json);
            JSONArray jsonArray = (JSONArray) jsonObject.get("data");
            
            List<String> itemList = new ArrayList<>();
            jsonArray.forEach((rawRow) -> {
                System.out.println("row: " + rawRow);
                JSONArray row = (JSONArray) rawRow;
                itemList.add(row.get(2).toString());    // ho ten
                itemList.add(row.get(1).toString());
                itemList.add(row.get(3).toString());
                itemList.add(row.get(4).toString());
                itemList.add(row.get(5).toString());
                itemList.add(row.get(6).toString());
            });
            if(!itemList.isEmpty()) result.add(itemList);
        } catch (Exception ex) {
            Logger.getLogger(IOGoogleSheet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return result;
    }
}
