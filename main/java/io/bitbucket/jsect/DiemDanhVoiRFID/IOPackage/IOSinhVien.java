/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.bitbucket.jsect.DiemDanhVoiRFID.IOPackage;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author TLiem
 */
public class IOSinhVien implements IOable{
    String URL = "";
    public IOSinhVien(String Url){
        URL=Url;
    }

    @Override
    public List<List<String>> importFile(String url) {
        List<List<String>> Data = new ArrayList<>();
        InputExcel_SinhVien_CD input = new InputExcel_SinhVien_CD();
        ListType getin = new ListType(new SinhVien());
        input.getExcel(url, getin);
        getin.gotoFirstElement();
        int i=0;
        while(getin.hasnext()){
            List<String> newgetin= new ArrayList<>();
            SinhVien SV = (SinhVien) getin.next();
            Data.add(newgetin);
            Data.get(i).add(SV.getName());
            Data.get(i).add(SV.getCode());
            Data.get(i).add(SV.getClas());
            Data.get(i).add(SV.getJob());
            Data.get(i).add(SV.getDepartment());
            Data.get(i).add(SV.getYear());
            //Data.get(i).add(SV.getCardCode());
            i=i+1;
        }
        return Data;
    }

    
    @Override
    public boolean exportFile(List<List<String>> data, String filename) {
        ListType SV =new ListType(new SinhVien());
        SinhVien nsv = new SinhVien();
        Iterator<List<String>> G1 = data.iterator();
        List<String> getin = new ArrayList<String>();
        while(G1.hasNext()){
            SV.add(new SinhVien());
            nsv = (SinhVien) SV.next();
            getin = G1.next();
            nsv.setName(getin.get(0));
            nsv.setCode(getin.get(1));
            nsv.setClas(getin.get(2));
            nsv.setJob(getin.get(3));
            nsv.setDepartment(getin.get(4));
            nsv.setYear(getin.get(5));
            //nsv.setCardCode(getin.get(6));
        }
        SV.gotoFirstElement();
        outputSinhVien OutSinhVien = new outputSinhVien(SV);
        String S = URL + filename;
        return OutSinhVien.savetofile(S, filename);
    }
}
